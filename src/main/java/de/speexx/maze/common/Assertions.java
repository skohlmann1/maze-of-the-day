/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.common;

import static java.lang.String.valueOf;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import static java.util.regex.Pattern.compile;
import org.apiguardian.api.API;
import static org.apiguardian.api.API.Status.STABLE;

/**
 * Enhanced assertion support.
 * @author sascha.kohlmann
 */
@API(status=STABLE)
public final class Assertions {

    private Assertions() {throw new AssertionError();}

    /** Asserts the given value is {@code false} and throws an {@link IllegalArgumentException} if not.
     * @param bool boolean to test
     * @param message message of the exception in case of the value is {@code true}. May be {@code null}.
     * @throws IllegalArgumentException if and only if <em>bool</em> is {@code true} */
    public static void assertFalse(final boolean bool, final String message) {
        assertFalse(bool, () -> message);
    }

    /** Asserts the given value is {@code false} and throws an {@link IllegalArgumentException} if not.
     * @param bool boolean to test
     * @param messageSupplier for the message of the exception in case of the value is {@code true}. May be {@code null}.
     * @throws IllegalArgumentException if and only if <em>bool</em> is {@code true} */
    public static void assertFalse(final boolean bool, final Supplier<String> messageSupplier) {
        if (bool) {
            throw new IllegalArgumentException(createMessage(bool, messageSupplier));
        }
    }

    /** Asserts the given value is {@code true} and throws an {@link IllegalArgumentException} if not.
     * @param bool boolean to test
     * @param message message of the exception in case of the value is {@code false}
     * @throws IllegalArgumentException if and only if <em>bool</em> is {@code false} */
    public static void assertTrue(final boolean bool, final String message) {
        assertTrue(bool, () -> message);
    }

    /** Asserts the given value is {@code true} and throws an {@link IllegalArgumentException} if not.
     * @param bool boolean to test
     * @param messageSupplier for the message of the exception in case of the value is {@code false}. May be {@code null}.
     * @throws IllegalArgumentException if and only if <em>bool</em> is {@code false} */
    public static void assertTrue(final boolean bool, final Supplier<String> messageSupplier) {
        if (!bool) {
            throw new IllegalArgumentException(createMessage(bool, messageSupplier));
        }
    }

    /** Asserts the given value is {@code false} and throws an {@link IllegalStateException} if not.
     * @param bool boolean to test
     * @param message message of the exception in case of the value is {@code true}. May be {@code null}.
     * @throws IllegalStateException if and only if <em>bool</em> is {@code false} */
    public static void assertStateFalse(final boolean bool, final String message) {
        assertStateFalse(bool, () -> message);
    }

    /** Asserts the given value is {@code true} and throws an {@link IllegalStateException} if not.
     * @param bool boolean to test
     * @param message message of the exception in case of the value is {@code false}. May be {@code null}.
     * @throws IllegalStateException if and only if <em>bool</em> is {@code false} */
    public static void assertStateTrue(final boolean bool, final String message) {
        assertStateTrue(bool, () -> message);
    }

    /** Asserts the given value is {@code false} and throws an {@link IllegalStateException} if not.
     * @param bool boolean to test
     * @param messageSupplier for the message of the exception in case of the value is {@code true}. May be {@code null}.
     * @throws IllegalStateException if and only if <em>bool</em> is {@code true} */
    public static void assertStateFalse(final boolean bool, final Supplier<String> messageSupplier) {
        if (bool) {
            throw new IllegalStateException(createMessage(bool, messageSupplier));
        }
    }

    /** Asserts the given value is {@code true} and throws an {@link IllegalStateException} if not.
     * @param bool boolean to test
     * @param messageSupplier for the message of the exception in case of the value is {@code false}. May be {@code null}.
     * @throws IllegalStateException if and only if <em>bool</em> is {@code true} */
    public static void assertStateTrue(final boolean bool, final Supplier<String> messageSupplier) {
        if (!bool) {
            throw new IllegalStateException(createMessage(bool, messageSupplier));
        }
    }

    /** Measures length after {@linkplain String#trim() triming} the given string. 
     * @param string string to measure
     * @param max maximum length of the string
     * @param message message of the exception in case of the {@code string} is too long
     * @return the given string
     * @throws IllegalArgumentException if and only if {@code string} is too long */
    public static String assertLength(final String string, final int max, final String message) {
        return Assertions.assertLength(string, max, () -> message);
    }

    /** Measures length after {@linkplain String#trim() triming} the given string. 
     * @param string string to measure
     * @param max maximum length of the string
     * @param messageSupplier for the message of the exception in case of the {@code string} is too long
     * @return the given string
     * @throws IllegalArgumentException if and only if {@code string} is too long */
    public static String assertLength(final String string, final int max, final Supplier<String> messageSupplier) {
        return assertLength(string, 0, max, messageSupplier);
    }

    /** Measures length after {@linkplain String#trim() triming} the given string to be in range of {@code minimum} and
     * {@code maximum} length.
     * @param string string to measure
     * @param max maximum length of the string
     * @param min minimum length of the string
     * @param message message of the exception in case of the {@code string} length is not in the required range.
     * @return the given string
     * @throws IllegalArgumentException if and only if {@code string} length is not in the required range */
    public static String assertLength(final String string, final int min, final int max, final String message) {
        return assertLength(string, min, max, () -> message);
    }

    /** Measures length after {@linkplain String#trim() triming} the given string to be in range of {@code minimum} and
     * {@code maximum} length.
     * @param string string to measure
     * @param max maximum length of the string
     * @param min minimum length of the string
     * @param messageSupplier for the message of the exception in case of the {@code string} length is not in the required range. May be {@code null}.
     * @return the given string
     * @throws IllegalArgumentException if and only if {@code string} length is not in the required range */
    public static String assertLength(final String string, final int min, final int max, final Supplier<String> messageSupplier) {
        int length = string.trim().length();
        if (length > max) {
            throw new IllegalArgumentException(createMessage("too long", messageSupplier));
        } else if (length < min) {
            throw new IllegalArgumentException(createMessage("too short", messageSupplier));
        }
        return string;
    }

    /** Asserts that the given {@link String} is not empty (of {@link String#length()} after triming.
     * @param string string to measure
     * @param message message of the exception in case of the {@code string} length is of length 0 after trimming.
     * @return the given string
     * @throws IllegalArgumentException if and only if {@code string} length is of length 0 after trimming */
    public static String assertNotEmpty(final String string, final String message) {
        return assertNotEmpty(string, () -> message);
    }

    /** Asserts that the given {@link String} is not empty (of {@link String#length()} after triming.
     * @param string string to measure
     * @param messageSupplier fo the message of the exception in case of the {@code string} length is of length 0 after trimming.
     * @return the given string
     * @throws IllegalArgumentException if and only if {@code string} length is of length 0 after trimming */
    public static String assertNotEmpty(final String string, final Supplier<String> messageSupplier) {
        assertNotNull(string, () -> "string is null");
        if (string.trim().isEmpty()) {
            throw new IllegalArgumentException(createMessage("empty", messageSupplier));
        }
        return string;
    }

    /**
     * Asserts the the given collection is not empty.
     * @param <T> type of the collection
     * @param collection teh collection to check
     * @param message message of the exception in case of the {@code collection} is empty.
     * @return the collection
     * @throws IllegalArgumentException if and only if {@code collection} is empty */
    public static <T extends Collection> T assertNotEmpty(final T collection, final String message) {
        return assertNotEmpty(collection, () -> message);
    }

    /**
     * Asserts the the given collection is not empty.
     * @param <T> type of the collection
     * @param collection teh collection to check
     * @param messageSupplier for the message of the exception in case of the {@code collection} is empty.
     * @return the collection
     * @throws IllegalArgumentException if and only if {@code collection} is empty */
    public static <T extends Collection> T assertNotEmpty(final T collection, final Supplier<String> messageSupplier) {
        if (collection == null || collection.isEmpty()) {
            throw new IllegalArgumentException(createMessage("empty", messageSupplier));
        }
        return collection;
    }
    
    /**
     * Asserts that the given instances are not equal.
     * @param <T> teh type to test
     * @param obj1 an object
     * @param obj2 the other object
     * @param message message of the exception in case of equal objects
     * @throws IllegalArgumentException if and only if {@code obj1} and {@code obj2} are not equal */
    public static <T> void assertNotEquals(final T obj1, final T obj2, final String message) {
        assertNotEquals(obj1, obj2, () -> message);
    }

    /**
     * Asserts that the given instances are not equal.
     * @param <T> the type to test
     * @param obj1 an object
     * @param obj2 the other object
     * @param messageSupplier for the message of the exception in case of equal objects
     * @throws IllegalArgumentException if and only if {@code obj1} and {@code obj2} are not equal */
    public static <T> void assertNotEquals(final T obj1, final T obj2, final Supplier<String> messageSupplier) {
        if (Objects.equals(obj1, obj2)) {
            throw new IllegalArgumentException(createMessage("not equal", messageSupplier));
        }
    }

    /**
     * Asserts that the given instances is {@code null}.
     * @param <T> th type to test to be {@code null}
     * @param obj the object to test
     * @param message message of the exception in case the object is not {@code null}
     * @throws IllegalArgumentException if and only if <em>obj</em> is not {@code null} */
    public static <T> void assertNull(final T obj, final String message) {
        assertNull(obj, () -> message);
    }

    /**
     * Asserts that the given instances is {@code null}.
     * @param <T> teh types to test to be {@code null}
     * @param obj the object to test
     * @param messageSupplier for the message of the exception in case the object is not {@code null}
     * @throws IllegalArgumentException if and only if <em>obj</em> is not {@code null} */
    public static <T> void assertNull(final T obj, final Supplier<String> messageSupplier) {
        if (obj != null) {
            throw new IllegalArgumentException(createMessage("not null", messageSupplier));
        }
    }

    /**
     * Asserts that the given instances is not {@code null}.
     * @param <T> the type to test not to be {@code null}
     * @param obj the object to test
     * @param message message of the exception in case the object is {@code null}
     * @return the given value
     * @throws IllegalArgumentException if and only if <em>obj</em> is  {@code null} */
    public static <T> T assertNotNull(final T obj, final String message) {
        return assertNotNull(obj, () -> message);
    }

    /**
     * Asserts that the given instances is not {@code null}.
     * @param <T> the type to test not to be {@code null}
     * @param obj the object to test
     * @param messageSupplier for the message of the exception in case the object is {@code null}
     * @return the given value
     * @throws ArgumentNullException if and only if <em>obj</em> is  {@code null} */
    public static <T> T assertNotNull(final T obj, final Supplier<String> messageSupplier) {
        if (obj == null) {
            throw new ArgumentNullException(createMessage(obj, messageSupplier));
        }
        return obj;
    }

    /** Asserts that the given value is between the minimum (include) and maximum (include).
     * @param value the vakue to test
     * @param min minimum (inclusive)
     * @param max maximum (inclusive)
     * @param message message if the value is not between minimum and maximum
     * @return the value
     * @throws IllegalArgumentException if and only if <em>value</em> is not between the {@code min} (include) and {@code max} (include) */
    public static int assertRange(final int value, final int min, final int max, final String message) {
        return assertRange(value, min, max, () -> message);
    }

    /** Asserts that the given value is between the minimum (include) and maximum (include).
     * @param value the vakue to test
     * @param min minimum (inclusive)
     * @param max maximum (inclusive)
     * @param messageSupplier for the message if the value is not between minimum and maximum
     * @return the value
     * @throws IllegalArgumentException if and only if <em>value</em> is not between the {@code min} (include) and {@code max} (include) */
    public static int assertRange(final int value, final int min, final int max, final Supplier<String> messageSupplier) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(createMessage(value, messageSupplier));
        }
        return value;
    }

    /** Asserts that the given value is between the minimum (include) and maximum (include).
     * @param value the vakue to test
     * @param min minimum (inclusive)
     * @param max maximum (inclusive)
     * @param message message if the value is not between minimum and maximum
     * @return the value
     * @throws IllegalArgumentException if and only if <em>value</em> is not between the {@code min} (include) and {@code max} (include) */
    public static long assertRange(final long value, final long min, final long max, final String message) {
        return assertRange(value, min, max, () -> message);
    }

    /** Asserts that the given value is between the minimum (include) and maximum (include).
     * @param value the vakue to test
     * @param min minimum (inclusive)
     * @param max maximum (inclusive)
     * @param messageSupplier for the message if the value is not between minimum and maximum
     * @return the value
     * @throws IllegalArgumentException if and only if <em>value</em> is not between the {@code min} (include) and {@code max} (include) */
    public static long assertRange(final long value, final long min, final long max, final Supplier<String> messageSupplier) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(createMessage(value, messageSupplier));
        }
        return value;
    }
    
    /**
     * Asserts that the given collection contains not {@code null} entry.
     * @param <T> the collection type
     * @param col the collection to test
     * @param message message for the exception if the given collection contains a {@code null} value
     * @return the given collection
     * @throws IllegalArgumentException if and only if <em>col</em> contains at least one value or <em>col</em> is {@code null} */
    public static <T extends Collection> T assertContainsNoNull(final T col, final String message) {
        return assertContainsNoNull(col, () -> message);
    }

    /**
     * Asserts that the given collection contains not {@code null} entry.
     * @param <T> the collection type
     * @param col the collection to test
     * @param messageSupplier for the message for the exception if the given collection contains a {@code null} value
     * @return the given collection
     * @throws IllegalArgumentException if and only if <em>col</em> contains at least one value or <em>col</em> is {@code null} */
    public static <T extends Collection> T assertContainsNoNull(final T col, final Supplier<String> messageSupplier) {
        assertNotNull(col, messageSupplier).forEach(obj -> assertNotNull(obj, messageSupplier));
        return col;
    }

    /**
     * Asserts that the given {@code String} contains not only whitspace characters.
     * @param string the String to test none whitespace characters
     * @param message message for the exception if the given string contains only whitespace characters
     * @return the string
     * @throws IllegalArgumentException if and only if <em>string</em> contains only whitespace characters or <em>string</em> is {@code null}
     */
    public static String assertNotBlank(final String string, final String message) {
        return assertNotBlank(string, () -> message);
    }

    /**
     * Asserts that the given {@code String} contains not only whitspace characters.
     * @param string the String to test none whitespace characters
     * @param messageSupplier supplier for message of the exception if the given string contains only whitespace characters
     * @return the string
     * @throws IllegalArgumentException if and only if <em>string</em> contains only whitespace characters or <em>string</em> is {@code null}
     */
    public static String assertNotBlank(final String string, final Supplier<String> messageSupplier) {
        assertNotNull(string, messageSupplier);
        if (!string.isBlank()) {
            return string;
        }
        throw new IllegalArgumentException(createMessage("blank", messageSupplier));
    }
    
    /**
     * Asserts that the given {@code CharSequence} matches the given {@code Pattern}.
     * @param sequence the CharSequence to test against the <em>regex</em>
     * @param regex the regex to test the <em>sequence</em> against
     * @param message  the message of the exception if the given <em>sequence</em> doesn't match against the <em>regex</em>
     * @return the char sequence
     * @throws IllegalArgumentException if and only if <em>sequence</em> doesn't mastch against the <em>regex</em> or if <em>sequence</em> or <em>regex</em> are {@code null}
     */
    public static CharSequence assertMatch(final CharSequence sequence, final Pattern regex, final String message) {
        return assertMatch(sequence, regex, () -> message);
    }

    /**
     * Asserts that the given {@code CharSequence} matches the given {@code Pattern}.
     * @param sequence the CharSequence to test against the <em>regex</em>
     * @param regex the regex to test the <em>sequence</em> against
     * @param messageSupplier supplier for message of the exception if the given <em>sequence</em> doesn't match against the <em>regex</em>
     * @return the char sequence
     * @throws IllegalArgumentException if and only if <em>sequence</em> doesn't mastch against the <em>regex</em> or if <em>sequence</em> or <em>regex</em> are {@code null}
     */
    public static CharSequence assertMatch(final CharSequence sequence, final Pattern regex, final Supplier<String> messageSupplier) {
        assertNotNull(regex, "Pattern is null");
        assertNotNull(sequence, "Sequence is null");
        if (Pattern.matches(regex.pattern(), sequence)) {
            return sequence;
        }
        throw new IllegalArgumentException(createMessage("not matching", messageSupplier));
    }

    /**
     * Asserts that the given {@code CharSequence} matches the given {@code Pattern}.
     * @param sequence the CharSequence to test against the <em>regex</em>
     * @param regex the regex to test the <em>sequence</em> against
     * @param message  the message of the exception if the given <em>sequence</em> doesn't match against the <em>regex</em>
     * @return the char sequence
     * @throws IllegalArgumentException if and only if <em>sequence</em> doesn't mastch against the <em>regex</em> or if <em>sequence</em> or <em>regex</em> are {@code null}
     */
    public static CharSequence assertMatch(final CharSequence sequence, final String regex, final String message) {
        return assertMatch(sequence, regex, () -> message);
    }

    /**
     * Asserts that the given {@code CharSequence} matches the given {@code Pattern}.
     * @param sequence the CharSequence to test against the <em>regex</em>
     * @param regex the regex to test the <em>sequence</em> against
     * @param messageSupplier supplier for message of the exception if the given <em>sequence</em> doesn't match against the <em>regex</em>
     * @return the char sequence
     * @throws IllegalArgumentException if and only if <em>sequence</em> doesn't mastch against the <em>regex</em> or if <em>sequence</em> or <em>regex</em> are {@code null} or <em>regex</em> is not a valid {@code java.util.regex.Pattern}.
     */
    public static CharSequence assertMatch(final CharSequence sequence, final String regex, final Supplier<String> messageSupplier) {
        assertNotNull(regex, "Pattern is null");
        return assertMatch(sequence, compile(regex), messageSupplier);
    }

    /**
     * Asserts that the given {@link Collection} has a size between the minimum (include) and maximum (include).
     * @param <T> the collection type
     * @param col the collection to test
     * @param min the minimum size value
     * @param max the maximum size valuee
     * @param message message if the value is not between minimum and maximum. May be {@code null}.
     * @return the given collection
     * @throws IllegalArgumentException if and only if {@link Collection#size()} is not between the {@code min} (include) and {@code max} (include) */
    public static <T extends Collection> T assertSize(final T col, final int min, final int max, final String message) {
        return assertSize(col, min, max, () -> message);
    }
    
    /**
     * Asserts that the given {@link Collection} has a size between the minimum (include) and maximum (include).
     * @param <T> the collection type
     * @param col the collection to test
     * @param min the minimum size value
     * @param max the maximum size valuee
     * @param messageSupplier supplier for the message if the value is not between minimum and maximum. May be {@code null}.
     * @return the given collection
     * @throws IllegalArgumentException if and only if {@link Collection#size()} is not between the {@code min} (include) and {@code max} (include) or th given collection is {@code null}
     * @see #assertRange(int, int, int, java.util.function.Supplier) 
     */
    public static <T extends Collection> T assertSize(final T col, final int min, final int max, final Supplier<String> messageSupplier) {
        assertNotNull(col, "Collection must be provided");
        assertRange(col.size(), min, max, messageSupplier);
        return col;
    }

    private static <T> String createMessage(final T value, final Supplier<String> messageSupplier) {
        if (messageSupplier != null) {
            final var message = messageSupplier.get();
            if (message != null) {
                return message;
            }
        }
        return valueOf(value);
    }
}
