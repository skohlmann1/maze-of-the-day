/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.security.SecureRandom;
import java.util.stream.Stream;

/**
 *
 * @author saschakohlmann
 * @param <T> type of the cell
 */
public interface Maze<T extends Cell> {
    
    Tiling tiling();

    Stream<T> cells();

    public default T randomCell() {
        final var nonNullCells = cells().filter(cell -> !cell.isNull()).toList();
        return nonNullCells.get(new SecureRandom().nextInt(nonNullCells.size()));
    }
}
