/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.domain.Mask.SimpleMaskType;
import static de.speexx.maze.domain.Mask.SimpleMaskType.EMPTY;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import static de.speexx.maze.common.Assertions.assertRange;
import static java.lang.String.format;


/**
 * 
 * @author saschakohlmann
 */
public final class MaskSupport {
    
    private MaskSupport() {}
    
    public static Mask<SimpleMaskType> loadTextMask(final InputStream input) {
        return new TextMaskLoader().loadMask(input);
    }    

    public static Mask<SimpleMaskType> loadImageMask(final InputStream input) {
        return new ImageMaskLoader().loadMask(input);
    }

    public static Mask<SimpleMaskType> loadMask(final InputStream input) {
        try (final var pushbackInput = new PushbackInputStream(input, 8);) {
            final var magic = new byte[8];
            pushbackInput.read(magic);
            pushbackInput.unread(magic);

            if (ImageMaskLoader.isMagicSupported(magic)) {
                return loadImageMask(pushbackInput);
            }
            return loadTextMask(pushbackInput);
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }


    public static Mask<SimpleMaskType> triangled(final int columns) {
        assertRange(columns, 6, 45000, () -> format("Columns value (%d) must be in range 6 - 45000", columns));
        final var mask = nullMask(new Dimension(columns % 2 == 0 ? columns / 2 : columns / 2 + 1, columns));
        triangled(mask);
        return mask;
    }

    public static Mask<SimpleMaskType> triangled(final Dimension dimension) {
        final var mask = nullMask(dimension);
        triangled(mask);
        return mask;
    }

    static MaskImpl<SimpleMaskType> triangled(final MaskImpl<SimpleMaskType> mask) {
        final var rowCount = mask.dimension().columns() % 2 == 0 ? mask.dimension().columns() / 2 - 1 : mask.dimension().columns() / 2;
        final var add = mask.dimension().columns() % 2;
        var maxColumnsLeft = rowCount;

        for (int row = 0 ; row < rowCount; row++) {
            for (int column = 0; column < maxColumnsLeft + add; column++) {
                final var leftCoordinate = new Coordinate(row, column);
                mask.set(leftCoordinate, EMPTY);
            }
            for (int column = mask.dimension().columns() - maxColumnsLeft - 1; column < mask.dimension().columns(); column++) {
                final var rightCoordinate = new Coordinate(row, column);
                mask.set(rightCoordinate, EMPTY);
            }
            maxColumnsLeft--;
        }
        
        mask.set(new Coordinate(mask.dimension().rows() -1, mask.dimension().columns() - 1), EMPTY);
        if (mask.dimension().rows() % 2 == 0) {
            mask.set(new Coordinate(mask.dimension().rows() - 1, 0), EMPTY);
        }
        
        return mask;
    }
    
    private static MaskImpl<SimpleMaskType> nullMask(final Dimension dimension) {
        return new MaskImpl(nullMaskTemplate(dimension));
    }
    
    private static List<List<SimpleMaskType>> nullMaskTemplate(final Dimension dimension) {
        final var listOfLists = new ArrayList<List<SimpleMaskType>>(dimension.rows());
        
        IntStream.range(0, dimension.rows()).forEach(row -> {
            final var rowList = new ArrayList<SimpleMaskType>(dimension.columns());
            IntStream.range(0, dimension.columns()).forEach(column -> {
                rowList.add(SimpleMaskType.AVAILABLE);
            });
            listOfLists.add(rowList);
        });
        
        return listOfLists;
    }
}
