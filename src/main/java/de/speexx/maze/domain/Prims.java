/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.security.SecureRandom;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Random;

import static de.speexx.maze.domain.Support.FETCH_UNLINKED_NEIGHBORS;

/**
 * Simplified <a href='https://en.wikipedia.org/wiki/Prim%27s_algorithm'>Prims algorithm</a>.
 * The algorithm doesn't support costs to get the next neighbor.
 * @see TruePrims
 * @author saschakohlmann
 */
public final class Prims implements MazeAlgorithm {

    private final Random random;

    public Prims() {
        this.random = new SecureRandom();
    }

    @Override
    public Maze on(final Maze maze, final Cell start) {
        final var active = new LinkedList<Cell>();
        active.push(start);
        
        while(!active.isEmpty()) {
            final var cell = randomCellFrom(active);
            final var availableNeighbors = FETCH_UNLINKED_NEIGHBORS.apply(cell);
            if (!availableNeighbors.isEmpty()) {
                final var neighbor = randomCellFrom(availableNeighbors);
                cell.linkWith(neighbor);
                active.push(neighbor);
            } else {
                active.remove(cell);
            }
        }
        
        return maze;
    }

    private Cell randomCellFrom(final LinkedList<Cell> neighbors) {
        final var randomIndex = this.random.nextInt(neighbors.size());
        return neighbors.get(randomIndex);
    }

    private Cell randomCellFrom(final Collection<Cell> neighbors) {
        final var cellArray = neighbors.toArray(new Cell[neighbors.size()]);
        final var randomNumber = this.random.nextInt(cellArray.length);
        return cellArray[randomNumber];
    }
}
