/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import static de.speexx.maze.domain.TriangleNeighborhood.OddEven.oddEvenFor;

/**
 *
 * @author saschakohlmann
 */
public enum TriangleNeighborhood implements Neighborhood {
    A,
    B,
    C;

    /**
     * Creates a trinagle neighborhood for {@link GridCell}s.
     * <p>
     * A triangle is defind like the following:
     * </p>
     * <pre>
     *    /\
     * A /  \ B
     *  /    \
     * +------+
     *    C
     * </pre>
     * <p>The rules of the connector combines for odd rows the cell at the odd
     * position with the following cell at the even position by the {@code B} side.
     * For the even cell with the following odd cell by the {@code A} side.
     * <pre>
     *              C
     *     /\   +--------+   /\
     *    /  \   \      /   /  \
     * A /    \ B \    / A /    \ B
     *  /      \   \  /   /      \
     * +--------+   \/   +--------+
     *     C                 C
     * </pre>
     * <p>For even rows the first form is defined as the even cell from the odd row.
     * This makes it possible to combine the odd cells from the odd and even rows or
     * the even cell for the even odd ros combination.</p>
     * <pre>
     *              C
     *     /\   +--------+   /\
     *    /  \   \      /   /  \
     * A /    \ B \    / A /    \ B
     *  /      \   \  /   /      \
     * +--------+   \/   +--------+
     *     C                 C
     * +--------+   /\   +--------+
     *  \      /   /  \   \      /
     * B \    / A /    \ B \    / A
     *    \  /   /      \   \  /
     *     \/   +--------+   \/
     *              C
     * </pre>
     */
    public static class TriangleNeighborhoodConnector implements NeighborhoodConnector<Grid> {
        @Override
        public void connect(final Grid grid) {
            grid.cells().forEach(cell -> {

                final var coordinate = cell.coordinate();
                final var cellRow = coordinate.row();
                final var cellColumn = coordinate.column();

                final var oddEven = oddEvenFor(cell);
                if (cellColumn != 0) {
                    final var cellLeft = grid.cellAt(new Coordinate(cellRow, cellColumn - 1));
                    if (oddEven.isOdd()) {
                        cell.withNeightborAt(A, cellLeft);
                    } else {
                        cell.withNeightborAt(B, cellLeft);
                    }
                }
                final var cellRight = grid.cellAt(new Coordinate(cellRow, cellColumn + 1));
                if (oddEven.isOdd()) {
                    cell.withNeightborAt(B, cellRight);
                } else {
                    cell.withNeightborAt(A, cellRight);
                }

                if (oddEven.isOdd()) {
                    final var cellBelow = grid.cellAt(new Coordinate(cellRow + 1, cellColumn));
                    cell.withNeightborAt(C, cellBelow);
                } else if (cellRow != 0) {
                    final var cellAbove = grid.cellAt(new Coordinate(cellRow - 1, cellColumn));
                    cell.withNeightborAt(C, cellAbove);
                }                    
            });
        }

        @Override
        public Tiling tiling() {
            return new Tiling("triangular", "", "3.3.3.3.3.3 (or 3\u2076) See https://en.wikipedia.org/wiki/Triangular_tiling");
        }
    }

    public static final NeighborhoodConnector<Grid> TRIANGLE_RIGHT_STANDARD_GRID = new TriangleRightNeighborhoodConnector();

    /**
     * Creates a trinagle neighborhood for {@link GridCell}s.
     * <p>
     * A triangle is defind like the following for odd rows:
     * </p>
     *    A
     *  +---+
     *   \  |
     *  C \ |B
     *     \|
     *      +
     *    A
     * <p>and the following for even rows:</p>
     * <pre>
     *  +
     *  |\
     * B| \ C
     *  |  \
     *  +---+
     *    A
     * </pre>
     * <p>The rules of the connector combines for odd rows the cell at the even
     * row cells like the following:</p>
     * <pre>
     *        A
     *  +   +---+
     *  |\   \  |
     * B| \ C \ |B
     *  |  \   \|
     *  +---+   +
     *    A
     * </pre>
     * <p>The resulting maze is build up like the following:</p>
     * <pre>
     *  +---+---+
     *  |\  |\  |
     *  | C B C B
     *  |  \|  \|
     *  +-A-+-A-+
     *  |\  |\  |
     *  | C B C B
     *  |  \|  \|
     *  +-A-+-A-+
     * </pre>
     */
    public static class TriangleRightNeighborhoodConnector extends TriangleNeighborhoodConnector {
        @Override
        public void connect(final Grid grid) {
            grid.cells().forEach(cell -> {

                final var coordinate = cell.coordinate();
                final var cellRow = coordinate.row();
                final var cellColumn = coordinate.column();

                if (cellRow % 2 == 0) {
                    if (cellRow != 0) {
                        final var cellAbove = grid.cellAt(new Coordinate(cellRow - 1, cellColumn));
                        cell.withNeightborAt(A, cellAbove);
                    }
                    final var cellRight = grid.cellAt(new Coordinate(cellRow + 1, cellColumn + 1));
                    cell.withNeightborAt(B, cellRight);
                    final var cellBelow = grid.cellAt(new Coordinate(cellRow + 1, cellColumn));
                    cell.withNeightborAt(C, cellBelow);
                    
                } else {
                    
                    final var cellAbove = grid.cellAt(new Coordinate(cellRow - 1, cellColumn));
                    cell.withNeightborAt(C, cellAbove);
                    final var cellBelow = grid.cellAt(new Coordinate(cellRow + 1, cellColumn));
                    cell.withNeightborAt(A, cellBelow);
                    if (cellColumn != 0) {
                        final var cellLeft = grid.cellAt(new Coordinate(cellRow - 1, cellColumn - 1));
                        cell.withNeightborAt(B, cellLeft);
                    }
                }
           });
        }
        @Override
        public Tiling tiling() {
            return new Tiling("triangular", "right", "3.3.3.3.3.3 (or 3\u2076) See https://en.wikipedia.org/wiki/Triangular_tiling");
        }
    }
    
    /**
     * The tetrakis cell organisation looks like the following.
     * 
     * <pre>
     *     A
     * +---+---+
     * |\  |  /|
     * | \ | / |
     * |  \|/  |
     * +---+---+ B
     * |  /|\  |
     * | / | \ |
     * |/  |  \|
     * +---+---+
     * </pre>
     * 
     * <p>Where {@link TriangleNeighborhood#A A} represents always the horizontal cathetus (leg) and
     * {@link TriangleNeighborhood#B B} represents always the vertical cathetus.
     * {@link TriangleNeighborhood#C C} represents always the hypotenuse.</p>
     * <p>The cell at the left cathetus in the first row is cell {@code {0,0}} and the cell at the
     * hypotenuse is {@code {0,1}}. The cell at the vertical cathetus if cell {@code {0,1}}
     * is {@code {0,2}} and the cell at the hypotenuse of {@code {0,2}} is cell {@code {0,3}}.</p>
     * 
     */
    public static class TetrakisSquareNeighborhoodConnector extends TriangleNeighborhoodConnector {
        @Override
        public void connect(final Grid grid) {
            grid.cells().forEach(cell -> {

                final var coordinate = cell.coordinate();
                final var cellRow = coordinate.row();
                final var cellColumn = coordinate.column();

                if (cellRow % 2 == 0) {
                    switch (cellColumn % 4) {
                        case 0 -> {
                            final var cellRightAbove = grid.cellAt(new Coordinate(cellRow, cellColumn + 1));
                            cell.withNeightborAt(C, cellRightAbove);
                            final var cellBelow = grid.cellAt(new Coordinate(cellRow + 1, cellColumn));
                            cell.withNeightborAt(A, cellBelow);
                            if (cellColumn != 0) {
                                final var cellLeft = grid.cellAt(new Coordinate(cellRow,cellColumn - 1));
                                cell.withNeightborAt(B, cellLeft);
                            }
                        }
                        case 1 -> {
                            if (cellRow != 0) {
                                final var cellAbove = grid.cellAt(new Coordinate(cellRow - 1, cellColumn));
                                cell.withNeightborAt(A, cellAbove);
                            }
                            final var cellRight = grid.cellAt(new Coordinate(cellRow, cellColumn + 1));
                            cell.withNeightborAt(B, cellRight);
                            final var cellLeftBelow = grid.cellAt(new Coordinate(cellRow, cellColumn - 1));
                            cell.withNeightborAt(C, cellLeftBelow);
                        }
                        case 2 -> {
                            if (cellRow != 0) {
                                final var cellAbove = grid.cellAt(new Coordinate(cellRow - 1, cellColumn));
                                cell.withNeightborAt(A, cellAbove);
                            }
                            final var cellRightBelow = grid.cellAt(new Coordinate(cellRow, cellColumn + 1));
                            cell.withNeightborAt(C, cellRightBelow);
                            final var cellLeft = grid.cellAt(new Coordinate(cellRow, cellColumn - 1));
                            cell.withNeightborAt(B, cellLeft);
                        }
                        default -> {
                            final var cellLeftAbove = grid.cellAt(new Coordinate(cellRow, cellColumn - 1));
                            cell.withNeightborAt(C, cellLeftAbove);
                            final var cellRight = grid.cellAt(new Coordinate(cellRow, cellColumn + 1));
                            cell.withNeightborAt(B, cellRight);
                            final var cellBelow = grid.cellAt(new Coordinate(cellRow + 1, cellColumn));
                            cell.withNeightborAt(A, cellBelow);
                        }
                    }
                } else {
                    switch (cellColumn % 4) {
                        case 0 -> {
                            final var cellAbove = grid.cellAt(new Coordinate(cellRow - 1, cellColumn));
                            cell.withNeightborAt(A, cellAbove);
                            final var cellRightBelow = grid.cellAt(new Coordinate(cellRow, cellColumn + 1));
                            cell.withNeightborAt(C, cellRightBelow);
                            if (cellColumn != 0) {
                                final var cellLeft = grid.cellAt(new Coordinate(cellRow, cellColumn - 1));
                                cell.withNeightborAt(B, cellLeft);
                            }
                        }
                        case 1 -> {
                            final var cellLeftAbove = grid.cellAt(new Coordinate(cellRow, cellColumn - 1));
                            cell.withNeightborAt(C, cellLeftAbove);
                            final var cellRight = grid.cellAt(new Coordinate(cellRow, cellColumn + 1));
                            cell.withNeightborAt(B, cellRight);
                            final var cellBelow = grid.cellAt(new Coordinate(cellRow + 1, cellColumn));
                            cell.withNeightborAt(A, cellBelow);
                        }
                        case 2 -> {
                            final var cellRightAbove = grid.cellAt(new Coordinate(cellRow, cellColumn + 1));
                            cell.withNeightborAt(C, cellRightAbove);
                            final var cellBelow = grid.cellAt(new Coordinate(cellRow + 1, cellColumn));
                            cell.withNeightborAt(A, cellBelow);
                            final var cellLeft = grid.cellAt(new Coordinate(cellRow, cellColumn - 1));
                            cell.withNeightborAt(B, cellLeft);
                        }
                        default -> {
                            final var cellAbove = grid.cellAt(new Coordinate(cellRow - 1, cellColumn));
                            cell.withNeightborAt(A, cellAbove);
                            final var cellRight = grid.cellAt(new Coordinate(cellRow, cellColumn + 1));
                            cell.withNeightborAt(B, cellRight);
                            final var cellLeftBelow = grid.cellAt(new Coordinate(cellRow, cellColumn - 1));
                            cell.withNeightborAt(C, cellLeftBelow);
                        }
                    }
                }
           });
        }
        @Override
        public Tiling tiling() {
            return new Tiling("tetrakis", "square", "V4.8.8, Conway: kisquadrille. See also https://en.wikipedia.org/wiki/Tetrakis_square_tiling");
        }
    }
    
    @Override
    public int maxNeighborhoods() {
        return values().length;
    }
    
    enum OddEven {
        ODD { @Override public boolean isOdd() { return true; } },
        EVEN { @Override public boolean isEven() { return true; } };
        
        public boolean isOdd() {
            return false;
        }
        public boolean isEven() {
            return false;
        }
        
        public static OddEven oddEvenFor(final GridCell cell) {
            final var sum = cell.coordinate().row() + cell.coordinate().column();
            final var modulo2 = sum % 2;
            final var oddEven = modulo2 == 0 ? ODD : EVEN;
            return oddEven;
        }
    }
}
