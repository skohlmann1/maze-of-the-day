/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

/**
 * 
 * @author saschakohlmann
 */
public final class Braid<T extends Cell> {
    
    private final Random random;
    private final float p;

    public Braid() {
        this(1.0f);
    }

    public Braid(final float p) {
        this.random = new SecureRandom();
        this.p = p;
    }

    public void on(final Stream<T> cells) {
        if (this.p < 1.0f) {
            cells.filter(cell -> cell.linkedWith().size() < 2)
                    .filter(cell -> cell.neighbors().size() > 1)
                    .filter(cell -> this.random.nextFloat() > this.p)
                    .forEach(cell -> {
                        final var fittingNeighbors = fittingNeighborsFor(cell);
                        final var bestNeighbors = bestNeighbors(fittingNeighbors);
                        final var neighbor = randomCellFrom(bestNeighbors);
                        cell.linkWith(neighbor);
                    });
        }
    }

    private List<T> bestNeighbors(final List<T> neighbors) {
        final var best = neighbors.stream().filter(neighbor -> neighbor.linkedWith().size() == 1).toList();
        if (best.isEmpty()) {
            return neighbors;
        }
        return best;
    }

    private List<T> fittingNeighborsFor(Cell cell) {
        final var neighbors = cell.neighbors().stream()
                .filter(neighbor -> !neighbor.linkedWith().contains(cell))
                .toList();
        return (List<T>) neighbors;
    }

    private T randomCellFrom(final List<T> neighbors) {
        return neighbors.get(this.random.nextInt(neighbors.size()));
    }

    @Override
    public String toString() {
        return "Braid{" + "p=" + p + '}';
    }
}
