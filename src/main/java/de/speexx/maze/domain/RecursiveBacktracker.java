/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.security.SecureRandom;
import java.util.ArrayDeque;

/**
 * Creates mazes with longer paths.
 * @author saschakohlmann
 * @see AldousBroder
 */
public final class RecursiveBacktracker implements MazeAlgorithm {

    @Override
    public Maze on(final Maze maze, final Cell start) {
        final var random = new SecureRandom();
        final var stack = new ArrayDeque<Cell>();
        stack.push(start);
        
        while(!stack.isEmpty()) {
            final var current = stack.getFirst();
            final var unlinkedNeighbors = current.neighbors().stream()
                    .filter(neighbor -> neighbor.linkedWith().isEmpty())
                    .toList();

            if (unlinkedNeighbors.isEmpty()) {
                stack.pop();
            } else {
                final var neighbor = unlinkedNeighbors.get(random.nextInt(unlinkedNeighbors.size()));
                current.linkWith(neighbor);
                stack.push(neighbor);
            }
        }
        
        return maze;
    }
}
