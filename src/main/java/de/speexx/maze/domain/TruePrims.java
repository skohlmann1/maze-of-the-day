/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.security.SecureRandom;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;

import static de.speexx.maze.domain.Support.FETCH_UNLINKED_NEIGHBORS;
import static java.lang.Integer.MAX_VALUE;

/**
 * <a href='https://en.wikipedia.org/wiki/Prim%27s_algorithm'>Prims algorithm</a>.
 * The algorithm support costs to get the next neighbor.
 * @see Prims
 * @author saschakohlmann
 */
public final class TruePrims implements MazeAlgorithm {

    private final Random random;

    public TruePrims() {
        this.random = new SecureRandom();
    }

    @Override
    public Maze on(final Maze maze, final Cell start) {
        final var active = new LinkedList<Cell>();
        active.push(start);
        
        final var costs = new HashMap<Cell, Integer>();
        maze.cells().forEach(cell -> costs.put((Cell) cell, nextCosts()));
        
        while(!active.isEmpty()) {
            final var cell = minimumCostCellFrom(costs, active);
            final var availableNeighbors = FETCH_UNLINKED_NEIGHBORS.apply(cell);
            if (!availableNeighbors.isEmpty()) {
                final var neighbor = minimumCostCellFrom(costs, availableNeighbors);
                cell.linkWith(neighbor);
                active.push(neighbor);
            } else {
                active.remove(cell);
            }
        }
        
        return maze;
    }

    private int nextCosts() {
        return this.random.nextInt(100);  
//        return (int) (this.random.nextGaussian() * 100d);
//        return (int) (this.random.nextExponential() * 100d);
    }

    private Cell minimumCostCellFrom(final Map<Cell, Integer> costs, final Collection<Cell> cells) {
        Cell currentMin = null;
        int currentCost = MAX_VALUE;

        for (final var cell : cells) {
            final var cost = costs.get(cell);
            if (cost < currentCost) {
                currentMin = cell;
                currentCost = cost;
            }
        }
        
        assert currentMin != null;
        return currentMin;
    }
}
