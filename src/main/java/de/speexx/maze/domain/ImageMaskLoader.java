/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.domain.Mask.SimpleMaskType;
import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.imageio.ImageIO;

import static de.speexx.maze.domain.Mask.SimpleMaskType.AVAILABLE;
import static de.speexx.maze.domain.Mask.SimpleMaskType.EMPTY;
import static de.speexx.maze.domain.Mask.SimpleMaskType.LINKED;
import static java.awt.Color.BLACK;
import static java.awt.Color.WHITE;
import java.util.stream.IntStream;

/**
 *
 * @author saschakohlmann
 */
final class ImageMaskLoader extends MaskLoader {
    
    private static final byte[] PNG_MAGIC = new byte[] {(byte) 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a};
    
    @Override
    public final Mask<SimpleMaskType> loadMask(final InputStream input) {
        try {
            final var image = ImageIO.read(input);
            final var rows = image.getHeight();
            final var columns = image.getWidth();

            final var rowList = new ArrayList<List<SimpleMaskType>>();

            IntStream.range(0, rows).forEach(y -> {
                final var columnList = new ArrayList<SimpleMaskType>();
                IntStream.range(0, columns).forEach(x -> {
                    final var color = new Color(image.getRGB(x, y));
                    final var cellType = mapColorToCellType(color);
                    columnList.add(cellType);
                });
                rowList.add(columnList);
            });
            return new MaskImpl<>(rowList);

        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    SimpleMaskType mapColorToCellType(final Color color) {
        if (color.equals(BLACK)) {
            return EMPTY;
        }
        if (color.equals(WHITE)) {
            return AVAILABLE;
        }
        return LINKED;
    }
    
    public static final boolean isMagicSupported(final byte[] magic) {
        return Arrays.equals(magic, PNG_MAGIC);
    }
}
