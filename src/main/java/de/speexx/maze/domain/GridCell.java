/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.common.Assertions;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author saschakohlmann
 */
public class GridCell extends NeighborCell{
    
    private final Coordinate coordinate;
    
    public GridCell(final Coordinate coordinate) {
        this.coordinate = Assertions.assertNotNull(coordinate, "Coordinate must be provided");
    }
    
    public Coordinate coordinate() {
        return this.coordinate;
    }

    @Override
    public GridCell neightborAt(final Neighborhood hoodOrientation) {
        final var cell = super.neightborAt(hoodOrientation);
        if (cell.isNull()) {
            return GridCell.NULL;
        }
        return (GridCell) cell;
    }
    
    @Override
    public int hashCode() {
        return name().hashCode();
    }

    @Override
    public String name() {
        return "[" + coordinate().row() + ":" + coordinate().column() + ']';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GridCell other = (GridCell) obj;
        return Objects.equals(this.coordinate, other.coordinate);
    }

    @Override
    public String toString() {
        return "GridCell{" + "coordinate=" + coordinate + ", neighborhood=" + neighborhoodAsString() + ", linkedTo=" + linkedToAsString() + '}';
    }

    final String neighborhoodAsString() {
        final var sb = new StringBuilder();
        neighborhoodOf(this).forEach((var neighborhood) -> {
            final var neighbor = neightborAt(neighborhood);
            sb.append(neighborhood.name() + "@[" + neighbor.coordinate().row() + ":" + neighbor.coordinate().column() + "] ");
        });
        
        return sb.toString();
    }
    
    private String linkedToAsString() {
        return linkedWith().stream().map(cell -> cell.name()).collect(Collectors.joining(" ", "[", "]"));
    }
    
    static final GridCell NULL = new GridCell(Coordinate.ZERO) {

        @Override
        public Coordinate coordinate() {
            throw new UnsupportedOperationException();
        }
       
        @Override
        public void linkWith(final Cell other) {throw new UnsupportedOperationException();}

        @Override
        public String name() {
            return toString();
        }

        @Override
        public int hashCode() {
            return "null".hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return this == obj;
        }

        @Override
        public String toString() {
            return "GridCell{NULL}";
        }
        
        @Override
        public Set<? extends GridCell> linkedWith() {
            return Set.of();
        }
        
        @Override
        public Set<? extends GridCell> neighbors() {
            return Set.of();
        }
        
        @Override
        public boolean isNull() {
            return true;
        }
    };
}
