/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static de.speexx.maze.common.Assertions.assertNotNull;
import static de.speexx.maze.common.Assertions.assertRange;

import static de.speexx.maze.domain.Cell.NullCell.NULL;


/**
 *
 * @author saschakohlmann
 * @param <T> type of the cell
 * @see NeighborCell
 */
public final class Distances<T extends Cell> {

    private final Map<T, Integer> distances = new HashMap<>();
    private final T root;
    
    public Distances(final T root) {
        this.root = assertNotNull(root, "Root Cell must be provided");
        addDistance(this.root, 0);
    }
    
    public void addDistance(final T cell, final int distance) {
        addDistance(new Distance(cell, distance));
    }

    public void addDistance(final Distance<T> distance) {
        this.distances.put(distance.cell(), distance.distance());
    }
        
    public Distance<T> distanceFor(final T cell) {
        return new Distance(cell, this.distances.get(cell));
    }
    
    public boolean contains(final T cell) {
        return this.distances.containsKey(cell);
    }
    
    public Stream<Distance<T>> distances() {
        return this.distances.entrySet().stream()
                .map(entry -> new Distance<T>(entry.getKey(), entry.getValue()))
                .sorted()
                .toList()
                .stream();
    }
    
    public Cell root() {
        return this.root;
    }
    
    public Distances<T> pathTo(final T goal) {
        var current = goal;
        
        final var dist = new Distances(root());
        if (root().equals(goal)) {
            return dist;
        }
        dist.addDistance(distanceFor(current));
        
        while (!current.equals(root())) {
            for (final var linked : current.linkedWith()) {
                final var linkedCell = (T) linked;
                if (distanceFor(linkedCell).compareTo(distanceFor(current)) < 0) {
                    dist.addDistance(distanceFor(linkedCell));
                    current = linkedCell;
                    break;
                }
            }
        }
        
        return dist;
    }
    
    public Distance<T> max() {
        final var entries = this.distances.entrySet();
        var longest = 0;
        Cell cell = NULL;
        for (final var entry : entries) {
            var next = entry.getValue();
            if (next > longest) {
                longest = next;
                cell = entry.getKey();
            }
        }
        
        return new Distance(cell, longest);
    }

    @Override
    public String toString() {
        return "Distances{" + "distances=" + distances + ", root=" + root + '}';
    }
    
    public static record Distance<T extends Cell>(T cell, int distance) implements Comparable<Distance<T>> {
        
        public Distance(final T cell, final int distance) {
            this.cell = assertNotNull(cell, "Cell must be provided");
            this.distance = assertRange(distance, 0, Integer.MAX_VALUE, () -> String.format("Distance out of range between 0 and %d", Integer.MAX_VALUE));
        }
        
        @Override
        public int compareTo(final Distance<T> o) {
            return Integer.compare(this.distance(), o.distance());
        }
    }
}
