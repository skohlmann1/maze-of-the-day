/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.security.SecureRandom;
import java.util.List;

import static de.speexx.maze.domain.GridCell.NULL;

/**
 * See <a href='http://people.cs.ksu.edu/~ashley78/wiki.ashleycoleman.me/index.php/Hunt_and_Kill_Algorithm.html'>Hunt and Kill Algorithm</a>
 * for additional information.
 * <p>Creates mid long paths.</p>
 * @author saschakohlmann
 */
public final class HuntAndKill implements MazeAlgorithm {

    @Override
    public Maze on(final Maze maze, final Cell start) {
        final var random = new SecureRandom();

        Cell current = start;
        
        while (!current.isNull()) {
            final var unvistitedNeighbors = current.neighbors().stream()
                    .filter(neighbor -> neighbor.linkedWith().isEmpty())
                    .toList();
            
            if (!unvistitedNeighbors.isEmpty()) {
                final var neighbor = unvistitedNeighbors.get(random.nextInt(unvistitedNeighbors.size()));
                current.linkWith(neighbor);
                current = neighbor;
            } else {
                current = NULL;
                for (final var cell : (List<Cell>) maze.cells().toList()) {
                    final var visitedNeighbors = cell.neighbors().stream()
                            .filter(neighbor -> !neighbor.isNull())
                            .filter(neighbor -> !neighbor.linkedWith().isEmpty())
                            .toList();
                    if (cell.linkedWith().isEmpty() && !visitedNeighbors.isEmpty()) {
                        current = cell;
                        final var neighbor = visitedNeighbors.get(random.nextInt(visitedNeighbors.size()));
                        current.linkWith(neighbor);
                        break;
                    }
                }
            }
        }

        return maze;
    }
}
