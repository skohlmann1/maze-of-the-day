/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import static de.speexx.maze.common.Assertions.assertRange;

/**
 * A coordinate in of a cell in a grid.
 * @see Grid
 * @see GridCell
 * @author saschakohlmann
 */
public record Coordinate(int row, int column) {
    
    public static final Coordinate ZERO = new Coordinate(0, 0);

    /**
     * {@code row} and {@code column} must be non-negative integers.
     * @param row non-negative integer
     * @param column non-negative integer
     * @throws IllegalArgumentException if and only if one of the given values is &lt; 0
     */
    public Coordinate(final int row, final int column) {
        this.row = assertRange(row, 0, Integer.MAX_VALUE, () -> String.format("Row value (%d) not in range 0 - %d", row, Integer.MAX_VALUE));
        this.column = assertRange(column, 0, Integer.MAX_VALUE, () -> String.format("Column value (%d) not in range 0 - %d", column, Integer.MAX_VALUE));
    }
}
