/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.MazeException;
import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonString;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static de.speexx.maze.domain.BaseGrid.nullCellGrid;
import static de.speexx.maze.common.Assertions.assertNotNull;
import static jakarta.json.Json.createValue;
import static jakarta.json.stream.JsonCollectors.toJsonArray;
import static java.lang.String.format;
import static java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME;
import static java.util.Optional.of;

/**
 *
 * @author saschakohlmann
 */
public final class MazeIO {
    
    private static final String PACKAGE_NAME = MazeIO.class.getPackageName() + ".";
    
    private MazeIO() {}
    
    /**
     * Serialize a grid into a JSON representation.
     * @param gridDistances a grid and an optional distance
     * @param output the stream to write the JSON representation to
     * @throws IOException if and only if the IO operations on the output stream failed
     */
    public static void write(final GridDistancesTuple gridDistances, final OutputStream output) throws IOException {
        final var cells = gridDistances.grid().cells().toList();
        final var generator = Json.createGenerator(output)
                .writeStartObject()
                    .writeStartObject("maze")
                        .write("version", "1")
                        .writeStartObject("header")
                            .write("generator", "MazeIO")
                            .write("generation-date", ZonedDateTime.now().format(ISO_ZONED_DATE_TIME))
                        .writeEnd()
                        .writeStartObject("dimension")
                            .write("rows", gridDistances.grid().dimension().rows())
                            .write("columns", gridDistances.grid().dimension().columns())
                        .writeEnd()
                        .writeStartObject("tiling")
                            .write("type", gridDistances.grid().tiling().type())
                            .write("sub-type", gridDistances.grid().tiling().subtype())
                            .write("description", gridDistances.grid().tiling().description())
                        .writeEnd()
                        .write("cells", cellArray(cells))
                        .write("neighborhoods", neighborhoodArray(cells))
                        .write("links", linkedWithArray(cells));

        gridDistances.distances().ifPresent(distances -> {
            generator.writeStartObject("path")
                    .write("start", identityToReference(System.identityHashCode(distances.root())))
                    .write("goal", identityToReference(System.identityHashCode(distances.max().cell())))
                    .writeEnd();
                            
        });

        generator.writeEnd()
                .writeEnd()
                .close();
    }
    
    private static JsonArray cellArray(final List<GridCell> cells) {
        return cells.parallelStream()
                .filter(cell -> !cell.isNull())
                .map(cell -> {
                    return Json.createObjectBuilder()
                        .add("id", identityToReference(System.identityHashCode(cell)))
                        .add("coordinate", coordinatesForCell(cell))
                        .build();
                }).collect(toJsonArray());
    }

    private static JsonArray neighborhoodArray(final List<GridCell> cells) {
        return cells.parallelStream()
                .filter(cell -> !cell.isNull())
                .map(cell -> {
                    return Json.createObjectBuilder()
                        .add("id", identityToReference(System.identityHashCode(cell)))
                        .add("neighbors", neighborsFor(cell))
                        .build();
                }).collect(toJsonArray());
    }

    private static JsonArray linkedWithArray(final List<GridCell> cells) {
        return cells.parallelStream()
                .filter(cell -> !cell.isNull())
                .map(cell -> {
                    return Json.createObjectBuilder()
                        .add("id", identityToReference(System.identityHashCode(cell)))
                        .add("refs", linkedWith(cell))
                        .build();
                }).collect(toJsonArray());
    }
    
    private static JsonObject coordinatesForCell(final GridCell cell) {
        return Json.createObjectBuilder()
                .add("row", cell.coordinate().row())
                .add("column", cell.coordinate().column())
                .build();
    }

    private static JsonArray linkedWith(final GridCell cell) {
        return cell.linkedWith().stream()
                .map(linkedCell -> createValue(identityToReference(System.identityHashCode(linkedCell))))
                .collect(toJsonArray());
    }
    
    private static JsonArray neighborsFor(final GridCell cell) {
        return cell.neighbors().stream()
                .map(neighbor -> {
                    return Json.createObjectBuilder()
                            .add("ref", identityToReference(System.identityHashCode(neighbor)))
                            .add("at", neighborTypesFor(cell.neighborhoodOf((GridCell) neighbor)))
                            .build();
                })
                .collect(toJsonArray());
    }

    private static JsonArray neighborTypesFor(final Set<Neighborhood> neighborhoods) {
        return neighborhoods.stream()
                .map(neighborhood -> {
                    return Json.createObjectBuilder()
                            .add("type", typeNameFor(neighborhood))
                            .add("name", neighborhood.name())
                            .build();
                })
                .collect(toJsonArray());
    }
    
    private static String typeNameFor(final Neighborhood neighborhood) {
        final var typeName = neighborhood.getClass().getName();
        if (typeName.startsWith(Neighborhood.class.getPackageName())) {
            return neighborhood.getClass().getSimpleName();
        }
        return typeName;
    }
    
    private static String identityToReference(final int identity) {
        return "#" + Integer.toString(identity, 36);
    }
    
    public static GridDistancesTuple read(final InputStream input) {
        final var reader = Json.createReader(input);
        final var jsonObject = reader.readObject();
        return gridFrom(jsonObject);
    }

    
    private static GridDistancesTuple gridFrom(final JsonObject rootObject) {

        final var mazeObject = rootObject.getJsonObject("maze");
        final var dimensionObject = mazeObject.getJsonObject("dimension");
        final var dimension = new Dimension(dimensionObject.getInt("rows"), dimensionObject.getInt("columns"));
        final var tilingObject = mazeObject.getJsonObject("tiling");
        final var tiling = new Tiling(tilingObject.getString("type"), tilingObject.getString("sub-type"), tilingObject.getString("description"));
        final var grid = nullCellGrid(dimension, tiling);
        
        final var cellArray = mazeObject.getJsonArray("cells");
        final var refCellMap = cellArrayToGrid(cellArray, grid);
        final var neighborhoodArray = mazeObject.getJsonArray("neighborhoods");
        neighborsToGrid(neighborhoodArray, grid, refCellMap);

        final var linksArray = mazeObject.getJsonArray("links");
        linksToGrid(linksArray, grid, refCellMap);
        
        final var pathObject = mazeObject.getJsonObject("path");
        if (pathObject != null) {
            final var distances = pathToDistance(pathObject, grid, refCellMap);
            return new GridDistancesTuple(grid, of(distances));
        }
        return new GridDistancesTuple(grid, Optional.empty());
    }
    
    private static Distances pathToDistance(final JsonObject pathObject, final BaseGrid grid, final Map<String, GridCell> cellRefs) {
        final var startCellRef = pathObject.getString("start");
        final var startCell = cellRefs.get(startCellRef);
        final var goalCellRef = pathObject.getString("goal");
        final var goalCell = cellRefs.get(goalCellRef);
        final var distances = startCell.distances();
        return distances.pathTo(goalCell);
    }
    
    private static void linksToGrid(final JsonArray linksArray, final BaseGrid grid, final Map<String, GridCell> cellRefs) {
        linksArray.stream()
                .map(value -> value.asJsonObject())
                .forEach(linkObject -> {
                     final var id = linkObject.getString("id");
                     final var cell = cellRefs.get(id);
                     final var cellRefArray = linkObject.getJsonArray("refs");
                     cellRefArray.stream()
                             .map(value -> (JsonString) value)
                             .map(refString -> cellRefs.get(refString.getString()))
                             .filter(refCell -> !cell.linkedWith().contains(refCell))
                             .forEach(refCell -> cell.linkWith(refCell));
                });
    }
   
    private static void neighborsToGrid(final JsonArray neighborhoodArray, final BaseGrid grid, final Map<String, GridCell> cellRefs) {
        neighborhoodArray.stream()
                .map(value -> value.asJsonObject())
                .forEach(neighborhoodObject -> {
                     final var id = neighborhoodObject.getString("id");
                     final var cell = cellRefs.get(id);
                     final var neighborArray = neighborhoodObject.getJsonArray("neighbors");
                     neighborArray.stream()
                             .map(value -> value.asJsonObject())
                             .forEach(neighborObject -> {
                                 final var neighborRef = neighborObject.getString("ref");
                                 final var neighbor = cellRefs.get(neighborRef);
                                 final var atArray = neighborObject.getJsonArray("at");
                                 atArray.stream()
                                         .map(value -> value.asJsonObject())
                                         .forEach(atObject -> {
                                             final var type = atObject.getString("type");
                                             final var name = atObject.getString("name");
                                             final var neighborhood = neighborhoodFor(type, name);
                                             cell.withNeightborAt(neighborhood, neighbor);
                                         });
                                 
                             });
        });
    }
    
    private static Neighborhood neighborhoodFor(final String typeName, final String name) {
        try {
            final var type = Class.forName(typeName.contains(".") ? typeName : PACKAGE_NAME + typeName);
            if (type.isEnum()) {
                return (Neighborhood) Enum.valueOf((Class<Enum>) type, name);
            }
            
            final var constructor = type.getConstructor(String.class);
            return (Neighborhood) constructor.newInstance(name);
            
        } catch (final ClassCastException | ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
            throw new MazeException(format("Unable to get type for name %s", typeName), ex);
        }
    }
    
    private static Map<String, GridCell> cellArrayToGrid(final JsonArray cellArray, final BaseGrid grid) {
        final var refMap = new HashMap<String, GridCell>();
        cellArray.stream()
                .map(value -> value.asJsonObject())
                .forEach(cellObject -> {
                    final var id = cellObject.getString("id");
                    final var coordinateObject = cellObject.getJsonObject("coordinate");
                    final var coordinate = new Coordinate(coordinateObject.getInt("row"), coordinateObject.getInt("column"));
                    final var cell = new GridCell(coordinate);
                    grid.put(cell);
                    refMap.put(id, cell);
        });
        
        return refMap;
    }

    public record GridDistancesTuple(BaseGrid grid, Optional<Distances<GridCell>> distances) {
        public GridDistancesTuple(final BaseGrid grid, Optional <Distances<GridCell>> distances) {
            this.grid = assertNotNull(grid, "BaseGrid must be provided");
            this.distances = assertNotNull(distances, "Optional<Distances> must be provided");
        }
    }
}
