/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.util.Optional;
import java.util.stream.IntStream;
import org.apiguardian.api.API;

import static de.speexx.maze.domain.ManhattenNeighborhood.EAST;
import static de.speexx.maze.domain.ManhattenNeighborhood.SOUTH;

/**
 *
 * @author saschakohlmann
 */
@API(status=API.Status.EXPERIMENTAL)
public final class ManhattenNeighborhoodBaseGridPrinter {
    
    private static final String PATH = "*";
    private static final String CORNER = "+";
    private static final String VERTICAL_LINE = "|";
    private static final String HORIZONTAL_LINE = "---";
    private static final String HORIZONTAL_FIELD = HORIZONTAL_LINE + CORNER;
    private static final String BODY = "   "; // 3 space (U+0020)
    private static final String BODY_WITH_PATH = " " + PATH + " ";
    
    private String lineSeparator = System.lineSeparator();

    public void print(final BaseGrid grid, final Writer writer) {
        print(grid, writer, Optional.empty());
    }

    public void print(final BaseGrid grid, final Writer writer, final Optional<Distances> distances) {
        try {
            final var sb = new StringBuilder();
            sb.append(CORNER);
            
            IntStream.range(0, grid.dimension().columns()).forEach(index -> sb.append(HORIZONTAL_FIELD));
            sb.append(this.lineSeparator);
            
            grid.rows().forEach(row -> {
                final var top = new StringBuilder(VERTICAL_LINE);
                final var bottom = new StringBuilder(CORNER);
                
                row.stream().forEach(cell -> {
                        final var links = cell.linkedWith();
                        final var body = body(distances, cell) + (links.contains(cell.neightborAt(EAST)) ? " " : VERTICAL_LINE);
                        top.append(body);
                        final var southBoundary = (links.contains(cell.neightborAt(SOUTH)) ? BODY : HORIZONTAL_LINE) + CORNER;
                        bottom.append(southBoundary);
                });
                sb.append(top);
                sb.append(this.lineSeparator);
                sb.append(bottom);
                sb.append(this.lineSeparator);
            });
            writer.write(sb.toString());
            writer.flush();
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }
    
    private String body(final Optional<Distances> distances, final Cell current) {
        if (distances.isPresent()) {
            if (distances.get().contains(current)) {
                return BODY_WITH_PATH;
            }
        }
        
        return BODY;
    }
}
