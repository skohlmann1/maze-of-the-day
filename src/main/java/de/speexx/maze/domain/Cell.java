/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author saschakohlmann
 */
public interface Cell {
    
    void linkWith(final Cell cell) throws UnsupportedLinkException;
    
    void unlinkFrom(final Cell cell);
    
    Set<? extends Cell> linkedWith();

    /** Neighbor cells of the this cell that may be linked.
     * <p>Implementations must not contain {@link Cell.NullCell#NULL} cells.</p>
     * @return all neighbor cells. Contains no {@link Cell.NullCell#NULL} cell.
     */
    Set<? extends Cell> neighbors();
    
    /**
     * Default implementation with <a href='https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm'>Dijkstra's algorithm</a>
     * @return all distances of not NULL cells reachable from this cell
     */
    public default Distances distances() {
        final var distances = new Distances(this);
        var frontier = new ArrayList<Cell>(List.of(this));
        
        while (!frontier.isEmpty()) {
            final var newFrontier = new ArrayList<Cell>();
            frontier.stream().forEach(cell -> {
                cell.linkedWith().forEach(linked -> {
                    if (!distances.contains(linked)) {
                        distances.addDistance(linked, distances.distanceFor(cell).distance() + 1);
                        newFrontier.add(linked);
                    }
                });
            });
            frontier = newFrontier;
        }        
        
        return distances;
    }
    
    String name();
    
    /**
     * A cell in a Maze must be unqiue identifiable against other cells.
     * @param other a cell
     * @return {@code true} if and only if the this cell and the other cell are equal.
     */
    @Override
    boolean equals(final Object other);

    /**
     * Support the equal hash contract.
     * @return the hash value
     */
    @Override
    int hashCode();
    
    /** Implements the NULL pattern.
     * @return default value is {@code true} */
    public default boolean isNull() {
        return true;
    }
    
    public static final class NullCell implements Cell {

        public static final Cell NULL = new NullCell();
        
        private NullCell() {}

        @Override
        public void linkWith(final Cell with) {throw new UnsupportedOperationException();}

        @Override
        public void unlinkFrom(final Cell from) {throw new UnsupportedOperationException();}


        @Override
        public int hashCode() {
            return "null".hashCode();
        }
        
        @Override
        public boolean equals(final Object other) {
            if (other == null) {
                return false;
            }
            return this.getClass() == other.getClass();
        }


        @Override
        public String toString() {
            return "Cell{NULL}";
        }
        
        @Override
        public String name() {
            return toString();
        }
        
        @Override
        public Set<Cell> linkedWith() {
            return Set.of();
        }
        
        @Override
        public Set<Cell> neighbors() {
            return Set.of();
        }
    }
}
