/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.util.ArrayList;
import static java.util.Arrays.asList;
import static java.util.Arrays.copyOfRange;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.apiguardian.api.API;

import static de.speexx.maze.common.Assertions.assertNotNull;

import de.speexx.maze.domain.Neighborhood.NeighborhoodConnector;
import java.util.List;

import static de.speexx.maze.domain.ManhattenNeighborhood.MANHATTEN_STANDARD_GRID;
import static de.speexx.maze.domain.GridCell.NULL;
import java.util.function.IntPredicate;
import java.util.function.IntUnaryOperator;

/**
 * @author saschakohlmann
 */
@API(status=API.Status.EXPERIMENTAL)
public sealed class BaseGrid implements Grid permits MaskedGrid {
    
    final GridCell[] cells;
    private final Dimension dimension;
    private final Tiling tiling;
    
    public BaseGrid(final Dimension dimension) {
        this(dimension, MANHATTEN_STANDARD_GRID);
    }

    /**
     * Generates a grid with tiling and cells 
     * @param dimension
     * @param tiling 
     */
    private BaseGrid(final Dimension dimension, final Tiling tiling) {
        this.tiling = assertNotNull(tiling, "Tiling must be provided");

        this.dimension = assertNotNull(dimension, "Dimension must be provided");
        final var rows = this.dimension.rows();
        final var columns = this.dimension().columns();
        this.cells = new GridCell[rows * columns];

        IntStream.range(0, rows).parallel().forEach(i -> {
            IntStream.range(0, columns).parallel().forEach(j -> {
                final var coordinate = new Coordinate(i, j);
                this.cells[offset(coordinate)] = NULL;
            });
        });
    }
    
    public BaseGrid(final Dimension dimension, final NeighborhoodConnector connector) {
        this(dimension, connector.tiling());

        final var rows = this.dimension().rows();
        final var columns = this.dimension().columns();

        IntStream.range(0, rows).parallel().forEach(i -> {
            IntStream.range(0, columns).parallel().forEach(j -> {
                final var coordinate = new Coordinate(i, j);
                put(new GridCell(coordinate));
            });
        });
        connector.connect(this);
    }
    
    @Override
    public final Dimension dimension() {
        return this.dimension;
    }
    
    @Override
    public Tiling tiling() {
        return this.tiling;
    }
    
    /**
     * Returns a cell at the given coordinate. If no cell is available at the
     * given coordinate a {@code Cell.NullCell} will be returned.
     * @param coordinate the coordinate
     * @return a grid cell or a {@code NullCell} if no cell is available at the given coordinates.
     * @throws NullPointerException if the given coordinate is {@code null}
     */
    @Override
    public final GridCell cellAt(final Coordinate coordinate) {
        if (coordinate.column() >= dimension().columns() || coordinate.row() >= dimension().rows()) {
            return GridCell.NULL;
        }
        final var offset = offset(coordinate);
        if (offset < 0 || offset >= this.cells.length) {
            return GridCell.NULL;
        }
        return this.cells[offset];
    }
    
    protected final GridCell removeCell(final GridCell reemovableCell) {
        final var coordinate = reemovableCell.coordinate();
        final var oldCell = cellAt(coordinate);

        oldCell.neighbors().stream()
                .map(neighbor -> (GridCell) neighbor)
                .forEach(neighbor -> {
                    neighbor.neighborhoodOf(oldCell).forEach(neighborHood -> {
                        neighbor.removeNeightborAt(neighborHood);
                        oldCell.neighborhoodOf(neighbor).stream()
                                .forEach(ownNeighborHood -> oldCell.removeNeightborAt(ownNeighborHood));
                    });
                });
        oldCell.linkedWith().stream().forEach(cell -> oldCell.unlinkFrom(cell));
        putCellAt(GridCell.NULL, coordinate);
        return oldCell;
    }

    final void put(final GridCell cell) {
        putCellAt(cell, cell.coordinate());
    }

    private void putCellAt(final GridCell cell, final Coordinate coordinate) {
        this.cells[offset(coordinate)] = cell;
    }

    private int offset(final Coordinate coordinate) {
        return dimension().columns() * coordinate.row() + coordinate.column();
    }
    
    /**
     * @return array of cells. may contain {@code NULL} cells.
     */
    public Stream<List<GridCell>> rows() {
        final var rowCount = dimension().rows();
        final var columnCount = dimension().columns();

        final var rows = new ArrayList<List<GridCell>>(rowCount);
        final IntPredicate isIndexLowerAsDimension = idx -> idx < dimension().size();
        final IntUnaryOperator indexCountUp = idx -> idx + columnCount;

        IntStream.iterate(0, isIndexLowerAsDimension, indexCountUp).forEachOrdered(idx -> {
            final var row = asList(copyOfRange(this.cells, idx, idx + columnCount));
            rows.add(row);
        });

        return rows.stream();
    }
    
    @Override
    public final Stream<GridCell> cells() {
        return rows().flatMap(list -> list.stream()).filter(cell -> !cell.isNull());
    }

    @Override
    public String toString() {
        return "BaseGrid{" + "dimension=" + dimension + ", tiling=" + tiling + '}';
    }

    static BaseGrid nullCellGrid(final Dimension dimension, final Tiling tiling) {
        return new BaseGrid(dimension, tiling);
    }
}
