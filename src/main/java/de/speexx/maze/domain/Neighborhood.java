/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

/**
 * Implementation defines the possible neighborhood of {@linkplain NeighborCell cells}.
 * It is convenient, that neighborhood implementations are defined as
 * {@link java.lang.Enum enum} instances.
 * @author saschakohlmann
 */
public interface Neighborhood {

    /** The name of the neighborhood.*/
    String name();
    
    /** The maximum amount of neighbors. */
    int maxNeighborhoods();
    
    public interface NeighborhoodConnector<T extends Maze> {
        void connect(final T maze);
        
        Tiling tiling();
    }
}
