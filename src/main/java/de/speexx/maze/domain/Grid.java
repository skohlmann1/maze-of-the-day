/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.util.ArrayList;
import java.util.stream.Stream;
import java.util.stream.IntStream;

/**
 *
 * @author saschakohlmann
 */
public interface Grid extends Maze<GridCell> {
    /**
     * Default implementation to get a stream of available none {@link GridCell#NULL} cells.
     * The stream may contains {@link GridCell#NULL} cells.
     * <p><strong>Implementation note:</strong> the implementation is inefficient an should
     * always be overridden by a more efficient implementation.</p>
     * @return a cell stream
     * @see #cellAt(de.speexx.maze.Coordinate)
     * @see #dimension()
     */
    @Override
    public default Stream<GridCell> cells() {
        final var list = new ArrayList<GridCell>();
        IntStream.range(0, dimension().rows()).parallel().forEach(i -> {
            IntStream.range(0, dimension().columns()).parallel()
                    .mapToObj(j -> cellAt(new Coordinate(i, j)))
                    .filter(cell -> !cell.isNull())
                    .forEach(cell -> list.add(cell));
        });
        
        return list.stream();
    }
    
    /**
     * The dimension of the grid.
     * @return the dimnension. Never {@code null}
     */
    Dimension dimension();
    
    /**
     * Returns a cell for the given coordinate. The given cell might be a {@link GridCell#NULL} cell. 
     * @param coordinate the coordinate for the cell to fetch
     * @return a cell
     */
    GridCell cellAt(final Coordinate coordinate);
    
}
