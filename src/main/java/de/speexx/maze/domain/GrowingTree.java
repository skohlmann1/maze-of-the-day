/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

import static de.speexx.maze.domain.Support.FETCH_UNLINKED_NEIGHBORS;
import static de.speexx.maze.common.Assertions.assertNotNull;

/**
 *
 * @author saschakohlmann
 */
public final class GrowingTree implements MazeAlgorithm {

    private static final Random RANDOM = new SecureRandom();
    
    public static final Function<LinkedList<Cell>, Cell> SAMPLE_SELECTOR = cellList -> {
        return randomCellFrom(cellList);
    };
    
    public static final Function<LinkedList<Cell>, Cell> LAST_SELECTOR = cellList -> {
        return cellList.getLast();
    };
    
    public static final Function<LinkedList<Cell>, Cell> RANDOM_SELECTOR = cellList -> {
        return RANDOM.nextInt(2) % 2 == 0 ? SAMPLE_SELECTOR.apply(cellList) : LAST_SELECTOR.apply(cellList);
    };
    
    private static final List<Function<LinkedList<Cell>, Cell>> SELECTORS = Arrays.asList(SAMPLE_SELECTOR, LAST_SELECTOR, RANDOM_SELECTOR);
    
    private Function<LinkedList<Cell>, Cell> nextCellSelector;
    
    public GrowingTree() {
        this(SELECTORS.get(RANDOM.nextInt(3)));
    }
    
    public GrowingTree(final Function<LinkedList<Cell>, Cell> nextCellSelector) {
        this.nextCellSelector = assertNotNull(nextCellSelector, () -> "Next cell selector must be provided");
    }
    
    
    @Override
    public Maze on(final Maze maze, final Cell start) {
        final var active = new LinkedList<Cell>();
        active.push(start);
        
        while(!active.isEmpty()) {
            final var cell = nextCellSelector.apply(active);
            final var availableNeighbors = FETCH_UNLINKED_NEIGHBORS.apply(cell);
            if (!availableNeighbors.isEmpty()) {
                final var neighbor = randomCellFrom(availableNeighbors);
                cell.linkWith(neighbor);
                active.push(neighbor);
            } else {
                active.remove(cell);
            }
        }
        
        return maze;
    }

    private Cell randomCellFrom(final Collection<Cell> neighbors) {
        final var cellArray = neighbors.toArray(new Cell[neighbors.size()]);
        final var randomNumber = RANDOM.nextInt(cellArray.length);
        return cellArray[randomNumber];
    }
    
    private static Cell randomCellFrom(final LinkedList<Cell> neighbors) {
        final var randomIndex = RANDOM.nextInt(neighbors.size());
        return neighbors.get(randomIndex);
    }    
}
