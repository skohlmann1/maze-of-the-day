/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import static de.speexx.maze.common.Assertions.assertLength;
import static de.speexx.maze.common.Assertions.assertNotNull;
import static java.lang.Integer.MAX_VALUE;

/**
 * Tiling defines the organization off a maze. Based on the geometric shapes of
 * euclidean uniform tiled {@linkplain Neighborhood neighborhoods} and there
 * combinations of possible tiles.
 * 
 * @author saschakohlmann
 */
public record Tiling(String type, String subtype, String description) {
    
    public Tiling(final String type, final String subtype, final String description) {
        assertNotNull(type, "Tiling type must be provided.");
        this.type = assertLength(type.strip(), 1, MAX_VALUE, () -> "Tiling name must contain at least one not whitespace character");
        this.subtype = subtype != null ? subtype.strip() : "";
        this.description = description != null ? description.strip() : "";
    }
}
