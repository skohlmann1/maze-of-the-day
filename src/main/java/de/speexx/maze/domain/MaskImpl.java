/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static de.speexx.maze.common.Assertions.assertNotNull;
import static de.speexx.maze.common.Assertions.assertNotEmpty;

/**
 *
 * @author saschakohlmann
 */
final class MaskImpl<T> implements Mask<T> {

    private final List<List<T>> maskValues;
    private final Dimension dimension;
    
    public MaskImpl(final List<List<T>> values) {
        assertNotNull(values, "Values must be provided");
        assertNotEmpty(values, "Values must contain at least 1 list");
        assertNotEmpty(values.get(0), "Value list must contain at least 1 entry");
        
        final int rows = values.size();
        final int columns = values.get(0).size();
        this.dimension = new Dimension(rows, columns);
        this.maskValues = values.stream()
                .peek(list -> {
                    if (list.size() != columns) {
                        throw new RowLengthMismatchException();
                    }
                })
                .map(innerList -> (List<T>) new ArrayList<T>(innerList))
                .toList();
    }
    
    @Override
    public Dimension dimension() {
        return this.dimension;
    }
    
    @Override
    public T configurationFor(final Coordinate coordinate) {
        return this.maskValues.get(coordinate.row()).get(coordinate.column());
    }
    
    void set(final Coordinate at, final T cellType) {
        final var row = this.maskValues.get(at.row());
        row.set(at.column(), cellType);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.maskValues);
        hash = 73 * hash + Objects.hashCode(this.dimension);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MaskImpl<?> other = (MaskImpl<?>) obj;
        if (!Objects.equals(this.maskValues, other.maskValues)) {
            return false;
        }
        return Objects.equals(this.dimension, other.dimension);
    }

    @Override
    public String toString() {
        return "MaskImpl{" + "maskValues=" + maskValues + ", dimension=" + dimension + '}';
    }
}
