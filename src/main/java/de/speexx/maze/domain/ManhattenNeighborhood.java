/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.domain.Neighborhood.NeighborhoodConnector;

/**
 *
 * @author saschakohlmann
 */
public enum ManhattenNeighborhood implements Neighborhood {
    NORTH,
    SOUTH,
    WEST,
    EAST;

    public static final NeighborhoodConnector MANHATTEN_STANDARD_GRID = new ManhattenNeighborhoodConnector();

    public static final class ManhattenNeighborhoodConnector implements NeighborhoodConnector<Grid> {    
        @Override
        public void connect(final Grid grid) {
            final var dimension = grid.dimension();
            grid.cells().sequential()
                .filter(cell -> !cell.isNull())
                .forEach(cell -> {
                    
                    final var coordinate = cell.coordinate();
                    final var cellRow = coordinate.row();
                    final var cellColumn = coordinate.column();

                    if (cellRow != 0) {
                        final var northCell = grid.cellAt(new Coordinate(cellRow - 1, cellColumn));
                        cell.withNeightborAt(NORTH, northCell);
                    }
                    if (cellRow < dimension.rows() - 1) {
                        final var southCell = grid.cellAt(new Coordinate(cellRow + 1, cellColumn));
                        cell.withNeightborAt(SOUTH, southCell);
                    }

                    if (cellColumn != 0) {
                        final var westCell = grid.cellAt(new Coordinate(cellRow, cellColumn - 1));
                        cell.withNeightborAt(WEST, westCell);
                    }
                    if (cellColumn < dimension.columns() - 1) {
                        final var eastCell = grid.cellAt(new Coordinate(cellRow, cellColumn + 1));
                        cell.withNeightborAt(EAST, eastCell);
                    }
                });
        }

        @Override
        public Tiling tiling() {
            return new Tiling("square", "", "4.4.4.4 or 4\u2074.  A.k.a. quadrille. Rectangle dimension possible. See also https://en.wikipedia.org/wiki/Square_tiling");
        }
    }

    @Override
    public int maxNeighborhoods() {
        return values().length;
    }
}
