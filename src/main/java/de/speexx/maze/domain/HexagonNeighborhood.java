/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

/**
 *
 * @author saschakohlmann
 */
public enum HexagonNeighborhood implements Neighborhood {

    NORTH,
    NORTH_EAST,
    SOUTH_EAST,
    SOUTH,
    SOUTH_WEST,
    NORTH_WEST;
    
    @Override
    public int maxNeighborhoods() {
        return 6;
    }
    
    public static final NeighborhoodConnector HEXAGON_STANDARD_GRID = new HexagonalNeighborhoodConnector();

    public static final class HexagonalNeighborhoodConnector implements NeighborhoodConnector<Grid> {

        @Override
        public void connect(final Grid grid) {
            grid.cells().sequential()
                .filter(cell -> !cell.isNull())
                .forEach(cell -> {
                    
                    final var coordinate = cell.coordinate();
                    final var cellRow = coordinate.row();
                    final var cellColumn = coordinate.column();
                    
                    if (cellRow > 1) {
                        final var northCell = grid.cellAt(new Coordinate(cellRow - 2, cellColumn));
                        cell.withNeightborAt(NORTH, northCell);
                    }
                    final var southCell = grid.cellAt(new Coordinate(cellRow + 2, cellColumn));
                    cell.withNeightborAt(SOUTH, southCell);

                    if (cellRow % 2 == 0) {
                        if (cellRow != 0) {
                            final var northWestCell = grid.cellAt(new Coordinate(cellRow - 1, cellColumn));
                            cell.withNeightborAt(NORTH_WEST, northWestCell);
                            final var northEastCell = grid.cellAt(new Coordinate(cellRow - 1, cellColumn + 1));
                            cell.withNeightborAt(NORTH_EAST, northEastCell);
                        }
                        final var southWestCell = grid.cellAt(new Coordinate(cellRow + 1, cellColumn));
                        cell.withNeightborAt(SOUTH_WEST, southWestCell);
                        final var southEastCell = grid.cellAt(new Coordinate(cellRow + 1, cellColumn + 1));
                        cell.withNeightborAt(SOUTH_EAST, southEastCell);
                    } else if (cellRow % 2 == 1) {
                        if (cellColumn != 0) {
                            final var northWestCell = grid.cellAt(new Coordinate(cellRow - 1, cellColumn - 1));
                            cell.withNeightborAt(NORTH_WEST, northWestCell);
                            final var southWestCell = grid.cellAt(new Coordinate(cellRow + 1, cellColumn - 1));
                            cell.withNeightborAt(SOUTH_WEST, southWestCell);
                        }
                        final var northEastCell = grid.cellAt(new Coordinate(cellRow - 1, cellColumn));
                        cell.withNeightborAt(NORTH_EAST, northEastCell);
                        final var southEastCell = grid.cellAt(new Coordinate(cellRow + 1, cellColumn));
                        cell.withNeightborAt(SOUTH_EAST, southEastCell);
                    }
                });
        }

        @Override
        public Tiling tiling() {
            return new Tiling("hexagonal", "", "6.6.6 or 6\u00B3. See also https://en.wikipedia.org/wiki/Hexagonal_tiling");
        }
    }
}
