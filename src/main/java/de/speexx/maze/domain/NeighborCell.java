/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */package de.speexx.maze.domain;

import java.util.HashMap;
import java.util.HexFormat;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Set.copyOf;

/**
 *
 * @author saschakohlmann
 */
public class NeighborCell implements Cell {
    
    private final Map<Neighborhood, NeighborCell> neighbors = new HashMap<>();
    private final Map<String, NeighborCell> links = new HashMap<>();

    /** @param cell must be of type {@code GridCell}. */
    @Override
    public void linkWith(final Cell cell) {
        if (cell == null || !neighbors().contains(cell)) {
            throw new UnsupportedLinkException(String.format("%s is not neighbor of this cell %s", cell, this));
        }
        linkWith((NeighborCell) cell, true);
    }
    
    void linkWith(final NeighborCell cell, final boolean bidi) {
        if (cell.isNull()) {
            return;
        }

        this.links.put(cell.name(), cell);
        if (bidi) {
            cell.linkWith(this, false);
        }
    }
    
    /** @param from must be of type {@code GridCell}. */
    @Override
    public void unlinkFrom(final Cell from) {
        unlink((NeighborCell) from, true);
    }
    
    private void unlink(final NeighborCell from, final boolean bidi) {
        if (from.isNull()) {
            return;
        }
        this.links.remove(from.name());
        if (bidi) {
            from.unlink(this, false);
        }
    }
    
    @Override
    public Set<? extends NeighborCell> linkedWith() {
        return Set.copyOf(this.links.values());
    }

    public NeighborCell neightborAt(final Neighborhood hoodOrientation) {
        return this.neighbors.getOrDefault(hoodOrientation, NULL);
    }
    
    /**
     * Add a new neighbor to the cell to generate a neighborhood relation.
     * In graph theory this defines an edge between the vertices (cells).
     * <p>The type of {@link Neighborhood} for a given cell should not be mixed.
     * This means, that a given cell should not contain a neighborhood of type
     * {@link Neighborhood A} and a neighborhood of type {@code B}.</p>
     * @param neighborhood the neighborhood
     * @param neighbor the neighbor of the new relation.
     *                 If {@code this} cell and <em>neighbor</em> are the same instance, nothing happen.
     * @return return {@code this} cell
     */
    public NeighborCell withNeightborAt(final Neighborhood neighborhood, final NeighborCell neighbor) {
        if (!neighbor.isNull() && neighbor != this) {
            this.neighbors.put(neighborhood, neighbor);
        }
        return this;
    }

    public NeighborCell removeNeightborAt(final Neighborhood hoodOrientation) {
        this.neighbors.remove(hoodOrientation);
        return this;
    }
    
    @Override
    public Set<? extends NeighborCell> neighbors() {
        return Set.copyOf(this.neighbors.values());
    }
    
    public Set<Neighborhood> neighborhoodOf(final NeighborCell cell) {
        if (cell == this) {
            return copyOf(this.neighbors.keySet());
        }
        return this.neighbors.entrySet().stream()
                .filter(entry -> entry.getValue().equals(cell))
                .map(entry -> entry.getKey())
                .collect(Collectors.toSet());
    }

    @Override
    public boolean isNull() {
        return false;
    }
    
    @Override
    public String name() {
        return HexFormat.of().toHexDigits(System.identityHashCode(this));
    }
    
    private static final NeighborCell NULL = new NeighborCell() {

        @Override
        public void linkWith(final Cell other) {throw new UnsupportedOperationException();}

        @Override
        public String name() {
            return toString();
        }

        @Override
        public int hashCode() {
            return "null".hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return this == obj;
        }

        @Override
        public String toString() {
            return "NeighborCell{NULL}";
        }
        
        @Override
        public Set<? extends NeighborCell> linkedWith() {
            return Set.of();
        }
        
        @Override
        public Set<? extends NeighborCell> neighbors() {
            return Set.of();
        }
        
        @Override
        public boolean isNull() {
            return true;
        }
    };
}
