/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import static de.speexx.maze.domain.ManhattenNeighborhood.MANHATTEN_STANDARD_GRID;
import de.speexx.maze.domain.Mask.SimpleMaskType;
import static de.speexx.maze.domain.Mask.SimpleMaskType.EMPTY;
import static de.speexx.maze.domain.Mask.SimpleMaskType.LINKED;
import de.speexx.maze.domain.Neighborhood.NeighborhoodConnector;
import static de.speexx.maze.common.Assertions.assertNotNull;


/**
 * Currently only {@link ManhattenNeighborhood} is supported.
 * @author saschakohlmann
 */
public final class MaskedGrid extends BaseGrid {
    
    private final Mask<SimpleMaskType> mask;

    public MaskedGrid(final Mask<SimpleMaskType> mask) {
        this(mask, MANHATTEN_STANDARD_GRID);
    }

    public MaskedGrid(final Mask<SimpleMaskType> mask, final NeighborhoodConnector connector) {
        super(assertNotNull(mask, "Mask must be provided").dimension(), connector);
        
        cells().map(cell -> cell.coordinate()).forEach(coordinate -> {
            final var type = mask.configurationFor(coordinate);
            final var cell = cellAt(coordinate);
            if (type == EMPTY) {
                removeCell(cell);
            } else if (type == LINKED) {
                cellAt(coordinate).neighbors().stream()
                        .map(neighbor -> (GridCell) neighbor)
                        .map(neighbor -> neighbor.coordinate())
                        .forEach(neighborCoordinate -> {
                            final var maskType = mask.configurationFor(coordinate);
                            if (maskType == LINKED) {
                                cellAt(coordinate).linkWith(cellAt(neighborCoordinate));
                            }
                        });
            }
        });
        this.mask = mask;
    }

    protected Mask<SimpleMaskType> mask() {
        return this.mask;
    }
}
