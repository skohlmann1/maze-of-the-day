/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.util.Collection;
import java.util.HashSet;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

/**
 *
 * @author saschakohlmann
 */
public final class Support {
    
    private Support() {}
    
    public static Collection<GridCell> deadEnds(final Stream<GridCell> cells) {
        return cells.parallel().filter(cell -> cell.linkedWith().size() == 1).toList();
    }

    /**
     * Detects all cells of a grid where a cell with neighborhoods as not a full list of neighbors.
     * The implementation currently assumes that a cell A has only one neighbor type like
     * {@link ManhattenNeighborhood}. If a cell has mixed neighbor relations
     * like with {@link Neighborhood A} and {@link Neighborhood B} the behavior is not
     * defined.
     * @param grid the grid to test for neighbors
     * @return collection of neighbors. 
     */
    public static final Collection<GridCell> borderCells(final Grid grid) {
        final var borderCells = new HashSet<GridCell>();
        grid.cells()
                .filter(cell -> !cell.isNull())
                .forEach(cell -> {
                    final var neighborhoods = cell.neighborhoodOf(cell);
                    neighborhoods.stream().findFirst().ifPresent(neighborhood -> {
                        if (neighborhoods.size() < neighborhood.maxNeighborhoods()) {
                            borderCells.add(cell);
                        }
                    });
                });
        return borderCells;
                
    }

    public static Function<Cell, Collection<Cell>> FETCH_UNLINKED_NEIGHBORS = cell -> {
        return cell.neighbors().stream()
                    .filter(neighbor -> neighbor.linkedWith().isEmpty())
                    .map(unlinked -> (GridCell) unlinked)
                    .collect(toSet());
    };
}
