/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.domain.Mask.SimpleMaskType;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import static de.speexx.maze.domain.Mask.SimpleMaskType.AVAILABLE;
import static de.speexx.maze.domain.Mask.SimpleMaskType.EMPTY;
import static de.speexx.maze.domain.Mask.SimpleMaskType.LINKED;

/**
 *
 * @author saschakohlmann
 */
final class TextMaskLoader extends MaskLoader {
    
    @Override
    public Mask<SimpleMaskType> loadMask(final InputStream input) {
        final var reader = new InputStreamReader(input);
        final var buffered = new BufferedReader(reader);
        final var rowStringStreams = buffered.lines();
        final var cellTypeListList = rowStringStreams.map(rowString -> toCellTypeList(rowString)).toList();
        return new MaskImpl<>(cellTypeListList);
    }
    
    
    List<SimpleMaskType> toCellTypeList(final String row) {
        return row.chars().mapToObj(character -> mapCharacterToCellType(character)).toList();
    }
    
    SimpleMaskType mapCharacterToCellType(final int character) {
        if (character == 'x' || character == 'X' || character == '\u00D7') {
            return EMPTY;
        }
        if (character == 'o' || character == 'O' || character == '0') {
            return LINKED;
        }
        return AVAILABLE;
    }
}
