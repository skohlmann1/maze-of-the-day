/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import static de.speexx.maze.common.Assertions.assertRange;

/**
 * The dimension of e.g. a {@link Grid}.
 * @author saschakohlmann
 */
public record Dimension(int rows, int columns) {
    
    private static final int MIN = 1;
    private static final int MAX = 45000;
    
    /**
     * {@code rows} and {@code columns} must be positive integers not greater 45000.
     * @param rows positive integer not greater 45000
     * @param columns positive integer not greater 45000
     * @throws IllegalArgumentException if and only if one of the given values is &lt; 1 or &gt; 45000
     */
    public Dimension(final int rows, final int columns) {
        this.rows = assertRange(rows, MIN, MAX, () -> String.format("Row value not in range %d - %d", MIN, MAX));
        this.columns = assertRange(columns, MIN, MAX, () -> String.format("Column value not in range %d - %d", MIN, MAX));
    }

    /**
     * The size of the dimension.
     * @return {@link #rows() rows()} × {@link #columns() columns()}
     */
    public int size() {
        return this.rows() * this.columns();
    }
}
