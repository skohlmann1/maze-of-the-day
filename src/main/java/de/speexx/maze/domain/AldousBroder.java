/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import java.security.SecureRandom;
import java.util.Collection;
import java.util.Random;

/**
 * Creates mazes with shorter paths.
 * @author saschakohlmann
 * @see RecursiveBacktracker
 */
public final class AldousBroder implements MazeAlgorithm {

    private final Random random;

    public AldousBroder() {
        this.random = new SecureRandom();
    }

    @Override
    public Maze on(final Maze maze, final Cell start) {
        Cell cell = start;
        var unvisited = noneNullCells(maze) - 1;

        while (unvisited > 0) {
            final var neighbors = cell.neighbors();
            final var neighbor = randomCellFrom(neighbors);
            
            if (neighbor.linkedWith().isEmpty()) {
                cell.linkWith(neighbor);
                unvisited--;
            }
            cell = neighbor;
        }
        
        return maze;
    }
    
    private Cell randomCellFrom(final Collection<? extends Cell> neighbors) {
        final var cellArray = neighbors.toArray(new Cell[neighbors.size()]);
        final var randomNumber = this.random.nextInt(cellArray.length);
        return cellArray[randomNumber];
    }

    // Note: this implementation is very inefficient and should be optimized.
    int noneNullCells(final Maze grid) {
        return (int) grid.cells().count();
    }
}
