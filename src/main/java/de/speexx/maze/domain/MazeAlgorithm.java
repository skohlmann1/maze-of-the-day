/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

/**
 * There are several maze creation algorithms available. Such algorithms should
 * implement this interface.
 * @author saschakohlmann
 */
@FunctionalInterface
public interface MazeAlgorithm {
    
    public default Maze on(final Maze maze) {
        return on(maze, maze.randomCell());
    }

    Maze on(final Maze maze, final Cell start);
}
