/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app;

import de.speexx.maze.app.command.GridMazeGenerator;
import de.speexx.maze.app.command.MazeToImage;
import de.speexx.maze.app.command.PathFinder;
import de.speexx.maze.app.command.GridAnalyzer;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import de.speexx.maze.app.command.Braider;
import de.speexx.maze.app.command.ShowTilings;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.Optional;
import java.util.function.Function;


/**
 *
 * @author saschakohlmann
 */
public final class App {
    
    @Parameter(names = { "-h", "--help"}, description = "Prints the help", help = true)
    private boolean help = false;

    @Parameter(names = { "-l", "--list"}, description = "Lists all given coands with a short description.")
    private boolean list = false;
    
    @Parameter(names = { "-v", "--verbose"}, description = "Prints verbose log messages")
    private boolean verbose = false;
    
    /**
     * @param args the command line arguments
     */
    public static void main(final String... args) {
        new App().run(args);
    }
    
    private void run(final String... args) {
        try {
            final var generator = new GridMazeGenerator();
            final var braider = new Braider();
            final var path = new PathFinder();
            final var image = new MazeToImage();
            final var analyze = new GridAnalyzer();
            final var showTilings = new ShowTilings();

            final var jc = JCommander.newBuilder()
                    .addObject(this)
                    .addCommand("grid", generator)
                    .addCommand("braid", braider)
                    .addCommand("path", path)
                    .addCommand("image", image)
                    .addCommand("analyze", analyze, "analyse")
                    .addCommand("tiling", showTilings)
                    .allowAbbreviatedOptions(true)
                    .allowParameterOverwriting(true)
                    .build();
            jc.setCaseSensitiveOptions(true);
            jc.setProgramName("maze");
            jc.setColumnSize(120);
            jc.parse(args);

            if (this.list) {
                System.out.format("%s commands:%n%n", jc.getProgramName());
                final var formatter = jc.getUsageFormatter();
                jc.getCommands().keySet().stream().forEach(commandName -> {
                    System.out.format("%s - %s%n", commandName, formatter.getCommandDescription(commandName));
                });
                System.exit(0);
            }
           
            if (this.help) {
                jc.usage();
                System.exit(0);
            }
            if (null == jc.getParsedCommand()) {
                System.err.format("No command found%n");
                System.err.format("Use 'maze -h' for more options.%n");
                System.exit(2);
            } else switch (jc.getParsedCommand()) {
                case "grid" -> generator.execute();
                case "braid" -> braider.execute();
                case "path" -> path.execute();
                case "image" -> image.execute();
                case "analyze" -> analyze.execute();
                case "tiling" -> showTilings.execute();
                default -> System.exit(1);
            }
        } catch (final RuntimeException ex) {
            System.err.format("A failure occured: %s%n", ex.getMessage());
            if (this.verbose) {
                ex.printStackTrace(System.err);
            }
            System.exit(1);
        }
        System.exit(0);
    }
    
    /**
     * Returns an {@link OutputStream} from the given path if and only if the path is available.
     * {@link System#out} otherwise,
     */
    public static Function<Optional<Path>, OutputStream> PATH_TO_OUTPUT_STREAM = optionalPath -> {
        if (optionalPath.isEmpty()) {
            return System.out;
        }
        try {
            return new FileOutputStream(optionalPath.get().toFile());
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    };

    /**
     * Returns an {@link InputStream} from the given path if and only if the path is available.
     * {@link System#in} otherwise,
     */
    public static Function<Optional<Path>, InputStream> PATH_TO_INPUT_STREAM = optionalPath -> {
        if (optionalPath.isEmpty()) {
            return System.in;
        }
        try {
            return new FileInputStream(optionalPath.get().toFile());
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    };
}
