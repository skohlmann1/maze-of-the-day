/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import de.speexx.maze.domain.BaseGrid;
import de.speexx.maze.domain.Distances;
import de.speexx.maze.domain.MazeIO.GridDistancesTuple;
import de.speexx.maze.domain.GridCell;
import de.speexx.maze.domain.Support;
import jakarta.json.Json;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UncheckedIOException;

/**
 *
 * @author saschakohlmann
 */
@Parameters(commandNames = {"analyze", "analyse", "analyzer", "analyser"}, commandDescription = "Prints some maze metrics.")
public class GridAnalyzer extends GridIoCommand {

    @ParametersDelegate
    private GridAnalyzerExecutionContext ec = new GridAnalyzerExecutionContext();

    @Override
    public void execute() {
        final var gridDistanceTuple = readTupleFrom(this.ec.inputStream());
        final var grid = gridDistanceTuple.grid();
        final var distances = distancesFor(gridDistanceTuple);
        
        if (!this.ec.isPrintJson()) {
            printRaw(distances, grid);
        } else {
            printJson(distances, grid);
        }
    }

    private void printJson(final Distances distances, final BaseGrid grid) {
        try (final var output = this.ec.outputStream();
             final var generator = Json.createGenerator(output).writeStartObject()) {
            
            if (this.ec.isPrintPathLength()) {
                generator.write(PATH_LENGTH, distances.max().distance());
            }
            if (this.ec.isPrintDeadEnds()) {
                generator.write(DEADENDS, Support.deadEnds(grid.cells()).size());
            }
            if (this.ec.isPrintDimension()) {
                generator.write(COLUMNS, grid.dimension().columns());
                generator.write(ROWS, grid.dimension().rows());
            }
            if (this.ec.isPrintCells()) {
                generator.write(CELLS, grid.cells().filter(cell -> !cell.isNull()).count());
            }
            if (this.ec.isPrintStart()) {
                final var row = ((GridCell) distances.root()).coordinate().row();
                final var column = ((GridCell) distances.root()).coordinate().column();
                generator.write(START_COLUMN, column);
                generator.write(START_ROW, row);
            }
            if (this.ec.isPrintGoal()) {
                final var row = ((GridCell) distances.max().cell()).coordinate().row();
                final var column = ((GridCell) distances.root()).coordinate().column();
                generator.write(GOAL_COLUMN, column);
                generator.write(GOAL_ROW, row);
            }
            
            generator.writeEnd();

        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    private void printRaw(final Distances distances, final BaseGrid grid) {
        try (final var configOut = this.ec.outputStream();
             final var out = new PrintStream(configOut)) {
            if (this.ec.isPrintPathLength()) {
                out.format("%s%d%n", description(PATH_LENGTH), distances.max().distance());
            }
            if (this.ec.isPrintDeadEnds()) {
                out.format("%s%d%n", description(DEADENDS), Support.deadEnds(grid.cells()).size());
            }
            if (this.ec.isPrintDimension()) {
                out.format("%s%d%n%s%d%n", description(COLUMNS), grid.dimension().columns(), description(ROWS), grid.dimension().rows());
            }
            if (this.ec.isPrintCells()) {
                out.format("%s%d%n", description(CELLS), grid.cells().filter(cell -> !cell.isNull()).count());
            }
            if (this.ec.isPrintStart()) {
                final var row = ((GridCell) distances.root()).coordinate().row();
                final var column = ((GridCell) distances.root()).coordinate().column();
                out.format("%s%d%n%s%d%n", description(START_COLUMN), column, description(START_ROW), row);
            }
            if (this.ec.isPrintGoal()) {
                final var row = ((GridCell) distances.max().cell()).coordinate().row();
                final var column = ((GridCell) distances.root()).coordinate().column();
                out.format("%s%d%n%s%d%n", description(GOAL_COLUMN), column, description(GOAL_ROW), row);
            }
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }
    private static final String GOAL_ROW = "goal-row";
    private static final String GOAL_COLUMN = "goal-column";
    private static final String START_ROW = "start-row";
    private static final String START_COLUMN = "start-column";
    private static final String CELLS = "cells";
    private static final String ROWS = "rows";
    private static final String COLUMNS = "columns";
    private static final String DEADENDS = "dead-ends";
    private static final String PATH_LENGTH = "path-length";
    
    private Distances distancesFor(final GridDistancesTuple gridTuple) {
       return gridTuple.distances().orElse(newDistancesFor(gridTuple));
    }
    
    private Distances newDistancesFor(final GridDistancesTuple gridTuple) {
        final var distance = gridTuple.grid().randomCell().distances();
        return new Distances(distance.max().cell());
    }

    private String description(final String description) {
        return this.ec.isPrintWithputDescription() ? "" : description + ": ";
    }
}
