/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import de.speexx.maze.domain.MazeIO;
import de.speexx.maze.domain.MazeIO.GridDistancesTuple;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Optional;

/**
 *
 * @author saschakohlmann
 */
@Parameters(commandNames = {"braid"}, commandDescription = "Makes a perfect maze a braid maze. Or makes a braid maze a more braid maze :-).")
public class Braider extends GridIoCommand {
    
    @ParametersDelegate
    final BraiderExecutionContext ec = new BraiderExecutionContext();

    @Override
    public void execute() {

        try (final var input = this.ec.inputStream();
             final var output = this.ec.outputStream()) {
            final var gridDistanceTuple = readTupleFrom(input);
            final var grid = gridDistanceTuple.grid();
            final var braid = this.ec.braid();
            braid.on(grid.cells());
        
            MazeIO.write(new GridDistancesTuple(grid, Optional.empty()), output);
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }
}
