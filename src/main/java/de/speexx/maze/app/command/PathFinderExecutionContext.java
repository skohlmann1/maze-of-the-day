/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParametersDelegate;
import de.speexx.maze.app.command.PathFinder.PathFinderStrategy;
import de.speexx.maze.domain.Coordinate;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;

import static de.speexx.maze.app.command.PathFinder.PathFinderStrategy.BORDER_BORDER;
import static de.speexx.maze.app.command.PathFinder.PathFinderStrategy.BORDER_MAYBE_INNER;
import static de.speexx.maze.app.command.PathFinder.PathFinderStrategy.MAYBE_INNER_INNER;
import static java.lang.String.format;
import static java.util.Locale.ENGLISH;

/**
 *
 * @author saschakohlmann
 */
public final class PathFinderExecutionContext {

    @ParametersDelegate
    private GridIoExecutionContext gioec = new GridIoExecutionContext();
    
    @Parameter(names = { "-s", "--strategy", "--path-strategy"},
               description = "Strategy for the finding the path. Supported values are 'border-border' (or 'bb') where the start and goal "
                             + "must be at a border cell. "
                             + "'border-maybe-inner' (or 'bm') where start must be a border cell but goal might be an inner cell. "
                             + "'maybe-inner-inner' (or 'mi') where start cell and goal cell may be an inner cell.",
               order = 1)
    String pathStrategy = "border-border";

    @Parameter(names = { "-e", "--enter" },
            description = "Optional start coordinate for the path finder. If not default value the path checker use only this coordinate. "
                    + "Format is 'CxR' where the is the Column and R is the Row. "
                    + "E.g. 0x0 is the upper left cell.",
            order = 2)
    String enter = "all";

    @Parameter(names = { "-g", "--goal" },
            description = "Optional goal coordinate for the path finder. If not default value the path checker use only this coordinate. "
                    + "Format is 'CxR' where the is the Column and R is the Row. "
                    + "E.g. 0x0 is the upper left cell.",
            order = 2)
    String goal = "all";

    public OutputStream outputStream() {
        return gioec.outputStream();
    }

    public InputStream inputStream() {
        return gioec.inputStream();
    }
    
    public PathFinderStrategy strategy() {
        return new PathFinderStrategyConverter().convert(this.pathStrategy);
    }

    public static final class PathFinderStrategyConverter implements IStringConverter<PathFinderStrategy> {

        @Override
        public PathFinderStrategy convert(final String value) {
            final var lower = value.toLowerCase(ENGLISH);
            if (null == lower) {
                throw new RuntimeException(format("Unsupported strategy: %s", value));
            } else switch (lower) {
                case "border-border", "bb" -> {
                    return BORDER_BORDER;
                }
                case "border-maybe-inner", "bm" -> {
                    return BORDER_MAYBE_INNER;
                }
                case "maybe-inner-inner", "mi" -> {
                    return MAYBE_INNER_INNER;
                }
                default -> throw new RuntimeException(format("Unsupported strategy: %s", value));
            }
        }
    }

    public static final class CoordinateConverter implements IStringConverter<Optional<Coordinate>> {

        @Override
        public Optional<Coordinate> convert(final String coordinateParameter) {
            if (coordinateParameter.toLowerCase(ENGLISH).startsWith("a")) {
                return Optional.empty();
            }
            return GridMazeGeneratorExecutionContext.coordinateForParameter(coordinateParameter);
        }
    }

    public Optional<Coordinate> startCoordinate() {
        return new CoordinateConverter().convert(this.enter);
    }

    public Optional<Coordinate> goalCoordinate() {
        return new CoordinateConverter().convert(this.goal);
    }

    @Override
    public String toString() {
        return "PathFinderExecutionContext{" + "gioec=" + gioec + ", pathStrategy=" + pathStrategy + '}';
    }
}
