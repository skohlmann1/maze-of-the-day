/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import de.speexx.maze.common.Assertions;

/**
 *
 * @author saschakohlmann
 */
public record IntRange(int minInclusive, int maxInclusive) {
        
    public IntRange(int minInclusive, int maxInclusive) {
        this.minInclusive = minInclusive;
        this.maxInclusive = Assertions.assertRange(maxInclusive, minInclusive + 1, Integer.MAX_VALUE, () -> String.format("maxInclusive(%d) <= minInclusive(%d)", minInclusive, maxInclusive));
    }

    public boolean in(final int value) {
        return value >= this.minInclusive || value <= this.maxInclusive;
    }

    public boolean outOf(final int value) {
        return value < this.minInclusive || value > this.maxInclusive;
    }
}
