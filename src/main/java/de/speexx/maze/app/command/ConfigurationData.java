/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import de.speexx.maze.domain.Tiling;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonString;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import static java.util.List.copyOf;

/**
 *
 * @author saschakohlmann
 */
final class ConfigurationData {

    final Collection<Configuration> configurations = new ArrayList<>();
    
    public Collection<Configuration> configurations() {
        return copyOf(this.configurations);
    }

    @Override
    public String toString() {
        return "ConfigurationData{" + "configurations=" + configurations + '}';
    }

    public static final class Configuration {

        public String mazeType;
        public String connectorClassName;
        public String tilerClassName;
        public String description;
        public Tiling tiling;
        public App app;

        @Override
        public String toString() {
            return "Configuration{" + "mazeType=" + mazeType + ", connectorClassName=" + connectorClassName + ", tilerClassName=" + tilerClassName + ", description=" + description + ", tiling=" + tiling + ", app=" + app + '}';
        }
    }
    
    public static final class App {
        public List<String> generatorNames = new ArrayList<>();

        @Override
        public String toString() {
            return "App{" + "generatorNames=" + generatorNames + '}';
        }
    }
    
    public static ConfigurationData loadConfiguration()  {
        try (final var input = ConfigurationData.class.getResourceAsStream("/META-INF/app/generator.config.json")) {
            final var reader = Json.createReader(input);
            final var configurationElements = reader.readArray();

            final var configurationData = new ConfigurationData();
            
            configurationElements.stream()
                    .map(json -> json.asJsonObject())
                    .map(jsonObject -> readConfiguration(jsonObject))
                    .forEach(configuration -> configurationData.configurations.add(configuration));
            
            return configurationData;
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }
    
    static Configuration readConfiguration(final JsonObject jo) {
        final var configuration = new Configuration();
        configuration.mazeType = jo.getString("maze-type");
        configuration.connectorClassName = jo.getString("connector");
        configuration.tilerClassName = jo.getString("tiler");
        configuration.description = jo.getString("description");
        configuration.tiling = readTiling(jo.getJsonObject("tiling"));
        configuration.app = readAppData(jo.getJsonObject("app"));
        return configuration;
    }
    
    static Tiling readTiling(final JsonObject jo) {
        final var type = jo.getString("type");
        var subtype = "";
        if (jo.containsKey("subtype")) {
            subtype = jo.getString("subtype");
        }
        return new Tiling(type, subtype, null);
    }

    static App readAppData(final JsonObject jo) {
        final var app = new App();
        app.generatorNames = jo.getJsonArray("generator-names").stream()
                .map(value -> (JsonString) value)
                .map(jsonString -> jsonString.getString())
                .toList();
        return app;
    }
}
