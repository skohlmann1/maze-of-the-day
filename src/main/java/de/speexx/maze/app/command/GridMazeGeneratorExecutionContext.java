/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.converters.IntegerConverter;
import com.beust.jcommander.converters.PathConverter;
import de.speexx.maze.domain.AldousBroder;
import de.speexx.maze.domain.Braid;
import de.speexx.maze.domain.Coordinate;
import de.speexx.maze.domain.Dimension;
import de.speexx.maze.domain.GrowingTree;
import de.speexx.maze.domain.HuntAndKill;
import de.speexx.maze.domain.Mask;
import de.speexx.maze.domain.Mask.SimpleMaskType;
import de.speexx.maze.domain.MaskSupport;
import de.speexx.maze.domain.MazeAlgorithm;
import de.speexx.maze.domain.Neighborhood.NeighborhoodConnector;
import de.speexx.maze.domain.Prims;
import de.speexx.maze.domain.RecursiveBacktracker;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.net.URI;
import java.nio.file.Path;
import java.util.Optional;
import java.util.function.BiFunction;

import static de.speexx.maze.domain.GrowingTree.LAST_SELECTOR;
import static de.speexx.maze.domain.GrowingTree.RANDOM_SELECTOR;
import static de.speexx.maze.domain.GrowingTree.SAMPLE_SELECTOR;
import static de.speexx.maze.app.App.PATH_TO_OUTPUT_STREAM;
import de.speexx.maze.MazeException;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import java.lang.reflect.InvocationTargetException;
import static java.util.Locale.ENGLISH;

/**
 *
 * @author saschakohlmann
 */
public final class GridMazeGeneratorExecutionContext {
    
    private static final IntRange BRAID_RANGE = new IntRange(0, 100);
    
    @Parameter(names = { "-d", "--dimension", "--grid-dimension" },
               description = "The dimension of the maze. Example: 30x20 generates a grid maze with 30 columns and 20 rows. "
                             + "In case of only 20 or 20x the grid will be a square of 20x20 cells.",
               order = 1)
    String dimension = "30x30";

    @Parameter(names = { "-e", "--enter" },
            description = "Optional start coordinate for the generator. "
                          + "Format is 'CxR' where the is the Column and R is the Row. "
                          + "E.g. 0x0 is the upper left cell.",
            order = 7)
    String enter = "random";

    @Parameter(names = { "-b", "--braid" },
               description = "Defines a percentage value of how large the permeability of dead ends in the maze is. Value must be between 0 and 100. "
                             + "0 means: no dead ends where 100 means only one possible path. All branches run into dead ends. Additional the value can be 'random'. "
                             + "In case of 'random' the implementation choose an random value between 0 and 100 for the braid level.",
               converter = IntegerConverter.class,
               order = 4)
    int braid = 100;
    
    @Parameter(names = { "-t", "--tiling"}, 
               description = "The tiling pattern of the maze. To get a list of all tiling pattern use 'maze tiling'",
               validateWith = NeighborhoodValidator.class,
               order = 2)
    String tiling = "manhatten";
    
    @Parameter(names = { "-o", "--output"}, 
               description = "Filename for grid output. If no value is given the output goes to standard-out",
               converter = PathConverter.class)
    Path output;
    
    @Parameter(names = { "-g", "--generator"},
               description = "Algorithm for maze generation. Supported values are: 'aldous-broder' (or 'a'), 'hunt-and-kill' (or 'h'), "
                             + "'recursive-backtrack' (or 'r'), 'prims' (or 'p'), 'true-prims' (or 't'), 'growing-tree' (or 'g').",
               order = 3)
    String algorithm = "hunt-and-kill";

    @Parameter(names = { "-s", "--generator-configuration"},
               description = "Additional configuration for generators. 'growing-tree' supports different generation strategies. "
                             + "Supported values are: 'sample', 'last' and 'random'. If no configuration for 'growing-tree' is "
                             + "given, one of the possible configurations are selected.",
               order = 5)
    String algorithmConfiguration = "";
    
    @Parameter(names = { "-m", "--mask"},
               description = "Path to a mask for the maze to generate. In case a mask is given, the dimension parameter will be ignored. "
                             + "Supported input formats are text files (UTF-8 encoded) or PNG images. For text files the Unicode characters "
                             + "x or X (U+0078 or U+0058) marking blocked cell. The Unicode characters o or O (U+006F or U+004F) marking "
                             + "usable cells. For PNG files a black pixel (0x000) marks a blocked cells and a white pixel (0xfff) "
                             + "marks a useable cell. The behavior for other characters and cells is undefined",
               converter = PathConverter.class,
               order = 6)
    Path maskPath;

    public NeighborhoodConnector neighborhoodConnector() {
        return new NeighborhoodConnectorConverter().convert(this.tiling);
    }
    
    public MazeAlgorithm generator() {
        return GENERATOR_CONVERTER.apply(this.algorithm, this.algorithmConfiguration);
    }
    
    private Optional<Path> outputPath() {
        return Optional.ofNullable(this.output);
    }
    
    public OutputStream outputStream() {
        return PATH_TO_OUTPUT_STREAM.apply(outputPath());
    }
    
    public Optional<URI> maskUri() {
        if (this.maskPath == null) {
            return Optional.empty();
        }
        return Optional.of(this.maskPath.toUri());
    }

    public Dimension dimension() {
        return new DimensionConverter().convert(this.dimension);
    }

    public Optional<Coordinate> startCoordinate() {
        return new CoordinateConverter().convert(this.enter);
    }

    public Optional<Mask<SimpleMaskType>> mask() {
        try {
            final var maskUri = maskUri();
            if (maskUri.isPresent()) {
                final var mask = MaskSupport.loadMask(maskUri.get().toURL().openStream());
                return Optional.of(mask);
            }
            
            return Optional.empty();
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }
    
    public Braid braid() {
        return new Braid(this.braid / 100f);
    }
    
    public static class BraidValidator implements IParameterValidator {

        @Override
        public void validate(final String string, final String value) throws ParameterException {
            final var braid = parseInt(value);
            if (BRAID_RANGE.outOf(braid)) {
                throw new ParameterException(format("Braid value not in range %s", BRAID_RANGE));
            }
        }
    }
    
    public static final class DimensionConverter implements IStringConverter<Dimension> {

        @Override
        public Dimension convert(final String dimensionParameter) {
            final var dimensionParts = dimensionParameter.split("x");
            final var columns = parseInt(dimensionParts[0]);
            if (dimensionParts.length == 1) {
                return new Dimension(columns, columns);
            }
            return new Dimension(parseInt(dimensionParts[1]), columns);
        }
    }

    public static final class CoordinateConverter implements IStringConverter<Optional<Coordinate>> {

        @Override
        public Optional<Coordinate> convert(final String coordinateParameter) {
            if (coordinateParameter.toLowerCase(ENGLISH).startsWith("r")) {
                return Optional.empty();
            }
            return coordinateForParameter(coordinateParameter);
        }
    }

    static Optional<Coordinate> coordinateForParameter(String coordinateParameter) {
        final var coordinateParts = coordinateParameter.split("x");
        final var columns = parseInt(coordinateParts[0]);
        if (coordinateParts.length == 1) {
            return Optional.of(new Coordinate(columns, columns));
        }
        return Optional.of(new Coordinate(parseInt(coordinateParts[1]), columns));
    }


    public static final class NeighborhoodConnectorConverter implements IStringConverter<NeighborhoodConnector> {

        @Override
        public NeighborhoodConnector convert(final String value) {
            final var lower = value.toLowerCase(ENGLISH);
            final var configurations =  ConfigurationData.loadConfiguration().configurations();
            for (final var configuration : configurations) {
                if (configuration.app.generatorNames.contains(lower)) {
                    final var className = configuration.connectorClassName;
                    try {
                        final var clazz = Class.forName(className);
                        final var constructor = clazz.getConstructor();
                        return (NeighborhoodConnector) constructor.newInstance();
                    } catch (final ClassNotFoundException ex) {
                        throw new MazeException(format("Neighbor connector (generator) class %s not available", className));
                    } catch (final NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                        throw new MazeException(format("Can't instantiate Neighbor connector (generator) of class %s", className), ex);
                    }
                }
            }
            
            throw new MazeException(format("Unsupported algorithm: %s", value));
        }
    }

    public final static BiFunction<String, String, MazeAlgorithm> GENERATOR_CONVERTER = (generatorName, generatorConfiguration) -> {

            final var lower = generatorName.toLowerCase(ENGLISH);
            switch (lower) {
                case "recursive-backtrack", "r" -> { return new RecursiveBacktracker(); }
                case "aldous-broder", "a"       -> { return new AldousBroder(); }
                case "hunt-and-kill", "h"       -> { return new HuntAndKill(); }
                case "true-prims", "t"          -> { return new GrowingTree(); }
                case "prims", "p"               -> { return new Prims(); }
                case "growing-tree", "g" -> {
                    final var lowerConfig = generatorConfiguration.toLowerCase(ENGLISH);
                    return switch (lowerConfig) {
                        case "sample" -> new GrowingTree(SAMPLE_SELECTOR);
                        case "last" -> new GrowingTree(LAST_SELECTOR);
                        default -> new GrowingTree(RANDOM_SELECTOR);
                    };
                }
                default -> throw new RuntimeException(format("Unsupported algorithm: %s", generatorName));
            }
        };
    
    public static final class NeighborhoodValidator implements IParameterValidator {

        @Override
        public void validate(String string, String value) throws ParameterException {
            final var generatorNames = ConfigurationData.loadConfiguration().configurations().stream()
                    .flatMap(configuration -> configuration.app.generatorNames.stream())
                    .map(name -> name.toLowerCase(ENGLISH))
                    .toList();
            if (!generatorNames.contains(value)) {
                throw new ParameterException(format("Value must be 'manhatten' or 'm' or 'triangle' or 't'. Is: %s", value));
            }
        }
    }

    public static final class GeneratorValidator implements IParameterValidator {
        @Override
        public void validate(String string, String value) throws ParameterException {
            if (!"aldous-broder".equalsIgnoreCase(value) && !"a".equalsIgnoreCase(value)
                    && !"hunt-and-kill".equalsIgnoreCase(value) && !"h".equalsIgnoreCase(value)
                    && !"recursive-backtrack".equalsIgnoreCase(value) && !"r".equalsIgnoreCase(value)
                    && !"prims".equalsIgnoreCase(value) && !"p".equalsIgnoreCase(value)
                    && !"true-prims".equalsIgnoreCase(value) && !"t".equalsIgnoreCase(value)
                    && !"growing-tree".equalsIgnoreCase(value) && !"g".equalsIgnoreCase(value)) {
                throw new ParameterException(format("Ansupported generator algorithm. Is: %s", value));
            }
        }
    }

    @Override
    public String toString() {
        return "GeneratorExecutionContext{" + "dimension=" + dimension + ", braid=" + braid + ", cellFormat=" + tiling + ", output=" + output + ", algorithm=" + algorithm + ", algorithmConfiguration=" + algorithmConfiguration + ", maskPath=" + maskPath + '}';
    }
}
