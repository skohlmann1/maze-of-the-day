/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import de.speexx.maze.domain.BaseGrid;
import de.speexx.maze.domain.Cell;
import de.speexx.maze.domain.Grid;
import de.speexx.maze.domain.MaskedGrid;
import de.speexx.maze.domain.MazeIO;
import de.speexx.maze.domain.MazeIO.GridDistancesTuple;
import de.speexx.maze.app.Command;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Optional;

/**
 *
 * @author saschakohlmann
 */
@Parameters(commandNames = {"grid"}, commandDescription = "Generates a grid maze with triangle, rectangle or hexagon cells with an optional mask.")
public class GridMazeGenerator implements Command {
    
    @ParametersDelegate
    final GridMazeGeneratorExecutionContext ec = new GridMazeGeneratorExecutionContext();

    @Override
    public void execute() {

        try {
            final var neighborhoodConnector = this.ec.neighborhoodConnector();
            final var optionalMask = this.ec.mask();
            final BaseGrid grid;
            if (optionalMask.isPresent()) {
                grid = new MaskedGrid(optionalMask.get(), neighborhoodConnector);
            } else {
                grid = new BaseGrid(this.ec.dimension(), neighborhoodConnector);
            }

            final var mazeAlgorithm = this.ec.generator();
            mazeAlgorithm.on(grid, startCellForGrid(grid));
            final var braid = this.ec.braid();
            braid.on(grid.cells());
        
            MazeIO.write(new GridDistancesTuple(grid, Optional.empty()), this.ec.outputStream());
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    private Cell startCellForGrid(final Grid grid) {
        assert grid != null;
        final var optional = ec.startCoordinate();
        if (optional.isEmpty()) {
            return grid.randomCell();
        }

        final var cell = grid.cellAt(optional.get());
        if (cell.neighbors().isEmpty()) {
            throw new IllegalArgumentException(String.format("Cell at coordinate %s has no neighbors", optional.get()));
        }
        return cell;
    }
}
