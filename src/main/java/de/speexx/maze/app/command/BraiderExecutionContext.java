/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.ParametersDelegate;
import com.beust.jcommander.converters.IntegerConverter;
import de.speexx.maze.domain.Braid;
import java.io.InputStream;
import java.io.OutputStream;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;

/**
 *
 * @author saschakohlmann
 */
public class BraiderExecutionContext {
    
    private static final IntRange BRAID_RANGE = new IntRange(0, 100);

    @ParametersDelegate
    private final GridIoExecutionContext gioec = new GridIoExecutionContext();

    @Parameter(names = { "-b", "--braid" },
               description = "Defines a percentage value of how large the permeability of dead ends in the maze is. Value must be between 0 and 100. "
                             + "0 means: no dead ends where 100 means only one possible path. All branches run into dead ends. Additional the value can be 'random'. "
                             + "In case of 'random' the implementation choose an random value between 0 and 100 for the braidd level.",
               converter = IntegerConverter.class,
               order = 4)
    int braid = 100;

    public Braid braid() {
        return new Braid(this.braid / 100f);
    }

    public static class BraidValidator implements IParameterValidator {

        @Override
        public void validate(final String string, final String value) throws ParameterException {
            final var braid = parseInt(value);
            if (BRAID_RANGE.outOf(braid)) {
                throw new ParameterException(format("Braid value not in range %s", BRAID_RANGE));
            }
        }
    }

    public OutputStream outputStream() {
        return gioec.outputStream();
    }

    public InputStream inputStream() {
        return gioec.inputStream();
    }
}
