/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import de.speexx.maze.MazeException;
import de.speexx.maze.domain.Distances;
import de.speexx.maze.domain.Maze;
import de.speexx.maze.domain.MazeIO.GridDistancesTuple;
import de.speexx.maze.graphics.SimpleEuclideanPlaneTiler.StartGoalConfiguration;
import de.speexx.maze.graphics.Tiler;
import de.speexx.maze.graphics.UnsupportedTilingException;
import java.awt.geom.Dimension2D;
import java.io.IOException;
import java.io.UncheckedIOException;

import static java.lang.String.format;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author saschakohlmann
 */
@Parameters(commandNames = {"image"}, commandDescription = "Generates an image based on the maze definition. Currently only platonic images are supported.")
public class MazeToImage extends GridIoCommand {

    @ParametersDelegate
    private MazeToImageExecutionContext ec = new MazeToImageExecutionContext();
    
    @Override
    public void execute() {
        
        final var bounds = this.ec.bounds();
        
        final var gridDistanceTuple = readTupleFrom(this.ec.inputStream());
        final var grid = gridDistanceTuple.grid();
        final var distances = distancesFor(gridDistanceTuple);
        final var tiler = tilerFor(grid);
        final var tilePattern = tiler.tile(grid, distances);
        
        final var exporter = this.ec.imageFormat().exporter();
        try (final var output = this.ec.outputStream()) {
            exporter.export(tilePattern, bounds, output);
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    private Distances distancesFor(final GridDistancesTuple gridTuple) {
       return gridTuple.distances().orElse(newDistancesFor(gridTuple));
    }

    private Distances newDistancesFor(final GridDistancesTuple gridTuple) {
        final var distance = gridTuple.grid().randomCell().distances();
        return new Distances(distance.max().cell());
    }
    
    private Tiler tilerFor(final Maze grid) {
        final var configurations =  ConfigurationData.loadConfiguration().configurations();
        final var tiling = grid.tiling();

        final var startGoalConfig = new StartGoalConfiguration(this.ec.isDrawStartGoal(), this.ec.isDrawPath());
        for (final var configuration : configurations) {
            if (configuration.tiling.type().equalsIgnoreCase(tiling.type()) && configuration.tiling.subtype().equalsIgnoreCase(tiling.subtype())) {
                final var className = configuration.tilerClassName;
                try {
                    final var clazz = Class.forName(className);
                    final var constructor = clazz.getConstructor(Dimension2D.class, StartGoalConfiguration.class);
                    return (Tiler) constructor.newInstance(this.ec.cellDimension(), startGoalConfig);
                } catch (final ClassNotFoundException ex) {
                    throw new MazeException(format("Tiler class class %s not available", className));
                } catch (final NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    ex.printStackTrace(System.err);
                    throw new MazeException(format("Can't instantiate Tiler of class %s", className), ex);
                }
            }
        }

        throw new UnsupportedTilingException(format("Tiling not suppported: %s", tiling));
    }
}
