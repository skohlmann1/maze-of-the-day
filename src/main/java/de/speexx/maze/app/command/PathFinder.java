/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import de.speexx.maze.domain.Cell;
import de.speexx.maze.domain.Distances;
import de.speexx.maze.domain.Grid;
import de.speexx.maze.domain.GridCell;
import de.speexx.maze.domain.MazeIO.GridDistancesTuple;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static de.speexx.maze.domain.Support.borderCells;
import static java.lang.String.format;
import static java.lang.System.Logger.Level.DEBUG;

/**
 *
 * @author saschakohlmann
 */
@Parameters(commandNames = {"path"}, commandDescription = "Finds pathes in a grid based on different strategies.")
public class PathFinder extends GridIoCommand {
    
    @ParametersDelegate
    private PathFinderExecutionContext ec = new PathFinderExecutionContext();

    @Override
    public void execute() {
        
        final var gridDistanceTuple = readTupleFrom(this.ec.inputStream());

        final var grid = gridDistanceTuple.grid();
        final var strategy = strategy();

        final var cellTuples = new HashSet<CellTuple>();

        Distances longest = null;

        if (strategy == PathFinderStrategy.CELL_TO_CELL) {
            final var goalCell = goalCell(grid);
            for (final var cell : cellsToCheck(grid)) {
                final var root = grid.cellAt(cell.coordinate());
                final var distances = root.distances();
                final var path = distances.pathTo(goalCell);
                if (longest == null || path.max().distance() > longest.max().distance()) {
                    longest = path;
                }
            }
        } else {
            for (final var cell : cellsToCheck(grid)) {
                System.getLogger("Maze").log(DEBUG, format("Cell: %s", cell));
                final var root = grid.cellAt(cell.coordinate());
                final var distances = root.distances();
                switch (strategy) {
                    case BORDER_BORDER -> {
                        for (final var pathToCell : borderCells(grid)) {
                            final var cellTuple = new CellTuple(cell, pathToCell);
                            if (!cellTuples.contains(cellTuple)) {
                                cellTuples.add(cellTuple);
                                final var path = distances.pathTo(pathToCell);
                                if (longest == null || path.max().distance() > longest.max().distance()) {
                                    longest = path;
                                }
                            }
                        }
                    }
                    case MAYBE_INNER_INNER -> {
                        final var max = distances.max();
                        longest = max.cell().distances();
                    }
                    case BORDER_MAYBE_INNER -> {
                        final var max = distances.max();
                        if (longest == null || longest.max().distance() < max.distance()) {
                            longest = distances;
                        }
                    }
                }
            }
        }

        final var outTuple = new GridDistancesTuple(grid, Optional.of(longest));
        writeTupleTo(outTuple, this.ec.outputStream());
    }

    private PathFinderStrategy strategy() {

        if (this.ec.goalCoordinate().isPresent() && this.ec.startCoordinate().isPresent()) {
            return PathFinderStrategy.CELL_TO_CELL;
        }

        return this.ec.strategy();
    }

    private Cell goalCell(final Grid grid) {
        assert grid != null;
        final var coordinate = this.ec.goalCoordinate();
        if (coordinate.isEmpty()) {
            throw new IllegalStateException("For cell to cell path goal coordinate must be available");
        }
        final var cell = grid.cellAt(coordinate.get());
        if (cell.neighbors().isEmpty()) {
            throw new IllegalArgumentException(String.format("Cell at coordinate %s has no neighbors", coordinate.get()));
        }
        return cell;
    }

    private Collection<GridCell> cellsToCheck(final Grid grid) {
        final var coordinate = this.ec.startCoordinate();
        if (coordinate.isEmpty()) {
            return borderCells(grid);
        }
        final var cell = grid.cellAt(coordinate.get());
        if (cell.neighbors().isEmpty()) {
            throw new IllegalArgumentException(String.format("Cell at coordinate %s has no neighbors", coordinate.get()));
        }
        return Set.of(cell);
    }
    
    public static enum PathFinderStrategy {
        BORDER_BORDER,
        BORDER_MAYBE_INNER,
        MAYBE_INNER_INNER,
        CELL_TO_CELL;
    }

    private record CellTuple(Cell c1, Cell cell2) {}
}
