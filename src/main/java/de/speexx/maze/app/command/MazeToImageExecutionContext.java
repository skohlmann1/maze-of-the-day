/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import de.speexx.maze.graphics.GraphicsFormatExporter;
import de.speexx.maze.graphics.SVGExporter;
import de.speexx.maze.graphics.PNGExporter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.PathConverter;
import de.speexx.maze.app.command.GridMazeGeneratorExecutionContext.DimensionConverter;
import java.awt.Dimension;
import java.awt.geom.Dimension2D;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Optional;

import static de.speexx.maze.app.App.PATH_TO_INPUT_STREAM;
import static de.speexx.maze.app.App.PATH_TO_OUTPUT_STREAM;
import static java.util.Locale.ENGLISH;

/**
 *
 * @author saschakohlmann
 */
public class MazeToImageExecutionContext {
    
    @Parameter(names = { "-f", "--image-format", "--format"},
               description = "Image format to export. Supported are PNG and SVG",
               order = 1)
    String format = "SVG";
    
    @Parameter(names = { "-d", "--cell-dimension" }, 
               description = "Dimension of a cell for image output. Depending on the image format the value is interpreted as pixel or points (1/72 inch). "
                           + "Example: 20x20 as a cell dimension of 20x20 pixel on PNG output. It's also possible to define the block dimension only with one value. "
                           + "In this case the cell dimension is a square block of the given value. Note 1: the cell dimension is the maximum cell dimension allowed. "
                           + "The implementation may choose a different cell dimension, based on the required tiling. Note 2: application calculates based on the grid "
                           + "dimension and the cell dimension the dimension of the output image with and additional bounds of the larger cell side. "
                           + "E.g. the cell dimension is 20x30 and the grid dimension is 10x20. The width of the generated image is "
                           + "20 \u00D7 10 + 2 \u00D7 30 pixel/millimeter. The height of the generated image is 30 \u00D7 20 + 2 \u00D7 30 pixel/points.",
               order = 2)
    String cellDimension = "40x40";

    @Parameter(names = { "-b", "--bounds"},
               description = "Bounds around the maze in the generated image.",
               order = 5)
    String bounds = cellDimension;

    @Parameter(names = { "-p", "--path"},
               description = "Draw path from start to goal.",
               order = 4)
    boolean drawPath = false;
    
    @Parameter(names = { "-sg", "--start-goal", "--startgoal"},
               description = "Draw start and goal cell if available.",
               order = 3)
    boolean drawStartGoal = false;
    
    @Parameter(names = { "-i", "--input"},
               description = "Filename for grid input. If no value is given the input reads from standard-in.",
               converter = PathConverter.class)
    Path input;

    @Parameter(names = { "-o", "--output"},
               description = "Filename for image output. If no value is given the output goes to standard-out.",
               converter = PathConverter.class)
    Path output;

    private Optional<Path> outputPath() {
        return Optional.ofNullable(this.output);
    }
    
    public OutputStream outputStream() {
        return PATH_TO_OUTPUT_STREAM.apply(outputPath());
    }

    private Optional<Path> inputPath() {
        return Optional.ofNullable(this.input);
    }
    
    public InputStream inputStream() {
        return PATH_TO_INPUT_STREAM.apply(inputPath());
    }
    
    public Dimension2D cellDimension() {
        final var domension = new DimensionConverter().convert(this.cellDimension);
        return new Dimension(domension.columns(), domension.rows());
    }
    
    public Dimension2D bounds() {
        final var domension = new DimensionConverter().convert(this.bounds);
        return new Dimension(domension.columns(), domension.rows());
    }
    
    public ImageFormat imageFormat() {
        final var formatUpper = this.format.toUpperCase(ENGLISH);
        return ImageFormat.valueOf(formatUpper);
    }

    public boolean isDrawPath() {
        return this.drawPath;
    }

    public boolean isDrawStartGoal() {
        return this.drawStartGoal;
    }
    
    public enum ImageFormat {
        SVG {
            @Override
            public GraphicsFormatExporter exporter() {
                return new SVGExporter();
            }
        },
        PNG {
            @Override
            public GraphicsFormatExporter exporter() {
                return new PNGExporter();
            }
        };
        
        public abstract GraphicsFormatExporter exporter();
    }
}
