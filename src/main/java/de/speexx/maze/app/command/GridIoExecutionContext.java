/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.PathConverter;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Optional;

import static de.speexx.maze.app.App.PATH_TO_INPUT_STREAM;
import static de.speexx.maze.app.App.PATH_TO_OUTPUT_STREAM;

/**
 *
 * @author saschakohlmann
 */
public class GridIoExecutionContext {
    
    @Parameter(names = { "-i", "--input"}, description = "Filename for grid input. If no value is given the input reads from standard-in.", converter = PathConverter.class)
    Path input;

    @Parameter(names = { "-o", "--output"}, description = "Filename for grid output. If no value is given the output goes to standard-out.", converter = PathConverter.class)
    Path output;

    private Optional<Path> outputPath() {
        return Optional.ofNullable(this.output);
    }
    
    public OutputStream outputStream() {
        return PATH_TO_OUTPUT_STREAM.apply(outputPath());
    }

    private Optional<Path> inputPath() {
        return Optional.ofNullable(this.input);
    }
    
    public InputStream inputStream() {
        return PATH_TO_INPUT_STREAM.apply(inputPath());
    }

    @Override
    public String toString() {
        return "GridIoExecutionContext{" + "input=" + input + ", output=" + output + '}';
    }
}
