/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import de.speexx.maze.domain.MazeIO;
import de.speexx.maze.domain.MazeIO.GridDistancesTuple;
import de.speexx.maze.app.Command;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;

/**
 *
 * @author saschakohlmann
 */
public abstract class GridIoCommand implements Command {
    
    protected void writeTupleTo(final GridDistancesTuple tuple, final OutputStream out) {
        try (final var output = out) {
            MazeIO.write(tuple, output);
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }
    
    protected GridDistancesTuple readTupleFrom(final InputStream in) {
        try (final var input = in) {
            return MazeIO.read(input);
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }
}
