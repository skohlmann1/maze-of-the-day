/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import de.speexx.maze.app.Command;
import java.util.ArrayList;
import java.util.List;
import static java.util.stream.Collectors.joining;
import java.util.stream.IntStream;

/**
 *
 * @author saschakohlmann
 */
@Parameters(commandNames = {"tilings"}, commandDescription = "Show all possible tilings and there descriptions.")
public class ShowTilings implements Command {

    @ParametersDelegate
    private ShowTilingsExecutionContext ec = new ShowTilingsExecutionContext();

    @Override
    public void execute() {

        final var configurations = ConfigurationData.loadConfiguration();
        configurations.configurations().stream()
                .forEach(config -> {
                    System.out.format("Tiling: %s", config.tiling.type());
                    if (!config.tiling.subtype().isBlank()) {
                        System.out.format(":%s", config.tiling.subtype());
                    }
                    System.out.format("%n");
                    System.out.format("    Pattern name(s): %s%n", config.app.generatorNames.stream().map(name -> "'" + name + "'").collect(joining(", ")));
                    System.out.format("    Description: %n%s%n%n", rowedDescription(config.description));
        });
        
    }
    
    private String rowedDescription(final String description) {
        final var rows = splitToColumnSize(10, description);
        return rows.stream().collect(joining(System.lineSeparator()));
    }
    
    private List<String> splitToColumnSize(final int indent, final String toSplit) {
        
        final var columns = new ArrayList<String>();
        final var splitted = toSplit.split("\\s");
        var sb = new StringBuilder(emptyStringWithLength(indent));

        for (final var part : splitted) {
            if ((part.length() + sb.length() + 1) >= this.ec.columns()) {
                columns.add(sb.toString());
                sb = new StringBuilder(emptyStringWithLength(indent));
            }
            sb.append(part);
            sb.append(" ");
        }
        columns.add(sb.toString());
        
        return columns;
    }
    
    private String emptyStringWithLength(final int length) {
        final var sb = new StringBuilder(length);
        IntStream.range(0, length).forEach(intend -> sb.append(" "));
        return sb.toString();
    }
}
