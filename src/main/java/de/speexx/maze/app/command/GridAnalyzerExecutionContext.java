/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.app.command;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParametersDelegate;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author saschakohlmann
 */
public class GridAnalyzerExecutionContext {
    
    @ParametersDelegate
    private GridIoExecutionContext gioec = new GridIoExecutionContext();
    
    @Parameter(names = { "-p", "--path-length"},
               description = "Prints the maximum path length (a.k.a. distance).")
    boolean printPathLength = false;

    @Parameter(names = { "-e", "--deadends"},
               description = "Prints the number of dead ends.")
    boolean printDeadEnds = false;

    @Parameter(names = { "-d", "--dimension"},
               description = "Prints the dimension of the grid. First output is the number of columns, second output is the number of rows.")
    boolean printDimension = false;

    @Parameter(names = { "-n", "--cells"},
               description = "Prints number of cells.")
    boolean printCells = false;

    @Parameter(names = { "-a", "--all"},
               description = "Print all possible values.")
    boolean printAll = true;

    @Parameter(names = { "-s", "--start"},
               description = "Prints the start coordinate. First output is the column value, second output is the row value. Values are 0 based.")
    boolean printStart = false;

    @Parameter(names = { "-g", "--goal"},
               description = "Prints the goal coordinate. First output is the column value, second output is the row value.. Values are 0 based.")
    boolean printGoal = false;

    @Parameter(names = { "-w", "--without"},
               description = "Prints without description. This value has no effect on JSON output.")
    boolean printWithputDescription = false;

    @Parameter(names = { "-j", "--json"},
               description = "Prints the description as JSON output. May use other tools like 'jq' to transform into e.g. CSV.")
    boolean printJson = false;
    
    public boolean isPrintPathLength() {
        return this.printAll == true ? true : this.printAll == true ? true : printPathLength;
    }

    public boolean isPrintDeadEnds() {
        return this.printAll == true ? true : printDeadEnds;
    }

    public boolean isPrintCells() {
        return this.printAll == true ? true : printCells;
    }

    public boolean isPrintStart() {
        return this.printAll == true ? true : printStart;
    }

    public boolean isPrintDimension() {
        return this.printAll == true ? true : printDimension;
    }

    public boolean isPrintJson() {
        return printJson;
    }

    public boolean isPrintGoal() {
        return this.printAll == true ? true : printGoal;
    }

    public boolean isPrintWithputDescription() {
        return printWithputDescription;
    }

    public OutputStream outputStream() {
        return gioec.outputStream();
    }

    public InputStream inputStream() {
        return gioec.inputStream();
    }

    @Override
    public String toString() {
        return "GridAnalyzerExecutionContext{" + "gioec=" + gioec + ", printPathLength=" + printPathLength + ", printDeadEnds=" + printDeadEnds + ", printDimension=" + printDimension + ", printCells=" + printCells + ", printAll=" + printAll + ", printStart=" + printStart + ", printGoal=" + printGoal + ", printWithputDescription=" + printWithputDescription + ", printJson=" + printJson + '}';
    }
}
