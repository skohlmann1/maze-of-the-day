/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.graphics;

import java.awt.Graphics2D;
import java.awt.geom.NoninvertibleTransformException;

/**
 *
 * @author saschakohlmann
 */
public final class PatternPainter {
    
    public void paint(final TilePattern tiles, final Graphics2D g2) {
        final var zeroPointShift = tiles.zeroPointShift();
        g2.transform(zeroPointShift);
        
        tiles.background().stream().sequential().forEach(tile -> {
            g2.setPaint(tile.paint());
            tile.stroke().ifPresent(stroke -> g2.setStroke(stroke));
            g2.fill(tile.shape());
        });
        tiles.barriers().stream().sequential().forEach(tile -> {
            g2.setPaint(tile.paint());
            tile.stroke().ifPresent(stroke -> g2.setStroke(stroke));
            g2.draw(tile.shape());
        });
        tiles.overlay().stream().sequential().forEach(tile -> {
            g2.setPaint(tile.paint());
            tile.stroke().ifPresent(stroke -> g2.setStroke(stroke));
            g2.fill(tile.shape());
        });
        try {
            g2.transform(zeroPointShift.createInverse());
        } catch (final NoninvertibleTransformException ex) {
            throw new PaintException(ex.getMessage(), ex);
        }
    }
}
