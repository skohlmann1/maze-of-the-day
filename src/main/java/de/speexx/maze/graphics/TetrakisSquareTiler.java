/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.graphics;

import de.speexx.maze.domain.GridCell;
import static de.speexx.maze.domain.TriangleNeighborhood.A;
import static de.speexx.maze.domain.TriangleNeighborhood.B;
import static de.speexx.maze.domain.TriangleNeighborhood.C;
import static de.speexx.maze.graphics.SimpleEuclideanPlaneTiler.DEFAULT_DIMENSION;
import static de.speexx.maze.graphics.SimpleEuclideanPlaneTiler.NOTHING;
import java.awt.Shape;
import java.awt.geom.Dimension2D;
import java.awt.geom.Path2D;
import static java.lang.Double.max;

/**
 *
 * @author saschakohlmann
 */
public class TetrakisSquareTiler extends TriangularRightTiler {
    
    public TetrakisSquareTiler() {
        this(DEFAULT_DIMENSION, NOTHING);
    }

    public TetrakisSquareTiler(final Dimension2D cellBounds) {
        this(cellBounds, NOTHING);
    }

    public TetrakisSquareTiler(final StartGoalConfiguration startGoalConfig) {
        this(DEFAULT_DIMENSION, startGoalConfig);
    }
    
    public TetrakisSquareTiler(final Dimension2D cellBounds, final StartGoalConfiguration startGoalConfig) {
        super(cellBounds, startGoalConfig);
    }

    @Override
    protected CellCoordinates calculateCellCoordinates(final GridCell cell, final Dimension2D cellDimension) {
        final var widthAndHeight = (int) max(cellDimension.getWidth(), cellDimension.getHeight());

        final var x = widthAndHeight * (cell.coordinate().column() % 2 == 0 ? cell.coordinate().column() / 2 : (cell.coordinate().column() - 1) / 2);
        final var y = widthAndHeight * cell.coordinate().row();
        return new CellCoordinates(x, y, widthAndHeight, widthAndHeight);
    }

    
    @Override
    Shape backgroundShapeFor(final GridCell cell, final CellCoordinates cellCoordinates) {
        final var cellWidth = cellCoordinates.width();
        final var cellHeight = cellCoordinates.height();
        final var x = cellCoordinates.x();
        final var y = cellCoordinates.y();
        final var triangleShape = new Path2D.Double();

        if (cell.coordinate().row() % 2 == 0) {

            switch (cell.coordinate().column() % 4) {
                case 0 -> {
                    triangleShape.moveTo(x, y);
                    triangleShape.lineTo(x + cellWidth, y + cellHeight);
                    triangleShape.lineTo(x, y + cellHeight);
                    triangleShape.lineTo(x, y);
                }
                case 1 -> {
                    triangleShape.moveTo(x, y);
                    triangleShape.lineTo(x + cellWidth, y);
                    triangleShape.lineTo(x + cellWidth, y + cellHeight);
                    triangleShape.lineTo(x, y);
                }
                case 2 -> {
                    triangleShape.moveTo(x, y);
                    triangleShape.lineTo(x + cellWidth, y);
                    triangleShape.lineTo(x, y + cellHeight);
                    triangleShape.lineTo(x, y);
                }
                default -> {
                    triangleShape.moveTo(x + cellWidth, y);
                    triangleShape.lineTo(x + cellWidth, y + cellHeight);
                    triangleShape.lineTo(x, y + cellHeight);
                    triangleShape.lineTo(x + cellWidth, y);
                }
            }
        } else {
            switch (cell.coordinate().column() % 4) {
                case 0 -> {
                    triangleShape.moveTo(x, y);
                    triangleShape.lineTo(x + cellWidth, y);
                    triangleShape.lineTo(x, y + cellHeight);
                    triangleShape.lineTo(x, y);
                }
                case 1 -> {
                    triangleShape.moveTo(x + cellWidth, y);
                    triangleShape.lineTo(x + cellWidth, y + cellHeight);
                    triangleShape.lineTo(x, y + cellHeight);
                    triangleShape.lineTo(x + cellWidth, y);
                }
                case 2 -> {
                    triangleShape.moveTo(x, y);
                    triangleShape.lineTo(x + cellWidth, y + cellHeight);
                    triangleShape.lineTo(x, y + cellHeight);
                    triangleShape.lineTo(x, y);
                }
                default -> {
                    triangleShape.moveTo(x, y);
                    triangleShape.lineTo(x + cellWidth, y);
                    triangleShape.lineTo(x + cellWidth, y + cellHeight);
                    triangleShape.lineTo(x, y);
                }
            }
        }

        triangleShape.closePath();
        return triangleShape;
    }

    @Override
    Shape barrierShape(final GridCell cell, final CellCoordinates cellCoordinates) {
        final var linkedWith = cell.linkedWith();
        final var cellWidth = cellCoordinates.width();
        final var cellHeight = cellCoordinates.height();
        final var x = cellCoordinates.x();
        final var y = cellCoordinates.y();
        final var triangleShape = new Path2D.Double();

        if (cell.coordinate().row() % 2 == 0) {
            switch (cell.coordinate().column() % 4) {
                case 0 -> {
                    triangleShape.moveTo(x, y);

                    final var cx = x + cellWidth;
                    final var cy = y + cellHeight;
                    if (!linkedWith.contains(cell.neightborAt(C))) {
                        triangleShape.lineTo(cx, cy);
                    } else {
                        triangleShape.moveTo(cx, cy);
                    }

                    final var ax = x;
                    final var ay = y + cellHeight;
                    if (!linkedWith.contains(cell.neightborAt(A))) {
                        triangleShape.lineTo(ax, ay);
                    } else {
                        triangleShape.moveTo(ax, ay);
                    }

                    final var bx = x;
                    final var by = y;
                    if (!linkedWith.contains(cell.neightborAt(B))) {
                        triangleShape.lineTo(bx, by);
                    } else {
                        triangleShape.moveTo(bx, by);
                    }
                }
                case 1 -> {
                    triangleShape.moveTo(x, y);

                    final var ax = x + cellWidth;
                    final var ay = y;
                    if (!linkedWith.contains(cell.neightborAt(A))) {
                        triangleShape.lineTo(ax, ay);
                    } else {
                        triangleShape.moveTo(ax, ay);
                    }

                    final var bx = x + cellWidth;
                    final var by = y + cellHeight;
                    if (!linkedWith.contains(cell.neightborAt(B))) {
                        triangleShape.lineTo(bx, by);
                    } else {
                        triangleShape.moveTo(bx, by);
                    }

                    final var cx = x;
                    final var cy = y;
                    if (!linkedWith.contains(cell.neightborAt(C))) {
                        triangleShape.lineTo(cx, cy);
                    } else {
                        triangleShape.moveTo(cx, cy);
                    }
                }
                case 2 -> {
                    triangleShape.moveTo(x, y);

                    final var ax = x + cellWidth;
                    final var ay = y;
                    if (!linkedWith.contains(cell.neightborAt(A))) {
                        triangleShape.lineTo(ax, ay);
                    } else {
                        triangleShape.moveTo(ax, ay);
                    }

                    final var cx = x;
                    final var cy = y + cellHeight;
                    if (!linkedWith.contains(cell.neightborAt(C))) {
                        triangleShape.lineTo(cx, cy);
                    } else {
                        triangleShape.moveTo(cx, cy);
                    }

                    final var bx = x;
                    final var by = y;
                    if (!linkedWith.contains(cell.neightborAt(B))) {
                        triangleShape.lineTo(bx, by);
                    } else {
                        triangleShape.moveTo(bx, by);
                    }
                }
                default -> {
                    triangleShape.moveTo(x + cellWidth, y);
                    final var bx = x + cellWidth;
                    final var by = y + cellHeight;
                    if (!linkedWith.contains(cell.neightborAt(B))) {
                        triangleShape.lineTo(bx, by);
                    } else {
                        triangleShape.moveTo(bx, by);
                    }

                    final var ax = x;
                    final var ay = y + cellHeight;
                    if (!linkedWith.contains(cell.neightborAt(A))) {
                        triangleShape.lineTo(ax, ay);
                    } else {
                        triangleShape.moveTo(ax, ay);
                    }

                    final var cx = x + cellWidth;
                    final var cy = y;
                    if (!linkedWith.contains(cell.neightborAt(C))) {
                        triangleShape.lineTo(cx, cy);
                    } else {
                        triangleShape.moveTo(cx, cy);
                    }
                }
            }
        } else {
            switch (cell.coordinate().column() % 4) {
                case 0 -> {
                    triangleShape.moveTo(x, y);

                    final var ax = x + cellWidth;
                    final var ay = y;
                    if (!linkedWith.contains(cell.neightborAt(A))) {
                        triangleShape.lineTo(ax, ay);
                    } else {
                        triangleShape.moveTo(ax, ay);
                    }

                    final var cx = x;
                    final var cy = y + cellHeight;
                    if (!linkedWith.contains(cell.neightborAt(C))) {
                        triangleShape.lineTo(cx, cy);
                    } else {
                        triangleShape.moveTo(cx, cy);
                    }

                    final var bx = x;
                    final var by = y;
                    if (!linkedWith.contains(cell.neightborAt(B))) {
                        triangleShape.lineTo(bx, by);
                    } else {
                        triangleShape.moveTo(bx, by);
                    }
                }
                case 1 -> {

                    triangleShape.moveTo(x + cellWidth, y);
                    final var bx = x + cellWidth;
                    final var by = y + cellHeight;
                    if (!linkedWith.contains(cell.neightborAt(B))) {
                        triangleShape.lineTo(bx, by);
                    } else {
                        triangleShape.moveTo(bx, by);
                    }

                    final var ax = x;
                    final var ay = y + cellHeight;
                    if (!linkedWith.contains(cell.neightborAt(A))) {
                        triangleShape.lineTo(ax, ay);
                    } else {
                        triangleShape.moveTo(ax, ay);
                    }

                    final var cx = x + cellWidth;
                    final var cy = y;
                    if (!linkedWith.contains(cell.neightborAt(C))) {
                        triangleShape.lineTo(cx, cy);
                    } else {
                        triangleShape.moveTo(cx, cy);
                    }
                }
                case 2 -> {
                    triangleShape.moveTo(x, y);
                    final var cx = x + cellWidth;
                    final var cy = y + cellHeight;
                    if (!linkedWith.contains(cell.neightborAt(C))) {
                        triangleShape.lineTo(cx, cy);
                    } else {
                        triangleShape.moveTo(cx, cy);
                    }

                    final var ax = x;
                    final var ay = y + cellHeight;
                    if (!linkedWith.contains(cell.neightborAt(A))) {
                        triangleShape.lineTo(ax, ay);
                    } else {
                        triangleShape.moveTo(ax, ay);
                    }

                    final var bx = x;
                    final var by = y;
                    if (!linkedWith.contains(cell.neightborAt(B))) {
                        triangleShape.lineTo(bx, by);
                    } else {
                        triangleShape.moveTo(bx, by);
                    }
                }
                default -> {
                    triangleShape.moveTo(x, y);
                    final var ax = x + cellWidth;
                    final var ay = y;
                    if (!linkedWith.contains(cell.neightborAt(A))) {
                        triangleShape.lineTo(ax, ay);
                    } else {
                        triangleShape.moveTo(ax, ay);
                    }

                    final var bx = x + cellWidth;
                    final var by = y + cellHeight;
                    if (!linkedWith.contains(cell.neightborAt(B))) {
                        triangleShape.lineTo(bx, by);
                    } else {
                        triangleShape.moveTo(bx, by);
                    }

                    final var cx = x;
                    final var cy = y;
                    if (!linkedWith.contains(cell.neightborAt(C))) {
                        triangleShape.lineTo(cx, cy);
                    } else {
                        triangleShape.moveTo(cx, cy);
                    }
                }
            }
        }

        return triangleShape;
    }
}

