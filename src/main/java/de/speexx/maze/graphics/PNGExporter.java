/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.graphics;

import java.awt.Color;
import java.awt.geom.Dimension2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import javax.imageio.ImageIO;

import static java.awt.Color.WHITE;
import static java.awt.RenderingHints.KEY_ANTIALIASING;
import static java.awt.RenderingHints.KEY_STROKE_CONTROL;
import static java.awt.RenderingHints.VALUE_ANTIALIAS_ON;
import static java.awt.RenderingHints.VALUE_STROKE_PURE;
import static java.awt.geom.AffineTransform.getTranslateInstance;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;


/**
 *
 * @author saschakohlmann
 */
public final class PNGExporter implements GraphicsFormatExporter {

    @Override
    public void export(final TilePattern tiles, final Dimension2D bounds, final OutputStream output) {
        try {
            final var imageSize = imageDimensionFor(tiles, bounds);
            final var bufferedImage = new BufferedImage((int) imageSize.getWidth(), (int) imageSize.getHeight(), TYPE_INT_ARGB);
            
            final var g2 = bufferedImage.createGraphics();
            g2.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_ON);
            g2.setRenderingHint(KEY_STROKE_CONTROL, VALUE_STROKE_PURE);
            g2.setColor(WHITE);
            g2.setBackground(new Color(255, 255, 255, 0));
            g2.fillRect(0, 0, (int) imageSize.getWidth(), (int) imageSize.getHeight());
            
            g2.transform(getTranslateInstance(bounds.getWidth(), bounds.getHeight()));
            g2.transform(tiles.zeroPointShift());
            
            new PatternPainter().paint(tiles, g2);
            
            ImageIO.write(bufferedImage, "PNG", output);
            final var c = "Created with SpeexX Maze".getBytes();
            output.write(c);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }
}
