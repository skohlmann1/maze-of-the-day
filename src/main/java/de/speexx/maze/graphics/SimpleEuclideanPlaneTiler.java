/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.graphics;

import de.speexx.maze.domain.Distances;
import de.speexx.maze.domain.GridCell;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Dimension2D;
import java.util.Optional;

import static java.awt.Color.BLACK;
import static java.awt.Color.WHITE;

import static de.speexx.maze.common.Assertions.assertNotNull;

/**
 *
 * @author saschakohlmann
 */
public abstract class SimpleEuclideanPlaneTiler<T extends GridCell> implements Tiler<T> {

    private static final Color KING_BLUE = new Color(65, 105, 225);
    private static final Color LIGHTSABER_RED = new Color(235, 33, 46);  // PANTONE: 1788 C
    private static final Color COMIC_BOOK_YELLOW = new Color(255,222,0);  // PANTONE: YELLOW C
    private static final Stroke BARRIER_STROKE = new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    static final Dimension DEFAULT_DIMENSION = new Dimension(20, 20);

    public static final StartGoalConfiguration WITH_START_GOAL = new StartGoalConfiguration(true, false);
    public static final StartGoalConfiguration WITH_PATH = new StartGoalConfiguration(true, true);
    public static final StartGoalConfiguration NOTHING = new StartGoalConfiguration(true, true);

    private final Dimension2D cellBounds;
    private final StartGoalConfiguration startGoalConfig;
    
    public SimpleEuclideanPlaneTiler() {
        this(DEFAULT_DIMENSION, NOTHING);
    }

    public SimpleEuclideanPlaneTiler(final Dimension2D cellBounds) {
        this(cellBounds, NOTHING);
    }

    public SimpleEuclideanPlaneTiler(final StartGoalConfiguration startGoalConfig) {
        this(DEFAULT_DIMENSION, startGoalConfig);
    }
    
    public SimpleEuclideanPlaneTiler(final Dimension2D cellBounds, final StartGoalConfiguration startGoalConfig) {
        this.cellBounds = assertNotNull(cellBounds, "CellBounds must be provided");
        this.startGoalConfig = assertNotNull(startGoalConfig, "StartGoalConfiguration must be provided");
    }
    
    protected Dimension2D cellBounds() {
        return cellBounds;
    }
    
    protected CellCoordinates calculateCellCoordinates(final GridCell cell, final Dimension2D cellDimension) {
        final var x = cell.coordinate().column() * cellDimension.getWidth();
        final var y = cell.coordinate().row() * cellDimension.getHeight();
        final var w = cellDimension.getWidth();
        final var h = cellDimension.getHeight();
        return new CellCoordinates(x, y, w, h);
    }

    protected Paint backgroundPaint() {
        return WHITE;
    }
    
    protected Stroke barrierStroke() {
        return BARRIER_STROKE;
    }
    
    protected Paint barrierPaint() {
        return BLACK;
    }
    
    protected Paint goalShapePaint() {
        return LIGHTSABER_RED;
    }
    
    protected Paint startShapePaint() {
        return KING_BLUE;
    }

    protected Paint pathPartShapePaint() {
        return COMIC_BOOK_YELLOW;
    }

    protected abstract Shape barrierShapeFor(final GridCell cell, final Distances distances, final CellCoordinates cellCoordinates);

    protected Optional<Shape> backgroundShapeFor(final GridCell cell, final Distances distances, final CellCoordinates cellCoordinates) {
        return Optional.empty();
    }

    protected Optional<Shape> overlayShapeFor(final GridCell cell, final Distances distances, final CellCoordinates cellCoordinates) {
        return Optional.empty();
    }
    
    protected final StartGoalConfiguration startGoalConfig() {
        return this.startGoalConfig;
    }

    public static record CellCoordinates(double x, double y, double width, double height) {}
    
    public static record StartGoalConfiguration(boolean isWithStartGoal, boolean isWithPath) {}
}
