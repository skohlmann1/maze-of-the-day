/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.graphics;

import java.awt.Color;
import java.awt.geom.Dimension2D;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UncheckedIOException;
import org.jfree.svg.SVGGraphics2D;
import org.jfree.svg.SVGUnits;

import static java.awt.Color.WHITE;
import static java.awt.geom.AffineTransform.getTranslateInstance;

/**
 *
 * @author saschakohlmann
 */
public final class SVGExporter implements GraphicsFormatExporter {

    @Override
    public void export(final TilePattern tiles, final Dimension2D bounds, final OutputStream output) {
        try {
            final var imageSize = imageDimensionFor(tiles, bounds);
            
            final var g2 = new SVGGraphics2D((int) imageSize.getWidth(), (int) imageSize.getHeight(), SVGUnits.PT);
            g2.setColor(WHITE);
            g2.setBackground(new Color(255, 255, 255, 0));
            g2.fillRect(0, 0, (int) imageSize.getWidth(), (int) imageSize.getHeight());
            
            g2.transform(getTranslateInstance(bounds.getWidth(), bounds.getHeight()));
            g2.transform(tiles.zeroPointShift());
            
            new PatternPainter().paint(tiles, g2);
            
            final var writer = new OutputStreamWriter(output);
            final var dom = g2.getSVGDocument();
            writer.append(dom);
            writer.append("<!-- Created with SpeexX Maze -->");
            writer.flush();
            output.flush();
            
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }
}
