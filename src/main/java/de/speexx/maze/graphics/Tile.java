/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.graphics;

import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.util.Optional;

import static de.speexx.maze.common.Assertions.assertNotNull;

/**
 *
 * @author saschakohlmann
 */
public interface Tile {
 
    Shape shape();
    Paint paint();
    Optional<Stroke> stroke();

    public static record DefaultTile(Shape shape, Paint paint, Optional<Stroke> stroke) implements Tile {

        public DefaultTile(final Shape shape, final Paint paint) {
            this(shape, paint, Optional.empty());
        }

        public DefaultTile(final Shape shape, final Paint paint, final Stroke stroke) {
            this(shape, paint, Optional.ofNullable(stroke));
        }

        public DefaultTile(final Shape shape, final Paint paint, final Optional<Stroke> stroke) {
            this.shape = assertNotNull(shape, "Shape must be provided");
            this.paint = assertNotNull(paint, "Paint must be provided");
            this.stroke = assertNotNull(stroke, "Optional<Stroke> must be provided");
        }
    }

    public default Rectangle2D bounds() {
        return shape().getBounds2D();
    }
}
