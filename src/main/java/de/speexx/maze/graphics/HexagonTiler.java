/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.graphics;

import de.speexx.maze.domain.Distances;
import de.speexx.maze.domain.GridCell;
import static de.speexx.maze.domain.HexagonNeighborhood.NORTH;
import static de.speexx.maze.domain.HexagonNeighborhood.NORTH_EAST;
import static de.speexx.maze.domain.HexagonNeighborhood.NORTH_WEST;
import static de.speexx.maze.domain.HexagonNeighborhood.SOUTH_EAST;
import static de.speexx.maze.domain.HexagonNeighborhood.SOUTH;
import static de.speexx.maze.domain.HexagonNeighborhood.SOUTH_WEST;
import de.speexx.maze.domain.Maze;
import static de.speexx.maze.graphics.SimpleEuclideanPlaneTiler.DEFAULT_DIMENSION;
import static de.speexx.maze.graphics.SimpleEuclideanPlaneTiler.NOTHING;
import de.speexx.maze.graphics.Tile.DefaultTile;
import de.speexx.maze.graphics.TilePattern.DefaultTilePattern;
import java.awt.Shape;
import java.awt.geom.Dimension2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import static java.lang.Double.max;
import static java.lang.StrictMath.sqrt;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Creates the {@linkplain TilePattern tile pattern} for uniform hexagon tiling.
 * 
 * <p><strong>Note:</strong> As the {@linkplain #cellBounds() cell bounds} may
 * fit an reqular hexagon. The tiler tries to find an optimal solution for
 * such a case.</p>
 * @author saschakohlmann
 */
public final class HexagonTiler<T extends GridCell> extends SimpleEuclideanPlaneTiler<T> {
    
    public HexagonTiler() {
        this(DEFAULT_DIMENSION, NOTHING);
    }

    public HexagonTiler(final Dimension2D cellBounds) {
        this(cellBounds, NOTHING);
    }

    public HexagonTiler(final StartGoalConfiguration startGoalConfig) {
        this(DEFAULT_DIMENSION, startGoalConfig);
    }
    
    public HexagonTiler(final Dimension2D cellBounds, final StartGoalConfiguration startGoalConfig) {
        super(cellBounds, startGoalConfig);
    }

    @Override
    public TilePattern tile(Maze<T> maze, Distances<T> distances) {
        final var perimeterRadius = optimalCircumRadius();
        final var regularHexagonRadius = new RegularHexagonRadius(perimeterRadius);
        final var baseHexagon = createWith(regularHexagonRadius);
        
        final var backgroundTiles = new ArrayList<Tile>();
        final var barrierTiles = new ArrayList<Tile>();
        final var overlayTiles = new ArrayList<Tile>();

        maze.cells().filter(cell -> !cell.isNull()).forEach(cell -> {

            final var cellCoordinates = calculateCellCoordinates(cell, regularHexagonRadius);

            final var backgroundShape = shapeFor(baseHexagon.copy(), cellCoordinates);
            final var startGoal = startGoalConfig();

            if (startGoal.isWithPath() && distances.contains(cell) && !distances.root().equals(cell) && !distances.max().cell().equals(cell)) {
                final var backgroundTile = new DefaultTile(backgroundShape, pathPartShapePaint());
                backgroundTiles.add(backgroundTile);
            } else if (startGoal.isWithStartGoal() && distances.max().cell().equals(cell)) {
                final var backgroundTile = new DefaultTile(backgroundShape, goalShapePaint());
                backgroundTiles.add(backgroundTile);
            } else if (startGoal.isWithStartGoal() && distances.root().equals(cell)) {
                final var backgroundTile = new DefaultTile(backgroundShape, startShapePaint());
                backgroundTiles.add(backgroundTile);
            } else {
                final var tile = new DefaultTile(backgroundShape, backgroundPaint());
                backgroundTiles.add(tile);
            }

            final var barrierShape = barrierShapeFor(cell, baseHexagon.copy(), cellCoordinates);
            final var barrierTile = new DefaultTile(barrierShape, barrierPaint(), barrierStroke());
            barrierTiles.add(barrierTile);
        });
        
        return new DefaultTilePattern(backgroundTiles, barrierTiles, overlayTiles);
    }
    
    private Shape barrierShapeFor(final GridCell cell, final Hexagon hexagon, final Point2D.Double cellCoordinates) {
        final var links = cell.linkedWith();
        var hex = hexagon;
        if (links.contains(cell.neightborAt(NORTH))) {
            hex = hex.withoutLineA();
        }
        if (links.contains(cell.neightborAt(NORTH_EAST))) {
            hex = hex.withoutLineB();
        }
        if (links.contains(cell.neightborAt(SOUTH_EAST))) {
            hex = hex.withoutLineC();
        }
        if (links.contains(cell.neightborAt(SOUTH))) {
            hex = hex.withoutLineD();
        }
        if (links.contains(cell.neightborAt(SOUTH_WEST))) {
            hex = hex.withoutLineE();
        }
        if (links.contains(cell.neightborAt(NORTH_WEST))) {
            hex = hex.withoutLineF();
        }

        return hex.asShape(cellCoordinates.getX(), cellCoordinates.getY());
    }

    private Point2D.Double calculateCellCoordinates(final GridCell cell, final RegularHexagonRadius radius) {
        final var coordinate = cell.coordinate();
        final var row = coordinate.row();
        final var column = coordinate.column();
        
        if (row % 2 == 0) {
            final var x = radius.inRadius() + radius.circumRadius() + (3 * radius.circumRadius() * column);
            final var y = (row / 2) * (2 * radius.inRadius());
            return new Point2D.Double(x, y);
        }
        final var x = (3 * radius.circumRadius()) * column + (radius.inRadius() / 2) - 1; // Not optimal. Needs rework
        final var y = (row / 2) * (2 * radius.inRadius()) + radius.inRadius();
        return new Point2D.Double(x, y);
    }
    
    private Shape shapeFor(final Hexagon hexagon, final Point2D.Double cellCoordinates) {
        return hexagon.asShape(cellCoordinates.getX(), cellCoordinates.getY());
    }
    
    private Hexagon createWith(final RegularHexagonRadius radius) {
        final var inRadius = radius.inRadius();
        final var circumRadius = radius.circumRadius();
        final var lineA = new Line(circumRadius / 2, 0, circumRadius / 2 + circumRadius, 0);
        final var lineB = new Line(circumRadius / 2 + circumRadius, 0, circumRadius * 2, inRadius);
        final var lineC = new Line(circumRadius * 2, inRadius, circumRadius / 2 + circumRadius, inRadius * 2);
        final var lineD = new Line(circumRadius / 2 + circumRadius, inRadius * 2, circumRadius / 2, inRadius * 2);
        final var lineE = new Line(circumRadius / 2, inRadius * 2, 0, inRadius);
        final var lineF = new Line(0, inRadius, circumRadius / 2, 0);

        return new Hexagon(lineA, lineB, lineC, lineD, lineE, lineF);
    }
    
    // Side length
    private double optimalCircumRadius() {
        return max(cellBounds().getWidth(), cellBounds().getHeight()) / 2;
    }
    
    /** Throws {@link UnsupportedOperationException}. */
    @Override
    protected Shape barrierShapeFor(final GridCell cell, final Distances distances, final CellCoordinates cellCoordinates) {
        throw new UnsupportedOperationException();
    }

    /** Throws {@link UnsupportedOperationException}. */
    @Override
    protected Optional<Shape> backgroundShapeFor(final GridCell cell, final Distances distances, final CellCoordinates cellCoordinates) {
        throw new UnsupportedOperationException();
    }
    
    /** Throws {@link UnsupportedOperationException}. */
    @Override
    protected Optional<Shape> overlayShapeFor(final GridCell cell, final Distances distances, final CellCoordinates cellCoordinates) {
        throw new UnsupportedOperationException();
    }

    private static record Line(double xStart, double yStart, double xEnd, double yEnd) {}
    
    private static record RegularHexagonRadius(double circumRadius) {
        
        private static final double RI_MULTIPLIER = sqrt(3) / 2;

        public double inRadius() {
            return circumRadius * RI_MULTIPLIER;
        }
    }
    
    private static record Hexagon(Line lineA, Line lineB, Line lineC, Line lineD, Line lineE, Line lineF) {

        public Rectangle2D bounds() {
            return asShape(0d, 0d).getBounds2D();
        }

        /** Provides all none {@code null} line instances. */
        public Stream<Line> lines() {
            return asList(lineA(), lineB(), lineC(), lineD(), lineE(), lineF()).stream().filter(line -> line != null);
        }
        
        public Hexagon withoutLineA() {
            return new Hexagon(null, lineB(), lineC(), lineD(), lineE(), lineF());
        }
        
        public Hexagon withoutLineB() {
            return new Hexagon(lineA(), null, lineC(), lineD(), lineE(), lineF());
        }
        
        public Hexagon withoutLineC() {
            return new Hexagon(lineA(), lineB(), null, lineD(), lineE(), lineF());
        }
        
        public Hexagon withoutLineD() {
            return new Hexagon(lineA(), lineB(), lineC(), null, lineE(), lineF());
        }

        public Hexagon withoutLineE() {
            return new Hexagon(lineA(), lineB(), lineC(), lineD(), null, lineF());
        }

        public Hexagon withoutLineF() {
            return new Hexagon(lineA(), lineB(), lineC(), lineD(), lineE(), null);
        }
        
        public Hexagon copy() {
            return new Hexagon(lineA(), lineB(), lineC(), lineD(), lineE(), lineF());
        }
        
        public Shape asShape(final double xShift, final double yShift) {
            final var path = new Path2D.Double();
            if (containsEmptyLine()) {
                this.lines().forEach(line -> {
                    path.moveTo(line.xStart() + xShift, line.yStart() + yShift);
                    path.lineTo(line.xEnd() + xShift, line.yEnd() + yShift);
                });
            } else {
                path.moveTo(lineA().xStart() + xShift, lineA().yStart() + yShift);
                this.lines().forEach(line -> {
                    path.lineTo(line.xEnd() + xShift, line.yEnd() + yShift);
                });
                path.moveTo(lineA().xStart() + xShift, lineA().yStart() + yShift);
            }
            return path;
        }
        
        private boolean containsEmptyLine() {
            return lineA() == null || lineB() == null || lineC() == null || lineD() == null || lineE() == null || lineD() == null;
        }
    }
}
