/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.graphics;

import de.speexx.maze.domain.Coordinate;
import de.speexx.maze.domain.Distances;
import de.speexx.maze.domain.GridCell;
import de.speexx.maze.domain.Maze;
import de.speexx.maze.graphics.Tile.DefaultTile;
import de.speexx.maze.graphics.TilePattern.DefaultTilePattern;
import java.awt.Shape;
import java.awt.geom.Dimension2D;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.Optional;

import static de.speexx.maze.domain.TriangleNeighborhood.A;
import static de.speexx.maze.domain.TriangleNeighborhood.B;
import static de.speexx.maze.domain.TriangleNeighborhood.C;

/**
 *
 * @author saschakohlmann
 */
public class TriangularTiler<T extends GridCell> extends SimpleEuclideanPlaneTiler<T> {

    public TriangularTiler() {
        this(DEFAULT_DIMENSION, NOTHING);
    }

    public TriangularTiler(final Dimension2D cellBounds) {
        this(cellBounds, NOTHING);
    }

    public TriangularTiler(final StartGoalConfiguration startGoalConfig) {
        this(DEFAULT_DIMENSION, startGoalConfig);
    }
    
    public TriangularTiler(final Dimension2D cellBounds, final StartGoalConfiguration startGoalConfig) {
        super(cellBounds, startGoalConfig);
    }
    
    @Override
    public TilePattern tile(final Maze<T> maze, final Distances<T> distances) {
        final var backgroundTiles = new ArrayList<Tile>();
        final var barrierTiles = new ArrayList<Tile>();
        final var overlayTiles = new ArrayList<Tile>();
        
        final var cellBounds = cellBounds();
        
        maze.cells().forEach(cell -> {

            final var cellCoordinates = calculateCellCoordinates(cell, cellBounds);
            backgroundShapeFor(cell, distances, cellCoordinates).ifPresent(backgroundShape -> {
                
                final var startGoal = startGoalConfig();
        
                if (startGoal.isWithStartGoal()) {
                    if (distances.root().equals(cell)) {
                        final var tile = new DefaultTile(backgroundShape, startShapePaint());
                        backgroundTiles.add(tile);
                        return;
                    } else if (distances.max().cell().equals(cell)) {
                        final var tile = new DefaultTile(backgroundShape, goalShapePaint());
                        backgroundTiles.add(tile);
                        return;
                    }
                }
        
                if (startGoal.isWithPath() && distances.contains(cell)) {
                    final var tile = new DefaultTile(backgroundShape, pathPartShapePaint());
                    backgroundTiles.add(tile);
                }
            });

            final var barrierShape = barrierShapeFor(cell, distances, cellCoordinates);
            final var barrierTile = new DefaultTile(barrierShape, barrierPaint(), barrierStroke());
            barrierTiles.add(barrierTile);
        });
        
        return new DefaultTilePattern(backgroundTiles, barrierTiles, overlayTiles);
    }
    
    @Override
    protected Shape barrierShapeFor(final GridCell cell, final Distances distances, final CellCoordinates cellCoordinates) {
        return barrierShape(cell, cellCoordinates);
    }

    @Override
    protected Optional<Shape> backgroundShapeFor(final GridCell cell, final Distances distances, final CellCoordinates cellCoordinates) {
        return Optional.of(backgroundShapeFor(cell, cellCoordinates));
    }

    private double calculateXAdjustment(final Coordinate coordinate, final CellCoordinates cellCoordinates) {
        
        final var column = coordinate.column();
        if (column == 0) {
            return 0;
        }
        if (column == 1) {
            return 0;
        }
        if (column % 2 == 0) {
            return column / 2 * cellCoordinates.width();
        }
        return (column - 1) / 2 * cellCoordinates.width();
    }

    Shape backgroundShapeFor(final GridCell cell, final CellCoordinates cellCoordinates) {
        final var coordinate = cell.coordinate();
        
        final var xAdjust = calculateXAdjustment(coordinate, cellCoordinates);
        final var cellWidth = cellCoordinates.width();
        final var cellHeight = cellCoordinates.height();
        final var triangleShape = new Path2D.Double();
                
        if (coordinate.row() % 2 == 0) {
            if (coordinate.column() % 2 == 0) {
                final var ax1 = coordinate.column() * cellWidth - xAdjust;
                final var ay1 = coordinate.row() * cellHeight + cellHeight;
                triangleShape.moveTo(ax1, ay1);
                
                final var ax2 = ax1 + cellWidth / 2;
                final var ay2 = ay1 - cellHeight;
                triangleShape.lineTo(ax2, ay2);

                final var bx1 = coordinate.column() * cellWidth + cellWidth - xAdjust;
                final var by1 = coordinate.row() * cellHeight + cellHeight;
                final var bx2 = ax1 - cellWidth / 2;
                final var by2 = ay1 - cellHeight;
                triangleShape.lineTo(bx1, by1);

                final var cx1 = coordinate.column() * cellWidth - xAdjust;
                final var cy1 = coordinate.row() * cellHeight + cellHeight;
                final var cx2 = coordinate.column() * cellWidth + cellWidth - xAdjust;
                final var cy2 = coordinate.row() * cellHeight + cellHeight;
                triangleShape.lineTo(cx2, cy2);

            } else {
 
                final var ax1 = coordinate.column() * cellWidth + cellWidth / 2 - xAdjust;
                final var ay1 = coordinate.row() * cellHeight;
                triangleShape.moveTo(ax1, ay1);

                final var ax2 = ax1 - cellWidth / 2;
                final var ay2 = ay1 + cellHeight;
                triangleShape.lineTo(ax2, ay2);

                final var bx1 = coordinate.column() * cellWidth - cellWidth / 2 - xAdjust;
                final var by1 = coordinate.row() * cellHeight;
                final var bx2 = ax1 + cellWidth / 2;
                final var by2 = ay1 + cellHeight;
                triangleShape.lineTo(bx1, by1);

                final var cx1 = coordinate.column() * cellWidth - cellWidth / 2 - xAdjust;
                final var cy1 = coordinate.row() * cellHeight;
                final var cx2 = coordinate.column() * cellWidth + cellWidth / 2 - xAdjust;
                final var cy2 = coordinate.row() * cellHeight;
                triangleShape.lineTo(cx2, cy2);
            }
        } else {
            if (coordinate.column() % 2 == 0) {

                final var ax1 = coordinate.column() * cellWidth + cellWidth - xAdjust;
                final var ay1 = coordinate.row() * cellHeight;
                triangleShape.moveTo(ax1, ay1);

                final var ax2 = ax1 - cellWidth / 2;
                final var ay2 = ay1 + cellHeight;
                triangleShape.lineTo(ax2, ay2);

                final var bx1 = coordinate.column() * cellWidth - xAdjust;
                final var by1 = coordinate.row() * cellHeight;
                final var bx2 = ax1 + cellWidth / 2;
                final var by2 = ay1 + cellHeight;
                triangleShape.lineTo(bx1, by1);

                final var cx1 = coordinate.column() * cellWidth - xAdjust;
                final var cy1 = coordinate.row() * cellHeight;
                final var cx2 = coordinate.column() * cellWidth + cellWidth - xAdjust;
                final var cy2 = ay1;
                triangleShape.lineTo(cx2, cy2);

            } else {

                final var ax1 = coordinate.column() * cellWidth - xAdjust;
                final var ay1 = coordinate.row() * cellHeight;
                triangleShape.moveTo(ax1, ay1);

                final var ax2 = ax1 - cellWidth / 2;
                final var ay2 = ay1 + cellHeight;
                triangleShape.lineTo(ax2, ay2);

                final var bx1 = coordinate.column() * cellWidth - xAdjust;
                final var by1 = coordinate.row() * cellHeight;
                final var bx2 = ax1 + cellWidth / 2;
                final var by2 = ay1 + cellHeight;
                triangleShape.lineTo(bx2, by2);

                final var cx1 = coordinate.column() * cellWidth - cellWidth / 2 - xAdjust;
                final var cy1 = coordinate.row() * cellHeight + cellHeight;
                final var cx2 = cx1 + cellWidth;
                final var cy2 = coordinate.row() * cellHeight + cellHeight;
                triangleShape.lineTo(cx2, cy2);
            }
        }
        
        triangleShape.closePath();
        return triangleShape;
    }

    Shape barrierShape(final GridCell cell, final CellCoordinates cellCoordinates) {
        final var coordinate = cell.coordinate();
        
        final var linkedWith = cell.linkedWith();
        final var xAdjust = calculateXAdjustment(coordinate, cellCoordinates);
        final var cellWidth = cellCoordinates.width();
        final var cellHeight = cellCoordinates.height();
        final var barrierShape = new Path2D.Double();
        
        if (coordinate.row() % 2 == 0) {
            if (coordinate.column() % 2 == 0) {
                var ax1 = coordinate.column() * cellWidth - xAdjust;
                var ay1 = coordinate.row() * cellHeight + cellHeight;
                barrierShape.moveTo(ax1, ay1);
                var ax2 = ax1 + cellWidth / 2;
                var ay2 = ay1 - cellHeight; 

                if (!linkedWith.contains(cell.neightborAt(A))) {
                    barrierShape.lineTo(ax2, ay2);
                }

                ax1 = coordinate.column() * cellWidth + cellWidth - xAdjust;
                ay1 = coordinate.row() * cellHeight + cellHeight;
                barrierShape.moveTo(ax1, ay1);
                ax2 = ax1 - cellWidth / 2;
                ay2 = ay1 - cellHeight;
                if (!linkedWith.contains(cell.neightborAt(B))) {
                    barrierShape.lineTo(ax2, ay2);
                } else {
                    barrierShape.moveTo(ax2, ay2);
                }

                ax1 = coordinate.column() * cellWidth - xAdjust;
                ay1 = coordinate.row() * cellHeight + cellHeight;
                barrierShape.moveTo(ax1, ay1);
                ax2 = coordinate.column() * cellWidth + cellWidth - xAdjust;
                ay2 = coordinate.row() * cellHeight + cellHeight;
                if (!linkedWith.contains(cell.neightborAt(C))) {
                    barrierShape.lineTo(ax2, ay2);
                }

            } else {
                var ax1 = coordinate.column() * cellWidth + cellWidth / 2 - xAdjust;
                var ay1 = coordinate.row() * cellHeight;
                barrierShape.moveTo(ax1, ay1);
                var ax2 = ax1 - cellWidth / 2;
                var ay2 = ay1 + cellHeight;
                if (!linkedWith.contains(cell.neightborAt(A))) {
                    barrierShape.lineTo(ax2, ay2);
                }

                ax1 = coordinate.column() * cellWidth - cellWidth / 2 - xAdjust;
                ay1 = coordinate.row() * cellHeight;
                barrierShape.moveTo(ax1, ay1);
                ax2 = ax1 + cellWidth / 2;
                ay2 = ay1 + cellHeight;
                if (!linkedWith.contains(cell.neightborAt(B))) {
                    barrierShape.lineTo(ax2, ay2);
                }

                ax1 = coordinate.column() * cellWidth - cellWidth / 2 - xAdjust;
                ay1 = coordinate.row() * cellHeight;
                barrierShape.moveTo(ax1, ay1);
                ax2 = coordinate.column() * cellWidth + cellWidth / 2 - xAdjust;
                ay2 = coordinate.row() * cellHeight;
                if (!linkedWith.contains(cell.neightborAt(C))) {
                    barrierShape.lineTo(ax2, ay2);
                }
            }
        } else {
            if (coordinate.column() % 2 == 0) {
                var ax1 = coordinate.column() * cellWidth + cellWidth - xAdjust;
                var ay1 = coordinate.row() * cellHeight;
                barrierShape.moveTo(ax1, ay1);
                var ax2 = ax1 - cellWidth / 2;
                var ay2 = ay1 + cellHeight;
                if (!linkedWith.contains(cell.neightborAt(A))) {
                    barrierShape.lineTo(ax2, ay2);
                }

                ax1 = coordinate.column() * cellWidth - xAdjust;
                ay1 = coordinate.row() * cellHeight;
                barrierShape.moveTo(ax1, ay1);
                ax2 = ax1 + cellWidth / 2;
                ay2 = ay1 + cellHeight;
                if (!linkedWith.contains(cell.neightborAt(B))) {
                    barrierShape.lineTo(ax2, ay2);
                }
                
                ax1 = coordinate.column() * cellWidth - xAdjust;
                ay1 = coordinate.row() * cellHeight;
                barrierShape.moveTo(ax1, ay1);
                ax2 = coordinate.column() * cellWidth + cellWidth - xAdjust;
                ay2 = ay1;
                if (!linkedWith.contains(cell.neightborAt(C))) {
                    barrierShape.lineTo(ax2, ay2);
                }
            } else {
                var ax1 = coordinate.column() * cellWidth - xAdjust;
                var ay1 = coordinate.row() * cellHeight;
                barrierShape.moveTo(ax1, ay1);
                var ax2 = ax1 - cellWidth / 2;
                var ay2 = ay1 + cellHeight;
                if (!linkedWith.contains(cell.neightborAt(A))) {
                    barrierShape.lineTo(ax2, ay2);
                }

                ax1 = coordinate.column() * cellWidth - xAdjust;
                ay1 = coordinate.row() * cellHeight;
                barrierShape.moveTo(ax1, ay1);
                ax2 = ax1 + cellWidth / 2;
                ay2 = ay1 + cellHeight;
                if (!linkedWith.contains(cell.neightborAt(B))) {
                    barrierShape.lineTo(ax2, ay2);
                }

                ax1 = coordinate.column() * cellWidth - cellWidth / 2 - xAdjust;
                ay1 = coordinate.row() * cellHeight + cellHeight;
                barrierShape.moveTo(ax1, ay1);
                ax2 = ax1 + cellWidth;
                ay2 = coordinate.row() * cellHeight + cellHeight;
                if (!linkedWith.contains(cell.neightborAt(C))) {
                    barrierShape.lineTo(ax2, ay2);
                }
            }
        }
        return barrierShape;
    }
}
