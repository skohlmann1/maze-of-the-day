/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.graphics;

import de.speexx.maze.domain.GridCell;
import static de.speexx.maze.domain.TriangleNeighborhood.A;
import static de.speexx.maze.domain.TriangleNeighborhood.B;
import static de.speexx.maze.domain.TriangleNeighborhood.C;
import static de.speexx.maze.graphics.SimpleEuclideanPlaneTiler.DEFAULT_DIMENSION;
import static de.speexx.maze.graphics.SimpleEuclideanPlaneTiler.NOTHING;
import de.speexx.maze.graphics.SimpleEuclideanPlaneTiler.StartGoalConfiguration;
import java.awt.Shape;
import java.awt.geom.Dimension2D;
import java.awt.geom.Path2D;
import static java.lang.Double.max;

/**
 *
 * @author saschakohlmann
 */
public class TriangularRightTiler extends TriangularTiler {

    public TriangularRightTiler() {
        this(DEFAULT_DIMENSION, NOTHING);
    }

    public TriangularRightTiler(final Dimension2D cellBounds) {
        this(cellBounds, NOTHING);
    }

    public TriangularRightTiler(final StartGoalConfiguration startGoalConfig) {
        this(DEFAULT_DIMENSION, startGoalConfig);
    }
    
    public TriangularRightTiler(final Dimension2D cellBounds, final StartGoalConfiguration startGoalConfig) {
        super(cellBounds, startGoalConfig);
    }

    @Override
    protected CellCoordinates calculateCellCoordinates(final GridCell cell, final Dimension2D cellDimension) {
        final var widthAndHeight = (int) max(cellDimension.getWidth(), cellDimension.getHeight());

        final var row = cell.coordinate().row();
        final var column = cell.coordinate().column();
        final var x = column * widthAndHeight;
        final var y = row % 2 == 0 ? (row / 2) * widthAndHeight : ((row - 1) / 2) * widthAndHeight;
        return new CellCoordinates(x, y, widthAndHeight, widthAndHeight);
    }

    @Override
    Shape backgroundShapeFor(final GridCell cell, final CellCoordinates cellCoordinates) {
        final var cellWidth = cellCoordinates.width();
        final var cellHeight = cellCoordinates.height();
        final var x = cellCoordinates.x();
        final var y = cellCoordinates.y();
        final var triangleShape = new Path2D.Double();

        triangleShape.moveTo(x, y);

        if (cell.coordinate().row() % 2 == 0) {
            triangleShape.lineTo(x + cellWidth, y);
            triangleShape.lineTo(x + cellWidth, y + cellHeight);
        } else {
            triangleShape.lineTo(x + cellWidth, y + cellHeight);
            triangleShape.lineTo(x, y + cellHeight);
        }
        triangleShape.lineTo(x, y);
        return triangleShape;
    }

    @Override
    Shape barrierShape(final GridCell cell, final CellCoordinates cellCoordinates) {
        final var linkedWith = cell.linkedWith();
        final var cellWidth = cellCoordinates.width();
        final var cellHeight = cellCoordinates.height();
        final var x = cellCoordinates.x();
        final var y = cellCoordinates.y();
        final var triangleShape = new Path2D.Double();

        triangleShape.moveTo(x, y);

        if (cell.coordinate().row() % 2 == 0) {
            final var ax = x + cellWidth;
            final var ay = y;
            if (!linkedWith.contains(cell.neightborAt(A))) {
                triangleShape.lineTo(ax, ay);
            } else {
                triangleShape.moveTo(ax, ay);
            }

            final var bx = x + cellWidth;
            final var by = y + cellHeight;
            if (!linkedWith.contains(cell.neightborAt(B))) {
                triangleShape.lineTo(bx, by);
            } else {
                triangleShape.moveTo(bx, by);
            }

            if (!linkedWith.contains(cell.neightborAt(C))) {
                triangleShape.lineTo(x, y);
            } else {
                triangleShape.moveTo(x, y);
            }
            
        } else {
            final var cx = x + cellWidth;
            final var cy = y + cellHeight;
            if (!linkedWith.contains(cell.neightborAt(C))) {
                triangleShape.lineTo(cx, cy);
            } else {
                triangleShape.moveTo(cx, cy);
            }

            final var ax = x ;
            final var ay = y + cellHeight;
            if (!linkedWith.contains(cell.neightborAt(A))) {
                triangleShape.lineTo(ax, ay);
            } else {
                triangleShape.moveTo(ax, ay);
            }

            if (!linkedWith.contains(cell.neightborAt(B))) {
                triangleShape.lineTo(x, y);
            } else {
                triangleShape.moveTo(x, y);
            }
        }

        return triangleShape;
    }
}
