/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.graphics;

import de.speexx.maze.domain.Distances;
import de.speexx.maze.domain.GridCell;
import de.speexx.maze.domain.ManhattenNeighborhood;
import de.speexx.maze.domain.Maze;
import de.speexx.maze.graphics.Tile.DefaultTile;
import de.speexx.maze.graphics.TilePattern.DefaultTilePattern;
import java.awt.Shape;
import java.awt.geom.Dimension2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Optional;

import static de.speexx.maze.domain.ManhattenNeighborhood.EAST;
import static de.speexx.maze.domain.ManhattenNeighborhood.NORTH;
import static de.speexx.maze.domain.ManhattenNeighborhood.SOUTH;
import static de.speexx.maze.domain.ManhattenNeighborhood.WEST;

/**
 *
 * @author saschakohlmann
 * @see ManhattenNeighborhood
 */
public final class ManhattenTiler<T extends GridCell> extends SimpleEuclideanPlaneTiler<T> {

    public ManhattenTiler() {
        this(DEFAULT_DIMENSION, NOTHING);
    }

    public ManhattenTiler(final Dimension2D cellBounds) {
        this(cellBounds, NOTHING);
    }

    public ManhattenTiler(final StartGoalConfiguration startGoalConfig) {
        this(DEFAULT_DIMENSION, startGoalConfig);
    }
    
    public ManhattenTiler(final Dimension2D cellBounds, final StartGoalConfiguration startGoalConfig) {
        super(cellBounds, startGoalConfig);
    }
    
    @Override
    public TilePattern tile(final Maze<T> maze, final Distances<T> distances) {

        final var backgroundTiles = new ArrayList<Tile>();
        final var barrierTiles = new ArrayList<Tile>();
        final var overlayTiles = new ArrayList<Tile>();
        
        final var cellBounds = cellBounds();
        
        maze.cells().filter(cell -> !cell.isNull()).forEach(cell -> {

            final var cellCoordinates = calculateCellCoordinates(cell, cellBounds);
            backgroundShapeFor(cell, distances, cellCoordinates).ifPresent(shape -> {
                final var tile = new DefaultTile(shape, backgroundPaint());
                backgroundTiles.add(tile);
            });

            final var barrierShape = barrierShapeFor(cell, distances, cellCoordinates);
            final var barrierTile = new DefaultTile(barrierShape, barrierPaint(), barrierStroke());
            barrierTiles.add(barrierTile);

            overlayShapeFor(cell, distances, cellCoordinates).ifPresent(overlayShape -> {
                final var startGoal = startGoalConfig();
        
                if (startGoal.isWithStartGoal()) {
                    if (distances.root().equals(cell)) {
                        final var tile = new DefaultTile(overlayShape, startShapePaint());
                        overlayTiles.add(tile);
                        return;
                    } else if (distances.max().cell().equals(cell)) {
                        final var tile = new DefaultTile(overlayShape, goalShapePaint());
                        overlayTiles.add(tile);
                        return;
                    }
                }
        
                if (startGoal.isWithPath() && distances.contains(cell)) {
                        final var tile = new DefaultTile(overlayShape, pathPartShapePaint());
                        overlayTiles.add(tile);
                        return;
                }
                final var tile = new DefaultTile(overlayShape, backgroundPaint());
                overlayTiles.add(tile);
            });
        });

        return new DefaultTilePattern(backgroundTiles, barrierTiles, overlayTiles);
    }

    @Override
    protected Optional<Shape> backgroundShapeFor(final GridCell cell, final Distances distances, final CellCoordinates cellCoordinates) {
        return Optional.of(new Rectangle2D.Double(cellCoordinates.x(), cellCoordinates.y(), cellCoordinates.width(), cellCoordinates.height()));
    }

    @Override
    protected Shape barrierShapeFor(GridCell cell, final Distances distances, CellCoordinates cellCoordinates) {

        final var x1 = cellCoordinates.x();
        final var y1 = cellCoordinates.y();
        final var x2 = (cell.coordinate().column() + 1) * cellCoordinates.width();
        final var y2 = (cell.coordinate().row() + 1) * cellCoordinates.height();
        
        final var links = cell.linkedWith();
        final var path = new Path2D.Double();

        path.moveTo(x1, y1);
        if (cell.neightborAt(NORTH).isNull()) {
            path.lineTo(x2, y1);
        } else {
            path.moveTo(x2, y1);
        }

        if (!links.contains(cell.neightborAt(EAST)) || cell.neightborAt(EAST).isNull()) {
            path.lineTo(x2, y2);
        } else {
            path.moveTo(x2, y2);
        }
        
        if (!links.contains(cell.neightborAt(SOUTH)) || cell.neightborAt(SOUTH).isNull()) {
            path.lineTo(x1, y2);
        } else {
            path.moveTo(x1, y2);
        }

        if (!links.contains(cell.neightborAt(WEST)) || cell.neightborAt(WEST).isNull()) {
            path.lineTo(x1, y1);
        }
        
        path.moveTo(x1, y1);
        path.closePath();
        return path;
    }

    @Override
    protected Optional<Shape> overlayShapeFor(final GridCell cell, final Distances distances, final CellCoordinates cellCoordinates) {
        final var startGoal = startGoalConfig();
        
        if (startGoal.isWithStartGoal()) {
            if (distances.root().equals(cell) || distances.max().cell().equals(cell)) {
                return Optional.of(shapeFor(cellCoordinates));
            }
        }
        
        if (startGoal.isWithPath() && distances.contains(cell)) {
            if (startGoal.isWithStartGoal() && (distances.root().equals(cell) || distances.max().cell().equals(cell))) {
                return Optional.empty();
            }
            return Optional.of(shapeFor(cellCoordinates));
        }
        return Optional.empty();
    }

    private Shape shapeFor(final CellCoordinates cellCoordinates) {
        return new Ellipse2D.Double(cellCoordinates.x() + 3, cellCoordinates.y() + 3, cellCoordinates.width() - 6, cellCoordinates.height() - 6);
    }
}
