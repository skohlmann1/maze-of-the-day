/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.graphics;

import java.awt.Dimension;
import java.awt.geom.Dimension2D;
import java.io.OutputStream;

/**
 *
 * @author saschakohlmann
 */
public interface GraphicsFormatExporter {
    void export(final TilePattern tiles, final Dimension2D bounds, final OutputStream output);


    public default Dimension2D imageDimensionFor(final TilePattern tiles, final Dimension2D inset) {
        final var bounds = tiles.bounds();
  
        return new Dimension(
                (int) bounds.getBounds2D().getWidth() + (int) inset.getWidth() * 2,
                (int) bounds.getBounds2D().getHeight() + (int) inset.getHeight() * 2
        );
    }
}
