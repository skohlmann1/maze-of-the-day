/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.graphics;

import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.Collection;

import static de.speexx.maze.common.Assertions.assertNotNull;
import static java.awt.geom.AffineTransform.getTranslateInstance;
import static java.lang.Double.MAX_VALUE;
import static java.lang.Double.MIN_VALUE;
import static java.util.List.copyOf;

/**
 *
 * @author saschakohlmann
 */
public interface TilePattern {
    
    Collection<Tile> background();
    Collection<Tile> barriers();
    Collection<Tile> overlay();
    
    public static record DefaultTilePattern(Collection<Tile> background, Collection<Tile> barriers, Collection<Tile> overlay) implements TilePattern {
        
        public DefaultTilePattern(final Collection<Tile> background, final Collection<Tile> barriers, final Collection<Tile> overlay) {
            this.background = copyOf(assertNotNull(background, "Background Tiles must be provided but may be empty"));
            this.barriers = copyOf(assertNotNull(barriers, "Barriers Tiles must be provided but may be empty"));
            this.overlay = copyOf(assertNotNull(overlay, "Overlay Tiles must be provided but may be empty"));
        }
    }
    
    public default Rectangle2D bounds() {
        var minX = MAX_VALUE;
        var maxX = MIN_VALUE;
        var minY = MAX_VALUE;
        var maxY = MIN_VALUE;
        
        for (final var bg : background()) {
            final var bounds = bg.bounds();
            if (bounds.getX() < minX) {
                minX = bounds.getX();
            }
            if (bounds.getMaxX() > maxX) {
                maxX = bounds.getMaxX();
            }
            if (bounds.getY() < minY) {
                minY = bounds.getY();
            }
            if (bounds.getMaxY() > maxY) {
                maxY = bounds.getMaxY();
            }
        }

        for (final var bg : barriers()) {
            final var bounds = bg.bounds();
            if (bounds.getX() < minX) {
                minX = bounds.getX();
            }
            if (bounds.getMaxX() > maxX) {
                maxX = bounds.getMaxX();
            }
            if (bounds.getY() < minY) {
                minY = bounds.getY();
            }
            if (bounds.getMaxY() > maxY) {
                maxY = bounds.getMaxY();
            }
        }

        for (final var bg : overlay()) {
            final var bounds = bg.bounds();
            if (bounds.getX() < minX) {
                minX = bounds.getX();
            }
            if (bounds.getMaxX() > maxX) {
                maxX = bounds.getMaxX();
            }
            if (bounds.getY() < minY) {
                minY = bounds.getY();
            }
            if (bounds.getMaxY() > maxY) {
                maxY = bounds.getMaxY();
            }
        }
        
        return new Rectangle2D.Double(minX, minY, -1 * minX + maxX, -1 * minY + maxY);
    }
    
    /**
     * The {@code AffineTransform} translates the {@link #bounds()} {@link Rectangle2D#getX() X}
     * and {@link Rectangle2D#getY() Y} point to {@code {0, 0}}}.
     */
    public default AffineTransform zeroPointShift() {
        final var bounds = bounds();
        return getTranslateInstance(-1 * bounds.getX(), -1 * bounds.getY());
    }
}
