function source_dir {
	$SOURCE = $PSCommandPath
    $DIR="$( dirname "$SOURCE" )"
    
    do{
        $SOURCE="$(readlink "$SOURCE")"
        $DIR="$( cd -P "$( dirname $SOURCE  )" -and Get-Location )"
    }
	while ( -h "$SOURCE" )
    $DIR="$( cd -P "$( dirname $SOURCE )" -and Get-Location )"
}
 

function create_classpath {
    dir=${1}
    CP="${dir}/lib"
    for (entry in "${dir}/lib"/*.jar) 
	{CP="${CP}:$entry"}
    echo $CP
}
$source_dir

$create_classpath 

java ${JAVA_OPTS} --enable-preview -cp "*.jar;lib\*" de.speexx.maze.app.App