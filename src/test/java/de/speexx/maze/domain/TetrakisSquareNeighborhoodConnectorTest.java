/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import static com.google.common.truth.Truth.assertThat;
import static de.speexx.maze.domain.TriangleNeighborhood.A;
import static de.speexx.maze.domain.TriangleNeighborhood.B;
import static de.speexx.maze.domain.TriangleNeighborhood.C;
import de.speexx.maze.domain.TriangleNeighborhood.TetrakisSquareNeighborhoodConnector;
import org.junit.jupiter.api.Test;

/**
 *
 * @author saschakohlmann
 */
public class TetrakisSquareNeighborhoodConnectorTest {
    
    @Test
    public void tetrakis_square_grid() {
        // When
        final var grid = new BaseGrid(new Dimension(6, 6), new TetrakisSquareNeighborhoodConnector());
        
        assertThat(grid.cellAt(Coordinate.ZERO).neightborAt(A).coordinate()).isEqualTo(new Coordinate(1, 0));
        assertThat(grid.cellAt(Coordinate.ZERO).neightborAt(B).isNull()).isTrue();
        assertThat(grid.cellAt(Coordinate.ZERO).neightborAt(C).coordinate()).isEqualTo(new Coordinate(0, 1));
        
        assertThat(grid.cellAt(new Coordinate(0, 1)).neightborAt(A).isNull()).isTrue();
        assertThat(grid.cellAt(new Coordinate(0, 1)).neightborAt(B).coordinate()).isEqualTo(new Coordinate(0, 2));
        assertThat(grid.cellAt(new Coordinate(0, 1)).neightborAt(C).coordinate()).isEqualTo(new Coordinate(0, 0));
        
        assertThat(grid.cellAt(new Coordinate(0, 2)).neightborAt(A).isNull()).isTrue();
        assertThat(grid.cellAt(new Coordinate(0, 2)).neightborAt(B).coordinate()).isEqualTo(new Coordinate(0, 1));
        assertThat(grid.cellAt(new Coordinate(0, 2)).neightborAt(C).coordinate()).isEqualTo(new Coordinate(0, 3));
        
        assertThat(grid.cellAt(new Coordinate(0, 3)).neightborAt(A).coordinate()).isEqualTo(new Coordinate(1, 3));
        assertThat(grid.cellAt(new Coordinate(0, 3)).neightborAt(B).coordinate()).isEqualTo(new Coordinate(0, 4));
        assertThat(grid.cellAt(new Coordinate(0, 3)).neightborAt(C).coordinate()).isEqualTo(new Coordinate(0, 2));
        
        assertThat(grid.cellAt(new Coordinate(1, 0)).neightborAt(A).coordinate()).isEqualTo(new Coordinate(0, 0));
        assertThat(grid.cellAt(new Coordinate(1, 0)).neightborAt(B).isNull()).isTrue();
        assertThat(grid.cellAt(new Coordinate(1, 0)).neightborAt(C).coordinate()).isEqualTo(new Coordinate(1, 1));
        
        assertThat(grid.cellAt(new Coordinate(1, 1)).neightborAt(A).coordinate()).isEqualTo(new Coordinate(2, 1));
        assertThat(grid.cellAt(new Coordinate(1, 1)).neightborAt(B).coordinate()).isEqualTo(new Coordinate(1, 2));
        assertThat(grid.cellAt(new Coordinate(1, 1)).neightborAt(C).coordinate()).isEqualTo(new Coordinate(1, 0));
        
        assertThat(grid.cellAt(new Coordinate(1, 2)).neightborAt(A).coordinate()).isEqualTo(new Coordinate(2, 2));
        assertThat(grid.cellAt(new Coordinate(1, 2)).neightborAt(B).coordinate()).isEqualTo(new Coordinate(1, 1));
        assertThat(grid.cellAt(new Coordinate(1, 2)).neightborAt(C).coordinate()).isEqualTo(new Coordinate(1, 3));
        
        assertThat(grid.cellAt(new Coordinate(1, 3)).neightborAt(A).coordinate()).isEqualTo(new Coordinate(0, 3));
        assertThat(grid.cellAt(new Coordinate(1, 3)).neightborAt(B).coordinate()).isEqualTo(new Coordinate(1, 4));
        assertThat(grid.cellAt(new Coordinate(1, 3)).neightborAt(C).coordinate()).isEqualTo(new Coordinate(1, 2));
    }
}
