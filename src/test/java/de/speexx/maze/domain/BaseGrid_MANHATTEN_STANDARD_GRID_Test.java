/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.domain.GridCell;
import de.speexx.maze.domain.Dimension;
import de.speexx.maze.domain.BaseGrid;
import de.speexx.maze.domain.Coordinate;
import static com.google.common.truth.Truth.assertThat;
import org.junit.jupiter.api.Test;

import static de.speexx.maze.domain.ManhattenNeighborhood.NORTH;
import static de.speexx.maze.domain.ManhattenNeighborhood.SOUTH;
import static de.speexx.maze.domain.ManhattenNeighborhood.WEST;
import static de.speexx.maze.domain.ManhattenNeighborhood.EAST;

/**
 *
 * @author saschakohlmann
 */
public class BaseGrid_MANHATTEN_STANDARD_GRID_Test {
 
    @Test
    public void simple3x3BaseGrid() {

        // Given
        final var dim = new Dimension(2, 3);
        final var grid = new BaseGrid(dim);

        final var zeroZero = new Coordinate(0, 0);
        assertThat(grid.cellAt(zeroZero).coordinate()).isEqualTo(zeroZero);
        assertThat(grid.cellAt(zeroZero).neightborAt(NORTH).isNull()).isTrue();
        assertThat(grid.cellAt(zeroZero).neightborAt(SOUTH).coordinate()).isEqualTo(new Coordinate(1, 0));
        assertThat(grid.cellAt(zeroZero).neightborAt(WEST)).isEqualTo(GridCell.NULL);
        assertThat(grid.cellAt(zeroZero).neightborAt(EAST).coordinate()).isEqualTo(new Coordinate(0, 1));

        final var zeroOne = new Coordinate(0, 1);
        assertThat(grid.cellAt(zeroOne).coordinate()).isEqualTo(zeroOne);
        assertThat(grid.cellAt(zeroOne).neightborAt(NORTH)).isEqualTo(GridCell.NULL);
        assertThat(grid.cellAt(zeroOne).neightborAt(SOUTH).coordinate()).isEqualTo(new Coordinate(1, 1));
        assertThat(grid.cellAt(zeroOne).neightborAt(WEST).coordinate()).isEqualTo(new Coordinate(0, 0));
        assertThat(grid.cellAt(zeroOne).neightborAt(EAST).coordinate()).isEqualTo(new Coordinate(0, 2));

        final var zeroTwo = new Coordinate(0, 2);
        assertThat(grid.cellAt(zeroTwo).coordinate()).isEqualTo(zeroTwo);
        assertThat(grid.cellAt(zeroTwo).neightborAt(NORTH)).isEqualTo(GridCell.NULL);
        assertThat(grid.cellAt(zeroTwo).neightborAt(SOUTH).coordinate()).isEqualTo(new Coordinate(1, 2));
        assertThat(grid.cellAt(zeroTwo).neightborAt(WEST).coordinate()).isEqualTo(new Coordinate(0, 1));
        assertThat(grid.cellAt(zeroTwo).neightborAt(EAST)).isEqualTo(GridCell.NULL);


        final var oneZero = new Coordinate(1, 0);
        assertThat(grid.cellAt(oneZero).coordinate()).isEqualTo(oneZero);
        assertThat(grid.cellAt(oneZero).neightborAt(NORTH).coordinate()).isEqualTo(new Coordinate(0, 0));
        assertThat(grid.cellAt(oneZero).neightborAt(SOUTH)).isEqualTo(GridCell.NULL);
        assertThat(grid.cellAt(oneZero).neightborAt(WEST)).isEqualTo(GridCell.NULL);
        assertThat(grid.cellAt(oneZero).neightborAt(EAST).coordinate()).isEqualTo(new Coordinate(1, 1));

        final var oneOne = new Coordinate(1, 1);
        assertThat(grid.cellAt(oneOne).coordinate()).isEqualTo(oneOne);
        assertThat(grid.cellAt(oneOne).neightborAt(NORTH).coordinate()).isEqualTo(new Coordinate(0, 1));
        assertThat(grid.cellAt(oneOne).neightborAt(SOUTH)).isEqualTo(GridCell.NULL);
        assertThat(grid.cellAt(oneOne).neightborAt(WEST).coordinate()).isEqualTo(new Coordinate(1, 0));
        assertThat(grid.cellAt(oneOne).neightborAt(EAST).coordinate()).isEqualTo(new Coordinate(1, 2));

        final var oneTwo = new Coordinate(1, 2);
        assertThat(grid.cellAt(oneTwo).coordinate()).isEqualTo(oneTwo);
        assertThat(grid.cellAt(oneTwo).neightborAt(NORTH).coordinate()).isEqualTo(new Coordinate(0, 2));
        assertThat(grid.cellAt(oneTwo).neightborAt(SOUTH)).isEqualTo(GridCell.NULL);
        assertThat(grid.cellAt(oneTwo).neightborAt(WEST).coordinate()).isEqualTo(new Coordinate(1, 1));
        assertThat(grid.cellAt(oneTwo).neightborAt(EAST)).isEqualTo(GridCell.NULL);
    }
}
