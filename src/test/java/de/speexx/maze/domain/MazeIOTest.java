/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.domain.MazeIO.GridDistancesTuple;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Optional;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static com.google.common.truth.Truth8.assertThat;

/**
 *
 * @author saschakohlmann
 */
public class MazeIOTest {
    
    @Test
    public void writeReadBaseGrid() throws Exception {
        // Given
        final var grid = new BaseGrid(new Dimension(2, 2));
        new AldousBroder().on(grid);
        final var distances = grid.cellAt(Coordinate.ZERO).distances();
        
        final var output = new ByteArrayOutputStream();
        MazeIO.write(new GridDistancesTuple(grid, Optional.of(distances)), output);
        final var jsonBytes = output.toByteArray();
        final var jsonInput = new ByteArrayInputStream(jsonBytes);
        
        // When
        final var gridTuple = MazeIO.read(jsonInput);
        
        // Then
        gridTuple.grid().cells().forEach(cell -> {
            final var coordinate = cell.coordinate();
            final var sourceCell = grid.cellAt(coordinate);
            assertThat(cell).isEqualTo(sourceCell);
            assertThat(cell.neighbors()).isEqualTo(sourceCell.neighbors());
            assertThat(cell.linkedWith()).isEqualTo(sourceCell.linkedWith());
        });
    }

    @Test
    public void writeReadTrinagleMaskedBaseGrid() throws Exception {
        // Given
        final var mask = MaskSupport.triangled(6);
        final var grid = new MaskedGrid(mask);
        new AldousBroder().on(grid);
        final var distances = grid.randomCell().distances();
        final var path = distances.pathTo(distances.max().cell());
        
        final var output = new ByteArrayOutputStream();
        MazeIO.write(new GridDistancesTuple(grid, Optional.of(path)), output);
        final var jsonBytes = output.toByteArray();
        final var jsonInput = new ByteArrayInputStream(jsonBytes);
        
        // When
        final var gridTuple = MazeIO.read(jsonInput);
        
        // Then
        gridTuple.grid().cells().forEach(cell -> {
            final var coordinate = cell.coordinate();
            final var sourceCell = grid.cellAt(coordinate);
            assertThat(cell).isEqualTo(sourceCell);
            assertThat(cell.neighbors()).isEqualTo(sourceCell.neighbors());
            assertThat(cell.linkedWith()).isEqualTo(sourceCell.linkedWith());
        });
        
        assertThat(gridTuple.grid().cellAt(Coordinate.ZERO).isNull()).isTrue();
        
        assertThat(gridTuple.distances().get().root()).isEqualTo(path.root());
        assertThat(gridTuple.distances().get().max().cell()).isEqualTo(path.max().cell());
    }

    @Test
    public void writeReaddBaseGrid_without_Distanecs() throws Exception {
        // Given
        final var mask = MaskSupport.triangled(6);
        final var grid = new MaskedGrid(mask);
        new AldousBroder().on(grid);
        
        final var output = new ByteArrayOutputStream();
        MazeIO.write(new GridDistancesTuple(grid, Optional.empty()), output);
        MazeIO.write(new GridDistancesTuple(grid, Optional.empty()), System.err);
        final var jsonBytes = output.toByteArray();
        final var jsonInput = new ByteArrayInputStream(jsonBytes);
        
        // When
        final var gridTuple = MazeIO.read(jsonInput);
        
        assertThat(gridTuple.distances()).isEmpty();
    }
}
