/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.domain.Coordinate;
import de.speexx.maze.domain.MaskSupport;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;
import static de.speexx.maze.domain.Mask.SimpleMaskType.AVAILABLE;
import static de.speexx.maze.domain.Mask.SimpleMaskType.EMPTY;
import static de.speexx.maze.domain.Mask.SimpleMaskType.LINKED;

/**
 *
 * @author saschakohlmann
 */
public class MaskSupportTest {
    
    @Test
    public void loadSimpleTextMask() throws Exception {
        // Given
        final var uri = getClass().getResource("/test-masks/simple.mask").toURI();
        
        // When
        final var mask = MaskSupport.loadTextMask(uri.toURL().openStream());
        
        // Then
        assertThat(mask.dimension().rows()).isEqualTo(15);
        assertThat(mask.dimension().columns()).isEqualTo(17);
        
        assertThat(mask.configurationFor(Coordinate.ZERO)).isEqualTo(EMPTY);
        assertThat(mask.configurationFor(new Coordinate(14, 16))).isEqualTo(EMPTY);

        assertThat(mask.configurationFor(new Coordinate(0, 3))).isEqualTo(AVAILABLE);
        assertThat(mask.configurationFor(new Coordinate(7, 9))).isEqualTo(LINKED);
    }

    @Test
    public void loadMask_with_PNG() throws Exception {
        // Given
        final var uri = getClass().getResource("/test-masks/test-mask.png").toURI();
        
        // When
        final var mask = MaskSupport.loadMask(uri.toURL().openStream());
        
        // Then
        assertThat(mask.dimension().rows()).isEqualTo(4);
        assertThat(mask.dimension().columns()).isEqualTo(4);
        
        assertThat(mask.configurationFor(Coordinate.ZERO)).isEqualTo(EMPTY);
        assertThat(mask.configurationFor(new Coordinate(1, 1))).isEqualTo(LINKED);
        assertThat(mask.configurationFor(new Coordinate(3, 3))).isEqualTo(AVAILABLE);
    }
}
