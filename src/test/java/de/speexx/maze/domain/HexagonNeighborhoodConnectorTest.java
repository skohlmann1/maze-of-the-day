/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import static com.google.common.truth.Truth.assertThat;
import static de.speexx.maze.domain.HexagonNeighborhood.HEXAGON_STANDARD_GRID;
import static de.speexx.maze.domain.HexagonNeighborhood.NORTH;
import static de.speexx.maze.domain.HexagonNeighborhood.NORTH_EAST;
import static de.speexx.maze.domain.HexagonNeighborhood.NORTH_WEST;
import static de.speexx.maze.domain.HexagonNeighborhood.SOUTH;
import static de.speexx.maze.domain.HexagonNeighborhood.SOUTH_EAST;
import static de.speexx.maze.domain.HexagonNeighborhood.SOUTH_WEST;
import org.junit.jupiter.api.Test;

/**
 *
 * @author saschakohlmann
 */
public class HexagonNeighborhoodConnectorTest {
    
    @Test
    public void simple_hexagon_grid() {
        // When
        final var grid = new BaseGrid(new Dimension(6, 6), HEXAGON_STANDARD_GRID);
        
        // Then
        assertThat(grid.cellAt(Coordinate.ZERO).neightborAt(NORTH_WEST).isNull()).isTrue();
        assertThat(grid.cellAt(Coordinate.ZERO).neightborAt(NORTH_EAST).isNull()).isTrue();
        assertThat(grid.cellAt(Coordinate.ZERO).neightborAt(NORTH).isNull()).isTrue();
        assertThat(grid.cellAt(Coordinate.ZERO).neightborAt(SOUTH_WEST).coordinate()).isEqualTo(new Coordinate(1, 0));
        assertThat(grid.cellAt(Coordinate.ZERO).neightborAt(SOUTH_EAST).coordinate()).isEqualTo(new Coordinate(1, 1));
        assertThat(grid.cellAt(Coordinate.ZERO).neightborAt(SOUTH).coordinate()).isEqualTo(new Coordinate(2, 0));

        assertThat(grid.cellAt(new Coordinate(1, 1)).neightborAt(NORTH_WEST).coordinate()).isEqualTo(Coordinate.ZERO);
        assertThat(grid.cellAt(new Coordinate(1, 1)).neightborAt(NORTH_EAST).coordinate()).isEqualTo(new Coordinate(0, 1));
        assertThat(grid.cellAt(new Coordinate(1, 1)).neightborAt(NORTH).isNull()).isTrue();
        assertThat(grid.cellAt(new Coordinate(1, 1)).neightborAt(SOUTH_WEST).coordinate()).isEqualTo(new Coordinate(2, 0));
        assertThat(grid.cellAt(new Coordinate(1, 1)).neightborAt(SOUTH_EAST).coordinate()).isEqualTo(new Coordinate(2, 1));
        assertThat(grid.cellAt(new Coordinate(1, 1)).neightborAt(SOUTH).coordinate()).isEqualTo(new Coordinate(3, 1));

        assertThat(grid.cellAt(new Coordinate(1, 0)).neightborAt(NORTH_WEST).isNull()).isTrue();
        assertThat(grid.cellAt(new Coordinate(1, 0)).neightborAt(NORTH_EAST).coordinate()).isEqualTo(new Coordinate(0, 0));
        assertThat(grid.cellAt(new Coordinate(1, 0)).neightborAt(NORTH).isNull()).isTrue();
        assertThat(grid.cellAt(new Coordinate(1, 0)).neightborAt(SOUTH_WEST).isNull()).isTrue();
        assertThat(grid.cellAt(new Coordinate(1, 0)).neightborAt(SOUTH_EAST).coordinate()).isEqualTo(new Coordinate(2, 0));
        assertThat(grid.cellAt(new Coordinate(1, 0)).neightborAt(SOUTH).coordinate()).isEqualTo(new Coordinate(3, 0));

        assertThat(grid.cellAt(new Coordinate(2, 0)).neightborAt(NORTH_WEST).coordinate()).isEqualTo(new Coordinate(1, 0));
        assertThat(grid.cellAt(new Coordinate(2, 0)).neightborAt(NORTH_EAST).coordinate()).isEqualTo(new Coordinate(1, 1));
        assertThat(grid.cellAt(new Coordinate(2, 0)).neightborAt(NORTH).coordinate()).isEqualTo(new Coordinate(0, 0));
        assertThat(grid.cellAt(new Coordinate(2, 0)).neightborAt(SOUTH_WEST).coordinate()).isEqualTo(new Coordinate(3, 0));
        assertThat(grid.cellAt(new Coordinate(2, 0)).neightborAt(SOUTH_EAST).coordinate()).isEqualTo(new Coordinate(3, 1));
        assertThat(grid.cellAt(new Coordinate(2, 0)).neightborAt(SOUTH).coordinate()).isEqualTo(new Coordinate(4, 0));

        assertThat(grid.cellAt(new Coordinate(3, 0)).neightborAt(NORTH_WEST).isNull()).isTrue();
        assertThat(grid.cellAt(new Coordinate(3, 0)).neightborAt(NORTH_EAST).coordinate()).isEqualTo(new Coordinate(2, 0));
        assertThat(grid.cellAt(new Coordinate(3, 0)).neightborAt(NORTH).coordinate()).isEqualTo(new Coordinate(1, 0));
        assertThat(grid.cellAt(new Coordinate(3, 0)).neightborAt(SOUTH_WEST).isNull()).isTrue();
        assertThat(grid.cellAt(new Coordinate(3, 0)).neightborAt(SOUTH_EAST).coordinate()).isEqualTo(new Coordinate(4, 0));
        assertThat(grid.cellAt(new Coordinate(3, 0)).neightborAt(SOUTH).coordinate()).isEqualTo(new Coordinate(5, 0));

        assertThat(grid.cellAt(new Coordinate(4, 0)).neightborAt(SOUTH).isNull()).isTrue();
    }
}
