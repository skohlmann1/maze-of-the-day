/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.domain.GridCell;
import de.speexx.maze.domain.Coordinate;
import de.speexx.maze.domain.Cell;
import de.speexx.maze.domain.Distances;
import static com.google.common.truth.Truth.assertThat;
import org.junit.jupiter.api.Test;

import static de.speexx.maze.domain.ManhattenNeighborhood.NORTH;
import static de.speexx.maze.domain.ManhattenNeighborhood.SOUTH;
import static de.speexx.maze.domain.ManhattenNeighborhood.WEST;
import static de.speexx.maze.domain.ManhattenNeighborhood.EAST;

/**
 *
 * @author saschakohlmann
 */
public class Cell_Distances_withNeighborCellImplementation_Test {
    
    @Test
    public void root_cell() {
        // Given
        final var rootCell = coordinateZeroCellFor2x2Grid();
        
        // When
        final var distances = rootCell.distances();
        
        // Then
        assertThat(distances.root()).isEqualTo(new GridCell(Coordinate.ZERO));
    }
    
    @Test
    public void max_distance() {
        // Given
        final var rootCell = coordinateZeroCellFor2x2Grid();
        
        // When
        final var distances = rootCell.distances();
        
        // Then
        assertThat(distances.max()).isEqualTo(new Distances.Distance(new GridCell(new Coordinate(0, 1)), 3));
    }

    @Test
    public void pathTo_distance() {
        // Given
        final var rootCell = coordinateZeroCellFor2x2Grid();
        final var baseDistances = rootCell.distances();

        // When
        // WTF, where is the fucking problem :-(
//        final var pathToDistances = baseDistances.pathTo(new GridCell(new Coordinate(1, 1)));
        
        // Then
    }
    
    private Cell coordinateZeroCellFor2x2Grid() {
        final var cell0x0 = new GridCell(Coordinate.ZERO);
        final var cell1x0 = new GridCell(new Coordinate(1, 0));
        final var cell0x1 = new GridCell(new Coordinate(0, 1));
        final var cell1x1 = new GridCell(new Coordinate(1, 1));
        
        cell0x0.withNeightborAt(NORTH, cell1x0);
        cell0x0.withNeightborAt(EAST, cell0x1);
        cell1x0.withNeightborAt(NORTH, cell0x0);
        cell1x0.withNeightborAt(EAST, cell1x1);
        cell0x1.withNeightborAt(SOUTH, cell1x1);
        cell0x1.withNeightborAt(WEST, cell0x0);
        cell1x1.withNeightborAt(NORTH, cell0x1);
        cell1x1.withNeightborAt(WEST, cell1x0);
        
        cell0x0.linkWith(cell1x0);
        cell1x0.linkWith(cell1x1);
        cell1x1.linkWith(cell0x1);
        
        return cell0x0;
    }
}
