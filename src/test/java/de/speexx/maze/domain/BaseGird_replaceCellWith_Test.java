/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.domain.GridCell;
import de.speexx.maze.domain.Dimension;
import de.speexx.maze.domain.BaseGrid;
import de.speexx.maze.domain.Coordinate;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

import static de.speexx.maze.domain.ManhattenNeighborhood.NORTH;
import static de.speexx.maze.domain.ManhattenNeighborhood.EAST;
import static de.speexx.maze.domain.ManhattenNeighborhood.MANHATTEN_STANDARD_GRID;

/**
 *
 * @author saschakohlmann
 */
public class BaseGird_replaceCellWith_Test {
 
    @Test
    public void removeCell() {
        // Given
        final var grid = new BaseGrid(new Dimension(3, 3), MANHATTEN_STANDARD_GRID);
        final var centeredCell = grid.cellAt(new Coordinate(1, 1));
        final var north = centeredCell.neightborAt(NORTH);
        centeredCell.linkWith(north);
        final var east = centeredCell.neightborAt(EAST);
        centeredCell.linkWith(east);
        
        // When
        final var oldCenteredCell = grid.removeCell(centeredCell);

        // Then
        assertThat(oldCenteredCell.neighbors()).isEmpty();
        assertThat(oldCenteredCell.linkedWith()).isEmpty();

    }
    @Test
    public void removedCell_is_NULL() {
        // Given
        final var grid = new BaseGrid(new Dimension(3, 3), MANHATTEN_STANDARD_GRID);
        final var centeredCell = grid.cellAt(new Coordinate(1, 1));
        
        // When
        grid.removeCell(centeredCell);

        // Then
        assertThat(grid.cellAt(centeredCell.coordinate())).isInstanceOf(GridCell.NULL.getClass());
    }
}
