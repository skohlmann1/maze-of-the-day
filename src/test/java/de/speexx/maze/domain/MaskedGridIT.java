/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.domain.MaskedGrid;
import de.speexx.maze.domain.ManhattenNeighborhoodBaseGridPrinter;
import de.speexx.maze.domain.AldousBroder;
import de.speexx.maze.domain.RecursiveBacktracker;
import de.speexx.maze.domain.MaskSupport;
import java.io.OutputStreamWriter;
import org.junit.jupiter.api.Test;

/**
 *
 * @author saschakohlmann
 */
public class MaskedGridIT {
    
    @Test
    public void textMaskedGrid() throws Exception {
        // Given
        final var uri = getClass().getResource("/masks/simple.mask").toURI();
        final var mask = MaskSupport.loadTextMask(uri.toURL().openStream());
        
        // When
        final var grid = new MaskedGrid(mask);
        new AldousBroder().on(grid);
        
        // Then
        new ManhattenNeighborhoodBaseGridPrinter().print(grid, new OutputStreamWriter(System.out));
    }
    
    @Test
    public void imageMaskedGrid() throws Exception {
        // Given
        final var uri = getClass().getResource("/masks/jojo.mask.png").toURI();
        final var mask = MaskSupport.loadImageMask(uri.toURL().openStream());
        
        // When
        final var grid = new MaskedGrid(mask);
        new RecursiveBacktracker().on(grid);
        
        // Then
        new ManhattenNeighborhoodBaseGridPrinter().print(grid, new OutputStreamWriter(System.out));
    }
}
