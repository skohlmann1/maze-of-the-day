/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.domain.Distances;
import static com.google.common.truth.Truth.assertThat;

import org.junit.jupiter.api.Test;

import static de.speexx.maze.domain.GridCell.NULL;

/**
 *
 * @author saschakohlmann
 */
public class DistanceTest {
    
    @Test
    public void compareTo() {
        
        // Given
        final var distance1 = new Distances.Distance(NULL, 1);
        final var distance2 = new Distances.Distance(NULL, 2);
        
        // When
        final var result = distance1.compareTo(distance2);
        
        // Then
        assertThat(result).isLessThan(0);
    }
}
