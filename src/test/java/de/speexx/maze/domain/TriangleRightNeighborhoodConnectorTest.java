/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import static com.google.common.truth.Truth.assertThat;
import static de.speexx.maze.domain.TriangleNeighborhood.A;
import static de.speexx.maze.domain.TriangleNeighborhood.B;
import static de.speexx.maze.domain.TriangleNeighborhood.C;
import de.speexx.maze.domain.TriangleNeighborhood.TriangleNeighborhoodConnector;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 *
 * @author saschakohlmann
 */
public class TriangleRightNeighborhoodConnectorTest {
    
    @Test
    @Disabled
    public void triangle_right_grid() {
        // When
        final var grid = new BaseGrid(new Dimension(6, 6), new TriangleNeighborhoodConnector());

        // Then
        assertThat(grid.cellAt(Coordinate.ZERO).neightborAt(A).isNull()).isTrue();
        assertThat(grid.cellAt(Coordinate.ZERO).neightborAt(B).coordinate()).isEqualTo(new Coordinate(1, 1));
        assertThat(grid.cellAt(Coordinate.ZERO).neightborAt(C).coordinate()).isEqualTo(new Coordinate(1, 0));

        assertThat(grid.cellAt(new Coordinate(1, 0)).neightborAt(A).coordinate()).isEqualTo(new Coordinate(2, 0));
        assertThat(grid.cellAt(new Coordinate(1, 0)).neightborAt(B).isNull()).isTrue();
        assertThat(grid.cellAt(new Coordinate(1, 0)).neightborAt(C).coordinate()).isEqualTo(new Coordinate(0, 0));

        assertThat(grid.cellAt(new Coordinate(2, 1)).neightborAt(A).coordinate()).isEqualTo(new Coordinate(1, 1));
        assertThat(grid.cellAt(new Coordinate(2, 1)).neightborAt(B).coordinate()).isEqualTo(new Coordinate(3, 2));
        assertThat(grid.cellAt(new Coordinate(2, 1)).neightborAt(C).coordinate()).isEqualTo(new Coordinate(3, 1));

        assertThat(grid.cellAt(new Coordinate(3, 1)).neightborAt(A).coordinate()).isEqualTo(new Coordinate(4, 1));
        assertThat(grid.cellAt(new Coordinate(3, 1)).neightborAt(B).coordinate()).isEqualTo(new Coordinate(2, 0));
        assertThat(grid.cellAt(new Coordinate(3, 1)).neightborAt(C).coordinate()).isEqualTo(new Coordinate(2, 1));

        assertThat(grid.cellAt(new Coordinate(4, 5)).neightborAt(A).coordinate()).isEqualTo(new Coordinate(3, 5));
        assertThat(grid.cellAt(new Coordinate(4, 5)).neightborAt(B).isNull()).isTrue();
        assertThat(grid.cellAt(new Coordinate(4, 5)).neightborAt(C).coordinate()).isEqualTo(new Coordinate(5, 5));

        assertThat(grid.cellAt(new Coordinate(5, 5)).neightborAt(A).isNull()).isTrue();
        assertThat(grid.cellAt(new Coordinate(5, 5)).neightborAt(B).coordinate()).isEqualTo(new Coordinate(4, 4));
        assertThat(grid.cellAt(new Coordinate(5, 5)).neightborAt(C).coordinate()).isEqualTo(new Coordinate(4, 5));
    }
}
