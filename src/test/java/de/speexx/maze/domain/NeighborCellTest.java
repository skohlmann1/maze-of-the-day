/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.domain;

import de.speexx.maze.domain.GridCell;
import de.speexx.maze.domain.Coordinate;
import de.speexx.maze.domain.UnsupportedLinkException;
import static com.google.common.truth.Truth.assertThat;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static de.speexx.maze.domain.ManhattenNeighborhood.NORTH;
import static de.speexx.maze.domain.ManhattenNeighborhood.SOUTH;
import static de.speexx.maze.domain.ManhattenNeighborhood.WEST;
import static de.speexx.maze.domain.ManhattenNeighborhood.EAST;

/**
 *
 * @author saschakohlmann
 */
public class NeighborCellTest {

    @Test
    public void neighborhoodDirections() {
        // Given
        final var root = new GridCell(new Coordinate(1, 1));
        
        root.withNeightborAt(NORTH, new GridCell(new Coordinate(0, 1)))
                .withNeightborAt(SOUTH, new GridCell(new Coordinate(0, 2)))
                .withNeightborAt(WEST, new GridCell(new Coordinate(1, 0)))
                .withNeightborAt(EAST, new GridCell(new Coordinate(1, 2)));
        
        // Then
        assertThat(root.neightborAt(NORTH)).isEqualTo(new GridCell(new Coordinate(0, 1)));
        assertThat(root.neightborAt(SOUTH)).isEqualTo(new GridCell(new Coordinate(0, 2)));
        assertThat(root.neightborAt(WEST)).isEqualTo(new GridCell(new Coordinate(1, 0)));
        assertThat(root.neightborAt(EAST)).isEqualTo(new GridCell(new Coordinate(1, 2)));
    }


    @Test
    public void withoutNeighborhoodDirections() {
        // Given
        final var root = new GridCell(new Coordinate(1, 1));
        
        root.withNeightborAt(NORTH, new GridCell(new Coordinate(0, 1)))
                .withNeightborAt(SOUTH, new GridCell(new Coordinate(0, 2)))
                .withNeightborAt(WEST, new GridCell(new Coordinate(1, 0)))
                .withNeightborAt(EAST, new GridCell(new Coordinate(1, 2)));
        
        // When
        root.removeNeightborAt(EAST)
                .removeNeightborAt(NORTH)
                .removeNeightborAt(SOUTH)
                .removeNeightborAt(WEST);
        final var neighbors = root.neighbors();
        
        // Then
        assertThat(neighbors).isEmpty();
    }
    
    @Test
    public void neighbors() {
        // Given
        final var root = new GridCell(new Coordinate(1, 1));
        
        root.withNeightborAt(NORTH, new GridCell(new Coordinate(0, 1)))
                .withNeightborAt(SOUTH, new GridCell(new Coordinate(0, 2)))
                .withNeightborAt(WEST, new GridCell(new Coordinate(1, 0)))
                .withNeightborAt(EAST, new GridCell(new Coordinate(1, 2)));
        
        // When
        final var neighbors = root.neighbors();
        
        // Then
        assertThat(neighbors).contains(new GridCell(new Coordinate(0, 1)));
        assertThat(neighbors).contains(new GridCell(new Coordinate(0, 2)));
        assertThat(neighbors).contains(new GridCell(new Coordinate(1, 0)));
        assertThat(neighbors).contains(new GridCell(new Coordinate(1, 2)));
    }
    
    @Test
    public void linkWith() {
        // Given
        final var root = new GridCell(Coordinate.ZERO);
        final var south = new GridCell(new Coordinate(1, 0));
        root.withNeightborAt(SOUTH, south);
        south.withNeightborAt(NORTH, root);
        
        // When
        root.linkWith(south);
        
        // Then
        assertThat(root.linkedWith()).contains(south);
        assertThat(south.linkedWith()).contains(root);
    }

    @Test
    public void unlinkFrom() {
        // Given
        final var root = new GridCell(Coordinate.ZERO);
        final var south = new GridCell(new Coordinate(1, 0));
        root.withNeightborAt(SOUTH, south)
                .withNeightborAt(NORTH, root);
        
        // When
        root.linkWith(south);
        root.unlinkFrom(south);
        
        // Then
        assertThat(root.linkedWith()).doesNotContain(south);
        assertThat(south.linkedWith()).doesNotContain(root);
    }

    @Test
    public void linkWith_NULL() {
        // Given
        final var root = new GridCell(Coordinate.ZERO);
        final var south = new GridCell(new Coordinate(1, 0));
        root.withNeightborAt(SOUTH, south)
                .withNeightborAt(NORTH, root);
        
        // Then
        assertThrows(UnsupportedLinkException.class, () -> root.linkWith(GridCell.NULL));
    }

    @Test
    public void linkWith_null() {
        // Given
        final var root = new GridCell(Coordinate.ZERO);
        final var south = new GridCell(new Coordinate(1, 0));
        root.withNeightborAt(SOUTH, south)
                .withNeightborAt(NORTH, root);
        
        // Then
        assertThrows(UnsupportedLinkException.class, () -> root.linkWith(null));
    }
    
    @Test
    public void neighborHoodOf() {
        
        // Given
        final var root = new GridCell(Coordinate.ZERO);
        final var neighbor1 = new GridCell(new Coordinate(0, 1));
        final var neighbor2 = new GridCell(new Coordinate(1, 1));
        root.withNeightborAt(SOUTH, neighbor1);
        root.withNeightborAt(EAST, neighbor1);
        root.withNeightborAt(WEST, neighbor2);
        neighbor1.withNeightborAt(NORTH, root);
        
        // When
        final var neighborHood = root.neighborhoodOf(neighbor1);
        
        // Then
        assertThat(neighborHood).containsExactly(SOUTH, EAST);
    }

    @Test
    public void neighborHoodOf_this() {
        
        // Given
        final var root = new GridCell(Coordinate.ZERO);
        final var neighbor1 = new GridCell(new Coordinate(0, 1));
        final var neighbor2 = new GridCell(new Coordinate(1, 1));
        root.withNeightborAt(SOUTH, neighbor1);
        root.withNeightborAt(EAST, neighbor1);
        root.withNeightborAt(WEST, neighbor2);
        neighbor1.withNeightborAt(NORTH, root);
        
        // When
        final var neighborHood = root.neighborhoodOf(root);
        
        // Then
        assertThat(neighborHood).containsExactly(WEST, SOUTH, EAST);
    }
    
    @Test
    public void neighborHoodOf_with_no_neighborHood() {
        
        // Given
        final var root = new GridCell(Coordinate.ZERO);
        final var neighbor = new GridCell(new Coordinate(0, 1));
        root.withNeightborAt(SOUTH, neighbor);
        
        // When
        final var neighborHood = neighbor.neighborhoodOf(root);
        
        // Then
        assertThat(neighborHood).isEmpty();
    }
}
