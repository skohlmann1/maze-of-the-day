/*
 * This file is part of the Maze project.
 * (c) 2021 Sascha Kohlmann - All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.maze.graphics;

import de.speexx.maze.graphics.Tile.DefaultTile;
import de.speexx.maze.graphics.TilePattern.DefaultTilePattern;
import java.awt.BasicStroke;
import java.awt.geom.Rectangle2D;
import org.junit.jupiter.api.Test;

import static java.awt.Color.BLACK;
import static java.util.Set.of;

import static com.google.common.truth.Truth.assertThat;

/**
 *
 * @author saschakohlmann
 */
public class TilePatternTest {
    
    @Test
    public void totalDimension() {
        // Give
        final var tile1 = new DefaultTile(new Rectangle2D.Double(12, 24, 8, 60), BLACK, new BasicStroke());
        final var tile2 = new DefaultTile(new Rectangle2D.Double(18, 26, 3, 56), BLACK, new BasicStroke());
        final var tile3 = new DefaultTile(new Rectangle2D.Double(-12, 2, 3, 3), BLACK, new BasicStroke());
        
        final var tiles = new DefaultTilePattern(of(tile1), of(tile2), of(tile3));

        // When
        final var bounds = tiles.bounds();

        // Then
        assertThat(bounds.getMinX()).isEqualTo(-12d);
        assertThat(bounds.getMinY()).isEqualTo(2d);
        assertThat(bounds.getMaxX()).isEqualTo(21d);
        assertThat(bounds.getMaxY()).isEqualTo(84d);

        assertThat(bounds.getWidth()).isEqualTo(33d);
        assertThat(bounds.getHeight()).isEqualTo(82d);

    }
}
