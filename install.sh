#!/bin/bash

mvn clean verify

cd target
unzip maze-*-bin.zip
cp -r maze-1.0-SNAPSHOT "${HOME}/bin"
