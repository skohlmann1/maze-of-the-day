= Maze

== Overview

This starts as a playground project inspired by http://www.jamisbuck.org/mazes/[Mazes by Jamis Buck].

Maze is a generator or mazes or labyrinth (whatever you name it).
It offers a command line interface to generate diffferent types of mazes like square or rectangled mazes, triangle mazes and more will come, sooner or later.

The plan is to make it a product. Means: it will not be finished, new features will be added when ever I've time or get code contributions.

Additional this is a _test-it-out_ project, based allways on the newst Java development version.
As of starting it based on Java 19 and `--enable-preview` is allways required for compilation and execution.
See link:poml.xml[`poml.xml`] for the lastest version.

And as a huge fan of open source software, I use the link:LICENSE[GNU Affero General Public License] version 3 or later for the codebase.

== Installation
    
There is currently no installation version available.
You must build the installation version by your own.
But this is pretty forward and simple.

. Install the newest Java version (see link:poml.xml[`poml.xml`])
. Install http://maven.apache.org/[Maven] if not yet installed on your system
. Clone this repository
. Enter the cloned repository
. Type `mvn install` to build the project

After building the project find in the `target` folder a file `maze-xxxxxxxxTxxxxxx-bin.zip` where `xxxxxxxxTxxxxxx` represents the build time like `20210601T051007`.
Inflate the ZIP file in a folder of your choice, enter the inflated `maze-...` folder and run the Maze grid command with `./maze grid`.
If the command was executed successfully it prints a Maze representation in JSON format to the console.

NOTE: this documentation is for Unix like systems.
A start script (https://www.microsoft.com/en-us/p/powershell/9mz1snwt0n5d[PowerShell]) for Windows environments is also available with `maze.ps1`.

== Usage

Type `maze -h` to get an overview of all available commands with a short parameter description.

TBC
